<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Utils\GlobalVariables;
use App\Models\Job;
use Illuminate\Support\Facades\Auth;
use Tests\Feature\Utils\TestUtil;
use App\User;

class JobControllerTest extends TestCase {

    use RefreshDatabase;

    private $response;
    private $requiredMessage;
    
    const NULL_JOB_DATA = [null];
    const VALID_JOB_DATA = ['Job'];
    const VALID_JOB_DATA_2 = ['Job2'];
    const VALID_JOB_DATA_3 = ['name3'];
    const UPDATED_JOB_DATA = ['Updated Job'];
    const NON_EXIST_ID = 1000;
    const JOB_ID = 1222;
    const SIZE = 1;
    const SEARCH = "Jo";

    private $job;
    private $updatedJob;

    protected function setUp(): void {
        $this->response = null;
        $this->job = null;
        $this->updatedJob = null;
        $this->requiredMessage= TestUtil::getErrorMessage("The :attribute is required.", [Job::NAME]);
        parent::setUp();
    }

    protected function tearDown(): void {
        $this->assertTrue(Auth::check());
        parent::tearDown();
    }
    

    public function test_givenEmptyJobDataAndAccountantUser_whenCreateJob_thenCreateWillBeInvalid() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->getJob(self::NULL_JOB_DATA);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_STORE), $this->job);
        //Then
        $this->assertEquals($this->response->getData()->errors->name[0], $this->requiredMessage);
    }

    public function test_givenEmptyJobDataAndWorkerUser_whenCreateJob_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->job = $this->getJob(self::NULL_JOB_DATA);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_STORE), $this->job);
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenValidJobDataAndAccountantUser_whenCreate_thenWillCreated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->getJob(self::VALID_JOB_DATA);
        $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_STORE), $this->job);
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::VALID_JOB_DATA, self::SIZE);
    }

    public function test_givenNonAccountantUserAndValidJobData_whenCreateJob_thenWillGiveUnuthorizedException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->job = $this->getJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_STORE), $this->job);
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUserAndNonExistJob_whenRequestUpdateView_ThenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->getJson(route(GlobalVariables::JOBS_EDIT, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenAccountantUserAndExistJob_whenRequestUpdateView_ThenWillDisplay() {
        //Given
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->getJson(route(GlobalVariables::JOBS_EDIT, $this->job->id));
        //Then
        $this->response->assertViewIs(GlobalVariables::JOBS_UPDATE_VIEW);
        $this->assertEquals($this->response->viewData(GlobalVariables::JOB)->name, self::VALID_JOB_DATA[0]);
    }

    public function test_givenWorkerUserAndExistJob_whenRequestUpdateView_ThenWillAbortAnuthorizePage() {
        //Given
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->getJson(route(GlobalVariables::JOBS_EDIT, $this->job->id));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenEmptyJobDataAndAccountantUser_whenUpdateJob_thenUpdateWillBeInvalid() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->updatedJob = $this->getUpdatedJob(self::NULL_JOB_DATA);
        //When
        $this->response = $this->putJson(route(GlobalVariables::JOBS_UPDATE, self::JOB_ID), $this->updatedJob);
        //Then
        $this->assertEquals($this->response->getData()->errors->name[0], $this->requiredMessage);
    }

    public function test_givenEmptyJobDataAndWorkerUser_whenUpdateJob_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->updatedJob = $this->getUpdatedJob(self::NULL_JOB_DATA);
        //When
        $this->response = $this->putJson(route(GlobalVariables::JOBS_UPDATE, self::JOB_ID), $this->updatedJob);
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenValidJobDataAndAccountantUser_whenUpdate_thenWillUpdated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->putJson(route(GlobalVariables::JOBS_UPDATE, $this->job->id)
                , $this->getUpdatedJob(self::UPDATED_JOB_DATA));
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::UPDATED_JOB_DATA, self::SIZE);
    }

    public function test_givenNonAccountantUserAndValidJobData_whenUpdateJob_thenWillGiveUnuthorizedException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->updatedJob = $this->getUpdatedJob(self::UPDATED_JOB_DATA);
        //When
        $this->response = $this->putJson(route(GlobalVariables::JOBS_UPDATE, self::JOB_ID), $this->updatedJob);
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestJobs_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        //Create 1 more deleted job to see if index action get only non deleted jobs or not
        $this->createThenDeleteJob(self::VALID_JOB_DATA_2);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_INDEX));
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::VALID_JOB_DATA, self::SIZE);
    }

    public function test_givenWorkerUser_whenRequestJobs_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_INDEX));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenDeleteJob_thenWillDeleted() {
        $this->withoutExceptionHandling();
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_DESTROY, $this->job->id));
        //Then
        $this->assertNull(Job::find($this->job->id));
        $this->assertNotNull(Job::withTrashed()->find($this->job->id));
    }

    public function test_givenAccountantUser_whenDeleteNonExistJob_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_DESTROY, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerUser_whenDeleteJob_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_DESTROY, $this->job->id));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRestoreJob_thenWillRestored() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_RESTORE, $this->job->id));
        //Then  
        $this->assertNotNull(Job::find($this->job->id));
    }

    public function test_givenAccountantUser_whenRestoreNonExistJob_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_RESTORE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerUser_whenrRestoreJob_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->job = $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_RESTORE, $this->job->id));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestDeletedJobs_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->createJob(self::VALID_JOB_DATA);
        //Create 1 more deleted job to see if get only  deleted jobs or not
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA_2);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_ONLY_DELETED));
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::VALID_JOB_DATA_2, self::SIZE);
    }

    public function test_givenWorkerUser_whenRequestDeletedJobs_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_ONLY_DELETED));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestSearchedJobs_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->createJob(self::VALID_JOB_DATA);
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA_2);
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA_3);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_SEARCH), $this->getJob(self::SEARCH));
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::VALID_JOB_DATA, 2);
    }

    public function test_givenWorkerUser_whenRequestSearchedJobs_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->createJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->postJson(route(GlobalVariables::JOBS_SEARCH), $this->getJob(self::SEARCH));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountatUser_whenRequestVerifyDeleteView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_VERIFY_DELETE, self::JOB_ID));
        //Then
        $this->response->assertViewIs(GlobalVariables::JOBS_VERIFY_DELETE_VIEW);
        $this->assertEquals($this->response->viewData(Job::ID), self::JOB_ID);
    }

    public function test_givenWorkerUser_whenRequestVerifyDeleteView_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_VERIFY_DELETE, self::JOB_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenForceDeleteJob_thenWillForceDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $this->createThenDeleteJob(self::VALID_JOB_DATA);
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_FORCE_DELETE, $this->job->id));
        //Then
        $this->assertView($this->response, GlobalVariables::JOBS_MANAGING_VIEW
                , GlobalVariables::JOBS, self::VALID_JOB_DATA, self::SIZE);
    }

    public function test_givenAccountantUser_whenForceDeleteNonExistJob_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_FORCE_DELETE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerUser_whenrForceDeleteJob_thenWillAbortUnuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->job = $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //When
        $this->response = $this->get(route(GlobalVariables::JOBS_FORCE_DELETE, $this->job->id));
         //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    //Testing util functions
    public function test_givenJobData_whenCreateThenDelete_thenWillCreateAndDelete() {
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->createThenDeleteJob(self::VALID_JOB_DATA);
        //Then
        $this->assertEquals(Job::withTrashed()->get()->toArray()[0][Job::NAME], self::VALID_JOB_DATA[0]);
    }

    //Utils
    private function getJob($jobData) {
        return [
            Job::NAME => $jobData[0]
        ];
    }

    private function getUpdatedJob($jobData) {
        $job = $this->getJob($jobData);
        $job[GlobalVariables::TOKEN] = GlobalVariables::TOKEN;
        $job[GlobalVariables::PUT] = GlobalVariables::PUT;
        return $job;
    }

    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE=>$type],true);
        $this->postJson(route('login'), ['email' =>$user[User::EMAIL], 'password' =>'12345678']);
    }

    private function createJob($jobData) {
        return Job::create($this->getJob($jobData));
    }

    private function createThenDeleteJob($jobData) {
        $job = $this->createJob($jobData);
        Job::find($job->id)->delete();
        return $job;
    }

    private function assertView($response, $viewName, $viewDataName, $viewDataValue, $size) {
        $jobs = $this->response->viewData($viewDataName)->toArray()['data'];
        $response->assertViewIs($viewName);
        $this->assertEquals(count($jobs), $size);
        $this->assertEquals($jobs[0][Job::NAME], $viewDataValue[0]);
    }

}
