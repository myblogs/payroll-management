<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Utils\GlobalVariables;
use Tests\Feature\Utils\TestUtil;
use App\User;
use App\Vacation;
use App\Utils\Messages\ErrorMessages;
use App\Models\Setting;
use App\Utils\Messages\SucessMessges;
use App\Models\FinancialVacation;

class VacationsControllerTest extends TestCase {

    use RefreshDatabase;

    public function test_givenUnworkerAccounntant_whenCreate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE));
        //Then
        $response->assertForbidden();
    }

    public function test_givenEmptyData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => '',
            Vacation::END_DATE => '',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Vacation::END_DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => 'Not valid date format',
            Vacation::END_DATE => 'Not valid date format',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::IS_NOT_DATE,
            Vacation::END_DATE => ErrorMessages::IS_NOT_DATE,
        ]);
    }

    public function test_2_givenInvalidData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => '2019-01-01',
            Vacation::END_DATE => '2019-01-01',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::BEFORE_CURRENT_DATE(),
            Vacation::END_DATE . "," . Vacation::START_DATE => ErrorMessages::DATE_IS_BEFORE_DATE,
        ]);
    }

    public function test_3_givenInvalidData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => TestUtil::addToDate($vacation[Vacation::END_DATE], '-1 day'),
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::NEW_VACATION_BEFORE_LAST_VACATION,
        ]);
    }

    public function test_4_givenInvalidData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 0
        ]);

        $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept();
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::END_DATE => ErrorMessages::EXCEED_MAX_VACATIONS_DAYS,
        ]);
    }

    public function test_5_givenInvalidData_whenCreate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), "+1 year"),
            Vacation::END_DATE => TestUtil::addToDate(date('Y-m-d'), "+1 year +1day")
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::END_DATE => ErrorMessages::DATE_YEAR_IS_NOT_CURRENT_YEAR,
            Vacation::START_DATE => ErrorMessages::DATE_YEAR_IS_NOT_CURRENT_YEAR,
        ]);
    }

    public function test_givenUserVacations_whenCreate_thenWillSumAllVacationsDaysAccourdingTheDatabaseBusiness() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 4
        ]);
        $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        //sum query will ignore those vacations
        $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            'created_at' => TestUtil::addToDate(date('Y-m-d'), '-1 year')
        ]);
        $this->createDefaultVacationExcept();
        //sum query will ignore those vacations

        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+2 day'),
            Vacation::END_DATE => TestUtil::addToDate(date('Y-m-d'), '+3 day'),
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $invalidVacation);
        //Then
        $response->assertOk();
    }

    public function givenWorkerUser_whenCreate_thenCreated() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultFinancialVacationsExcept([
            FinancialVacation::DATE => TestUtil::addToDate(date('Y-m-d'), '+2 day')
        ]);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 2
        ]);
        $validVacation = $this->getDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => date('Y-m-d'),
            Vacation::END_DATE => TestUtil::addToDate(date('Y-m-d'), '+4 day')
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_STORE), $validVacation);
        //Then
        $createdVacation = Vacation::get()->first();
        $this->assertVacationEquals($createdVacation, $validVacation, 2, $user[User::NAME]);
        $response->assertSee(SucessMessges::CREARTED_SUCCESSFULLY);
    }

    public function test_givenWrongVacation_whenUserRequestUpdateView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_EDIT, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacationStarted_whenUserRequestUpdateView_thenWillAbortBadRequest() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_EDIT, $vacation[Vacation::ID]));
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $response->assertJson([
            'message' => ErrorMessages::VACATION_START
        ]);
    }

    public function test_givenVacation_whenUserRequestUpdateView_thenWillRendered() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day'),
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_EDIT, $vacation[Vacation::ID]));
        //Then
        $response->assertViewIs(GlobalVariables::VACATIONS_UPDATE_VIEW);
        $response->assertViewHas(GlobalVariables::VACATION, $vacation);
    }

    public function test_givenWrongVacation_whenUpdate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenEmptyData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $vacation[Vacation::ID],
            Vacation::START_DATE => '',
            Vacation::END_DATE => '',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $vacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Vacation::END_DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $vacation[Vacation::ID],
            Vacation::START_DATE => 'Not valid date format',
            Vacation::END_DATE => 'Not valid date format',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $vacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::IS_NOT_DATE,
            Vacation::END_DATE => ErrorMessages::IS_NOT_DATE,
        ]);
    }

    public function test_2_givenInvalidData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $vacation[Vacation::ID],
            Vacation::START_DATE => '2019-01-01',
            Vacation::END_DATE => '2019-01-01',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $vacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::BEFORE_CURRENT_DATE(),
            Vacation::END_DATE . "," . Vacation::START_DATE => ErrorMessages::DATE_IS_BEFORE_DATE,
        ]);
    }

    public function test_3_givenInvalidData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        $desiredVacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $desiredVacation[Vacation::ID],
            Vacation::START_DATE => TestUtil::addToDate($vacation[Vacation::END_DATE], '-1 day'),
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $desiredVacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::NEW_VACATION_BEFORE_LAST_VACATION,
        ]);
    }

    public function test_4_givenInvalidData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 1
        ]);
        $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);

        $desiredVacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $desiredVacation[Vacation::ID],
        ]);

        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $desiredVacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::END_DATE => ErrorMessages::EXCEED_MAX_VACATIONS_DAYS,
        ]);
    }

    public function test_5_givenInvalidData_whenUpdate_thenWillThrowsInvalidationException() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $vacation[Vacation::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), "+1 year"),
            Vacation::END_DATE => TestUtil::addToDate(date('Y-m-d'), "+1 year +1day")
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $vacation[Vacation::ID])
                , $invalidVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            Vacation::START_DATE => ErrorMessages::DATE_YEAR_IS_NOT_CURRENT_YEAR,
            Vacation::END_DATE => ErrorMessages::DATE_YEAR_IS_NOT_CURRENT_YEAR,
        ]);
    }

    public function test_givenVacationStarted_whenUpdate_thenWillAbortBadRequest() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept();
        $desiredVacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
        ]);
        $invalidVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $desiredVacation[Vacation::ID],
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $desiredVacation[Vacation::ID])
                , $invalidVacation);
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $response->assertJson([
            'message' => ErrorMessages::VACATION_START
        ]);
    }

    public function test_givenVacation_whenUpdate_thenUpdated() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 2
        ]);
        $desiredVacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day'),
        ]);
        $validVacation = $this->getDefaultVacationExcept([
            Vacation::ID => $desiredVacation[Vacation::ID],
            Vacation::USER_ID => $user[User::ID],
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::VACATIONS_UPDATE, $desiredVacation[Vacation::ID])
                , $validVacation);
        //Then
        $response->assertSee(SucessMessges::UPDATED_SUCCESSFULLY);
        $updatedVacation = Vacation::find($desiredVacation[Vacation::ID]);
        $this->assertVacationEquals($updatedVacation, $validVacation, 1, $user[User::NAME]);
    }

    public function test_givenWrongVacation_whenDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacationStarted_whenDelete_thenWillAbortBadRequest() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $response->assertJson([
            'message' => ErrorMessages::VACATION_START
        ]);
    }

    public function test_givenVacation_whenDelete_thenWillDeleteed() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day'),
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_DELETE, $vacation[Vacation::ID]));
        //Then
        $this->assertVacationDeleted($vacation[Vacation::ID]);
    }

    public function test_givenWrongVacation_whenRequestConfirmDeleteView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_CONFIRM_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacation_whenRequestConfirmDeleteView_thenWillRendered() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day')
        ]);
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_CONFIRM_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertViewIs(GlobalVariables::VACATIONS_CONFIRM_DELETE_VIEW);
        $response->assertViewHas(Vacation::ID, $vacation[Vacation::ID]);
    }

    public function test_givenWrongVacation_whenForceDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_FORCE_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacation_whenFroceDelete_thenWillDeleted() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
            Vacation::START_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day')
        ]);
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_FORCE_DELETE, $vacation[Vacation::ID]));
        //Then
        $response->assertSee(SucessMessges::DELETED_SUCCESSFULLY);
        $this->assertVacationForcingDeleted($vacation[Vacation::ID]);
    }

    public function test_givenWrongVacation_whenRestore_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept();
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_RESTORE, $vacation[Vacation::ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacationNotAfterLastVacation_whenRestore_thenWillAbortBadRequest() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->createDefaultVacationExcept();
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
        ]);
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_RESTORE, $vacation[Vacation::ID]));
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $response->assertJson([
            'message' => ErrorMessages::NOT_VALID
        ]);
    }

    public function test_givenExceedMaxVacationDays_whenRestore_thenWillAbortBadRequest() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
        ]);
        $vacation->delete();

        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_RESTORE, $vacation[Vacation::ID]));
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $response->assertJson([
            'message' => ErrorMessages::NOT_VALID
        ]);
    }

    public function test_givenVacation_whenRestore_thenRestored() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_VACATION_DAYS => 2
        ]);
        $vacation = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID],
        ]);
        $vacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_RESTORE, $vacation[Vacation::ID]));
        //Then
        $this->assertVacationRestored($vacation[Vacation::ID]);
    }

    public function test_givenNotWorker_whenRequestWorkerVacations_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_INDEX));
        //Then
        $response->assertForbidden();
    }

    public function test_givenWorkerVacations_whenRequestVacations_thenWillReturned() {
        //Given
        $user = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $vacationOne = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $vacationTow = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $user[User::ID]
        ]);
        $vacationTow->delete();
        //This will not returned
        $this->createDefaultVacationExcept([
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::VACATIONS_INDEX));
        //Then
        $response->assertViewIs(GlobalVariables::VACATIONS_MANAGING_VIEW);
        $workerVacations = $response->viewData(GlobalVariables::VACATIONS);
        $this->assertVacationsEquals($workerVacations, [$vacationOne, $vacationTow]
                , 2, $vacationOne[Vacation::VACATION_DAYS], $user[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenSearchVacations_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_SEARCH));
        //Then
        $response->assertForbidden();
    }

    public function test_givenVacations_whenSearch_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        
        $userOne= TestUtil::createUser([User::NAME=>'Omar']);
        $vacationOne = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $userOne[User::ID]
        ]);
        
        $vacationTow = $this->createDefaultVacationExcept([
            Vacation::USER_ID => $userOne[User::ID]
        ]);
        $vacationTow->delete();
        
        $userThree=TestUtil::createUser([User::NAME=>'Ahmed']);
        //This will not returned
        $this->createDefaultVacationExcept([
            Vacation::USER_ID => $userThree[User::ID],
            Vacation::VACATION_DAYS =>10,
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::VACATIONS_SEARCH),[User::NAME=>'Omar']);
        //Then
        $response->assertViewIs(GlobalVariables::VACATIONS_MANAGING_VIEW);
        $workerVacations = $response->viewData(GlobalVariables::VACATIONS);
        $this->assertVacationsEquals($workerVacations, [$vacationOne, $vacationTow]
                , 2, $vacationOne[Vacation::VACATION_DAYS], $userOne[User::NAME]);
    }

    //Utils
    private function getDefaultVacationExcept($data = array(), $object = false) {
        $vacation = ($object) ? new Vacation() : array();
        $vacation[Vacation::USER_ID] = isset($data[Vacation::USER_ID]) ? $data[Vacation::USER_ID] : null;
        $vacation[Vacation::ID] = isset($data[Vacation::ID]) ? $data[Vacation::ID] : null;
        $vacation[Vacation::START_DATE] = isset($data[Vacation::START_DATE]) ? $data[Vacation::START_DATE] : date('Y-m-d');
        $vacation[Vacation::END_DATE] = isset($data[Vacation::END_DATE]) ? $data[Vacation::END_DATE] : TestUtil::addToDate(date('Y-m-d'), '+1 day');
        if (isset($data['created_at'])) {
            $vacation['created_at'] = $data['created_at'];
        }
        return $vacation;
    }

    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), ['email' => $user['email'], 'password' => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function createDefaultVacationExcept($data = array()) {
        $data = $this->getDefaultVacationExcept($data, true);
        $data[Vacation::VACATION_DAYS] = 1;
        $data->save();
        return $data;
    }

    private function assertVacationEquals($vacation1, $vacation2, $vacationDays, $userName) {
        $this->assertEquals($vacation1[Vacation::START_DATE], $vacation2[Vacation::START_DATE]);
        $this->assertEquals($vacation1[Vacation::END_DATE], $vacation2[Vacation::END_DATE]);
        $this->assertEquals($vacation1[Vacation::VACATION_DAYS], $vacationDays);
        $this->assertEquals($vacation1[Vacation::USER_ID], $vacation2[Vacation::USER_ID]);
        $this->assertEquals($vacation1[Vacation::USER][User::NAME], $userName);
    }

    private function assertVacationDeleted($id) {
        $this->assertNull(Vacation::find($id));
        $this->assertNotNull(Vacation::withTrashed()->find($id));
    }

    private function assertVacationRestored($id) {
        $this->assertNotNull(Vacation::find($id));
        $this->assertNull(Vacation::onlyTrashed()->find($id));
    }

    public function assertVacationForcingDeleted($id) {
        $this->assertNull(Vacation::find($id));
        $this->assertNull(Vacation::withTrashed()->find($id));
    }

    public function assertVacationsEquals($workerVacations, $_workerVacations, $size, $vacationDays
    , $userName) {
        for ($i = 0; $i < $size; $i++) {
            $this->assertVacationEquals($workerVacations[$i], $_workerVacations[$i]
                    , $vacationDays, $userName);
        }
        $this->assertEquals($workerVacations->count(), $size);
    }

}
