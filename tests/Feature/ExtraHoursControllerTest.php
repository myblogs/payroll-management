<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Utils\GlobalVariables;
use App\Models\ExtraHours;
use Tests\Feature\Utils\TestUtil;
use App\Utils\Messages\ErrorMessages;
use App\Utils\Messages\SucessMessges;
use Illuminate\Pagination\Paginator;

class ExtraHoursControllerTest extends TestCase {

    use RefreshDatabase;

    public function test_givenNotAccountantUser_whenCreate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_STORE));
        //Then
        $response->assertForbidden();
    }

    public function test_givenEmptyData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => ''
        ];
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_STORE), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::ATTRIBUTE_IS_EMPTY
        ]);
    }

    public function test_1_givenInvalidData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => 'non-numeric-value'
        ];
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_STORE), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::IS_NOT_NUMBER
        ]);
    }

    public function test_2_givenInvalidData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => 1
        ];
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_STORE), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::EXCEED_MAX_EXTRA_HOURS
        ]);
    }

    public function test_givenValidData_whenCreate_thenWillCreated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $worker = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $validData = [
            ExtraHours::EXTRA_HOURS => 0,
            ExtraHours::USER_ID => $worker[User::ID],
        ];
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_STORE), $validData);
        //Then
        $response->assertSee(SucessMessges::CREARTED_SUCCESSFULLY);
        $extraHours = ExtraHours::get()->first();
        $this->assertExtraHoursEquals($extraHours, $validData, $worker[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenRequestUpdateView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_EDIT, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonExistExtraHours_whenRequestUpdateView_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_EDIT, 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExistExtraHours_whenRequestUpdateView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $worker = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $_extraHours = ExtraHours::create([
                    ExtraHours::USER_ID => $worker[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_EDIT
                        , $_extraHours[ExtraHours::ID]));
        //Then
        $extraHours = $response->viewData(GlobalVariables::EXTRA_HOUR);
        $workers = $response->viewData(GlobalVariables::WORKERS);
        $this->assertExtraHoursEquals($extraHours, $_extraHours, $worker[User::NAME]);
        $this->assertEquals($workers->count(), 1);
        $this->assertEquals($workers->first()[User::NAME], $worker[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenUpdate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->putJson(route(GlobalVariables::EXTRA_HOURS_UPDATE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenEmptyData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => ''
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::EXTRA_HOURS_UPDATE, 1), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::ATTRIBUTE_IS_EMPTY
        ]);
    }

    public function test_1_givenInvalidData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => 'non-numeric-value'
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::EXTRA_HOURS_UPDATE, 1), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::IS_NOT_NUMBER
        ]);
    }

    public function test_2_givenInvalidData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidData = [
            ExtraHours::EXTRA_HOURS => 1
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::EXTRA_HOURS_UPDATE, 1), $invalidData);
        //Then
        TestUtil::assertInvalidation($response, [
            ExtraHours::EXTRA_HOURS => ErrorMessages::EXCEED_MAX_EXTRA_HOURS
        ]);
    }

    public function test_givenExistExtraHours_whenUpdate_thenWillUpdated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $workerOne = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $workerTow = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $_extraHours = ExtraHours::create([
                    ExtraHours::USER_ID => $workerOne[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);
        $validData = [
            ExtraHours::USER_ID => $workerTow[User::ID],
            ExtraHours::EXTRA_HOURS => 0,
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::EXTRA_HOURS_UPDATE
                        , $_extraHours[ExtraHours::ID]), $validData);
        //Then
        $response->assertSee(SucessMessges::UPDATED_SUCCESSFULLY);
        $extraHours = ExtraHours::find($_extraHours[ExtraHours::ID]);
        $this->assertExtraHoursEquals($extraHours, $validData, $workerTow[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenRequestExtraHours_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_INDEX));
        //Then
        $response->assertForbidden();
    }

    public function test_givenExtraHours_whenRequestExtraHours_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $workerOne = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $workerTow = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //query return all non deleted extrahours for all workers
        $extraHoursOne = ExtraHours::create([
                    ExtraHours::USER_ID => $workerOne[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);
        $extraHoursTow = ExtraHours::create([
                    ExtraHours::USER_ID => $workerTow[User::ID],
                    ExtraHours::EXTRA_HOURS => 3,
        ]);
        $extraHoursThree = ExtraHours::create([
                    ExtraHours::USER_ID => $workerOne[User::ID],
                    ExtraHours::EXTRA_HOURS => 4,
        ]);
        $extraHoursThree->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_INDEX));
        //Then
        $extrahours = $response->viewData(GlobalVariables::EXTRA_HOURS);
        $response->assertViewIs(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW);
        $this->assertTrue($extrahours instanceof Paginator);
        $this->assertExtraHoursEquals($extrahours[0], $extraHoursOne, $workerOne[User::NAME]);
        $this->assertExtraHoursEquals($extrahours[1], $extraHoursTow, $workerTow[User::NAME]);
        $this->assertEquals($extrahours->count(), 2);
        $workers = $response->viewData(GlobalVariables::WORKERS);
        $this->assertEquals($workers->count(), 2);
        $this->assertEquals($workers[0][User::NAME], $workerOne[User::NAME]);
        $this->assertEquals($workers[1][User::NAME], $workerTow[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_DELETE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNotExistExtraHours_whenDelete_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_DELETE, 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExtraHours_whenDelete_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 1
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_DELETE, $extraHours[ExtraHours::ID]));
        //Then
        $this->assertDeleted($extraHours[ExtraHours::ID]);
    }

    public function test_givenNotAccountantUser_whenRestore_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_RESTORE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNotExistExtraHours_whenRestore_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 2
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_RESTORE, $extraHours[ExtraHours::ID]));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExtraHours_whenRestore_thenWillRestored() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 1
        ]);
        $extraHours->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_RESTORE, $extraHours[ExtraHours::ID]));
        //Then
        $this->assertRestored($extraHours[ExtraHours::ID]);
    }

    public function test_givenNotAccountantUser_whenRequestConfirmDeleteView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNotExistExtraHours_whenRequestConfirmDeleteView_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 2
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE
                        , $extraHours[ExtraHours::ID]));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExtraHours_whenRequestConfirmDeleteView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 1
        ]);
        $extraHours->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE
                        , $extraHours[ExtraHours::ID]));
        //Then
        $response->assertViewHas(ExtraHours::ID, $extraHours[ExtraHours::ID]);
        $response->assertViewIs(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE_VIEW);
    }

    public function test_givenNotAccountantUser_whenForceDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_FORCE_DELETE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNotExistExtraHours_whenForceDelete_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 2
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_FORCE_DELETE
                        , $extraHours[ExtraHours::ID]));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExtraHours_whenForceDelete_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $extraHours = ExtraHours::create([
                    ExtraHours::EXTRA_HOURS => 1
        ]);
        $extraHours->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_FORCE_DELETE
                        , $extraHours[ExtraHours::ID]));
        //Then
        $response->assertSee(SucessMessges::DELETED_SUCCESSFULLY);
        $this->assertForcedDeleted($extraHours[ExtraHours::ID]);
    }

    public function test_givenNotAccountantUser_whenRequestAllDeleted_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_ALL_DELETED));
        //Then
        $response->assertForbidden();
    }

    public function test_givenExtraHours_whenRequestAllDeleted_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $workerOne = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $workerTow = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //query return all  deleted extrahours for all workers
        $extraHoursOne = ExtraHours::create([
                    ExtraHours::USER_ID => $workerOne[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);
        $extraHoursOne->delete();

        $extraHoursTow = ExtraHours::create([
                    ExtraHours::USER_ID => $workerTow[User::ID],
                    ExtraHours::EXTRA_HOURS => 3,
        ]);
        $extraHoursTow->delete();

        ExtraHours::create([
            ExtraHours::USER_ID => $workerOne[User::ID],
            ExtraHours::EXTRA_HOURS => 4,
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_ALL_DELETED));
        //Then
        $extrahours = $response->viewData(GlobalVariables::EXTRA_HOURS);
        $response->assertViewIs(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW);
        $this->assertTrue($extrahours instanceof Paginator);
        $this->assertExtraHoursEquals($extrahours[0], $extraHoursOne, $workerOne[User::NAME]);
        $this->assertExtraHoursEquals($extrahours[1], $extraHoursTow, $workerTow[User::NAME]);
        $this->assertEquals($extrahours->count(), 2);
        $workers = $response->viewData(GlobalVariables::WORKERS);
        $this->assertEquals($workers->count(), 2);
        $this->assertEquals($workers[0][User::NAME], $workerOne[User::NAME]);
        $this->assertEquals($workers[1][User::NAME], $workerTow[User::NAME]);
    }

    public function test_givenNotAccountantUser_whenSearch_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::EXTRA_HOURS_SEARCH));
        //Then
        $response->assertForbidden();
    }

    public function test_givenExtraHours_whenSearch_thenWillReturned() {

        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $workerOne = TestUtil::createUser([
                    User::NAME => 'Ali'
        ]);
        $workerTow = TestUtil::createUser([
                    User::NAME => 'Alaa'
        ]);

        $workerThree = TestUtil::createUser([
                    User::NAME => 'Osama'
        ]);
        //query return all  extrahours of specific worker name
        $extraHoursOne = ExtraHours::create([
                    ExtraHours::USER_ID => $workerOne[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);

        $extraHoursTow = ExtraHours::create([
                    ExtraHours::USER_ID => $workerTow[User::ID],
                    ExtraHours::EXTRA_HOURS => 3,
        ]);
        $extraHoursTow->delete();

        ExtraHours::create([
            ExtraHours::USER_ID => $workerThree[User::ID],
            ExtraHours::EXTRA_HOURS => 4,
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::EXTRA_HOURS_SEARCH), [User::NAME => 'Al']);
        //Then
        $extrahours = $response->viewData(GlobalVariables::EXTRA_HOURS);
        $response->assertViewIs(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW);
        $this->assertTrue($extrahours instanceof Paginator);
        $this->assertExtraHoursEquals($extrahours[0], $extraHoursOne, $workerOne[User::NAME]);
        $this->assertExtraHoursEquals($extrahours[1], $extraHoursTow, $workerTow[User::NAME]);
        $this->assertEquals($extrahours->count(), 2);
        $workers = $response->viewData(GlobalVariables::WORKERS);
        $this->assertEquals($workers->count(), 3);
        $this->assertEquals($workers[0][User::NAME], $workerOne[User::NAME]);
        $this->assertEquals($workers[1][User::NAME], $workerTow[User::NAME]);
        $this->assertEquals($workers[2][User::NAME], $workerThree[User::NAME]);
    }

    public function test_givenNonWorkerUser_whenRequestWorkerExtraHours_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::WORKER_EXTRA_HOURS));
        //Then
        $response->assertForbidden();
    }

    public function test_givenExtraHours_whenWorkersRequestHisExtraHours_thenWillReturned() {
        $this->withoutExceptionHandling();
        //Given
        $authUser=$this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $worker = TestUtil::createUser([
                    User::NAME => 'Ali'
        ]);
        //query return all  extrahours of authenticated user
        $extraHoursOne = ExtraHours::create([
                    ExtraHours::USER_ID => $authUser[User::ID],
                    ExtraHours::EXTRA_HOURS => 1,
        ]);
        
        $extraHoursTow = ExtraHours::create([
                    ExtraHours::USER_ID => $authUser[User::ID],
                    ExtraHours::EXTRA_HOURS => 3,
        ]);
        $extraHoursTow->delete();

        ExtraHours::create([
            ExtraHours::USER_ID => $worker[User::ID],
            ExtraHours::EXTRA_HOURS => 4,
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::WORKER_EXTRA_HOURS));
        //Then
        $extrahours = $response->viewData(GlobalVariables::EXTRA_HOURS);
        $response->assertViewIs(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW);
        $this->assertTrue($extrahours instanceof Paginator);
        $this->assertExtraHoursEquals($extrahours[0], $extraHoursOne, $authUser[User::NAME]);
        $this->assertExtraHoursEquals($extrahours[1], $extraHoursTow, $authUser[User::NAME]);
        $this->assertEquals($extrahours->count(), 2);
    }

    //Utils 
    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), [User::EMAIL => $user[User::EMAIL], User::PASSWORD
            => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function assertExtraHoursEquals($extraHours, $_extraHours, $userName) {
        $this->assertEquals($extraHours[ExtraHours::USER_ID], $_extraHours[ExtraHours::USER_ID]);
        $this->assertEquals($extraHours[ExtraHours::EXTRA_HOURS], $_extraHours[ExtraHours::EXTRA_HOURS]);
        $this->assertEquals($extraHours[ExtraHours::USER][User::NAME], $userName);
    }

    private function assertDeleted($id) {
        $this->assertNull(ExtraHours::find($id));
        $this->assertNotNull(ExtraHours::onlyTrashed()->find($id));
    }

    public function assertRestored($id) {
        $this->assertNotNull(ExtraHours::find($id));
        $this->assertNull(ExtraHours::onlyTrashed()->find($id));
    }

    public function assertForcedDeleted($id) {
        $this->assertNull(ExtraHours::find($id));
        $this->assertNull(ExtraHours::onlyTrashed()->find($id));
    }

}
