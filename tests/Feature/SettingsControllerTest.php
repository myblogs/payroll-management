<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Tests\Feature\Utils\TestUtil;
use App\Utils\GlobalVariables;
use App\Models\Setting;
use App\Utils\Messages\ErrorMessages;
use App\Utils\Messages\SucessMessges;

class SettingsControllerTest extends TestCase {

    use RefreshDatabase;

    public function test_givenEmptyDataAndAccountantUser_whenSetSettings_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $emptySettings = [
            Setting::MAXIMUM_BORROWING => '',
            Setting::MAXIMUM_PREMIUMS_NUMBERS => '',
            Setting::MAXIMUM_VACATION_DAYS => '',
            Setting::FIRST_WEEKEND_DAY => '',
            Setting::SECOND_WEEKEND_DAY => '',
            Setting::MAX_EXTRA_HOURS => '',
            Setting::HOUR_VALUE => '',
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::SETTINGS_SET), $emptySettings);
        //Then
        $this->assertInvalidation($response, [
            Setting::MAXIMUM_BORROWING => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::MAXIMUM_VACATION_DAYS => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::FIRST_WEEKEND_DAY => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::SECOND_WEEKEND_DAY => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::MAX_EXTRA_HOURS => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Setting::HOUR_VALUE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidDataAndAccountantUser_whenSetSettings_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidSettings = [
            Setting::MAXIMUM_BORROWING => -1,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => -1,
            Setting::MAXIMUM_VACATION_DAYS => -1,
            Setting::MAX_EXTRA_HOURS => -1,
            Setting::HOUR_VALUE => -1,
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::SETTINGS_SET), $invalidSettings);
        //Then
        $this->assertInvalidation($response, [
            Setting::MAXIMUM_BORROWING => ErrorMessages::IS_LESS_THAN_ZERO,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => ErrorMessages::IS_LESS_THAN_ZERO,
            Setting::MAXIMUM_VACATION_DAYS => ErrorMessages::IS_LESS_THAN_ZERO,
            Setting::MAX_EXTRA_HOURS => ErrorMessages::IS_LESS_THAN_ZERO,
            Setting::HOUR_VALUE => ErrorMessages::IS_LESS_THAN_ZERO,
        ]);
    }

    public function test_2_givenInvalidDataAndAccountantUser_whenSetSettings_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidSettings = [
            Setting::MAXIMUM_BORROWING => 'non-numeric-value',
            Setting::MAXIMUM_PREMIUMS_NUMBERS => 'non-numeric-value',
            Setting::MAXIMUM_VACATION_DAYS => 'non-numeric-value',
            Setting::MAX_EXTRA_HOURS => 'non-numeric-value',
            Setting::HOUR_VALUE => 'non-numeric-value',
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::SETTINGS_SET), $invalidSettings);
        //Then
        $this->assertInvalidation($response, [
            Setting::MAXIMUM_BORROWING => ErrorMessages::IS_NOT_NUMBER,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => ErrorMessages::IS_NOT_NUMBER,
            Setting::MAXIMUM_VACATION_DAYS => ErrorMessages::IS_NOT_NUMBER,
            Setting::MAX_EXTRA_HOURS => ErrorMessages::IS_NOT_NUMBER,
            Setting::HOUR_VALUE => ErrorMessages::IS_NOT_NUMBER,
        ]);
    }

    public function test_givenValidDataAndAccountantUser_whenSetSettings_thenWillSet() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $existSetting = Setting::create([
                    Setting::MAXIMUM_BORROWING => 0.0,
                    Setting::MAXIMUM_PREMIUMS_NUMBERS => 0.0,
                    Setting::MAXIMUM_VACATION_DAYS => 0.0,
                    Setting::FIRST_WEEKEND_DAY => "Fri",
                    Setting::SECOND_WEEKEND_DAY => "Sat",
                    Setting::MAX_EXTRA_HOURS => 1,
                    Setting::HOUR_VALUE => 1,
        ]);
        $validSettings = [
            Setting::MAXIMUM_BORROWING => 1000.0,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => 2000.0,
            Setting::MAXIMUM_VACATION_DAYS => 3.0,
            Setting::FIRST_WEEKEND_DAY => "Wed",
            Setting::SECOND_WEEKEND_DAY => "Thu",
            Setting::MAX_EXTRA_HOURS => 2,
            Setting::HOUR_VALUE => 2,
        ];
        //When
        $response = $this->putJson(route(GlobalVariables::SETTINGS_SET), $validSettings);
        //Then
        $updatedSettings = Setting::find($existSetting->id);
        $this->assertSettingsEquals($updatedSettings, $validSettings);
        $this->assertEquals($response->getContent(), SucessMessges::SETTINGS_HAS_SET_SUCCESSFULLY);
    }

    public function test_givenSettingsDataAndWorkerUser_whenSetSettings_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->putJson(route(GlobalVariables::SETTINGS_SET));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenSettingsDataAndAccountantUser_whenRequestSettingsView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $_existSetting = Setting::create([
                    Setting::MAXIMUM_BORROWING => 0.0,
                    Setting::MAXIMUM_PREMIUMS_NUMBERS => 0.0,
                    Setting::MAXIMUM_VACATION_DAYS => 0.0,
                    Setting::FIRST_WEEKEND_DAY => "Fri",
                    Setting::SECOND_WEEKEND_DAY => "Sat",
                    Setting::MAX_EXTRA_HOURS => 2,
                    Setting::HOUR_VALUE => 2,
        ]);

        //When
        $response = $this->get(route(GlobalVariables::SETTINGS_EDIT));
        //Then
        $response->assertViewHas(GlobalVariables::SETTINGS, $_existSetting);
    }

    public function test_givenSettingsDataAndWorkerUser_whenRequestSettingsView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->get(route(GlobalVariables::SETTINGS_EDIT));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    //Utils
    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), [User::EMAIL => $user[User::EMAIL], User::PASSWORD => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function assertInvalidation($response, $data) {
        $errors = (array) $response->getData()->errors;
        foreach ($data as $field => $message) {
            $this->assertEquals($errors[$field][0], TestUtil::getErrorMessage($message, [$field]));
        }
    }

    private function assertSettingsEquals($settings, $_settings) {
        $this->assertEquals($settings[Setting::MAXIMUM_BORROWING], $_settings[Setting::MAXIMUM_BORROWING]);
        $this->assertEquals($settings[Setting::MAXIMUM_PREMIUMS_NUMBERS], $_settings[Setting::MAXIMUM_PREMIUMS_NUMBERS]);
        $this->assertEquals($settings[Setting::MAXIMUM_VACATION_DAYS], $_settings[Setting::MAXIMUM_VACATION_DAYS]);
        $this->assertEquals($settings[Setting::FIRST_WEEKEND_DAY], $_settings[Setting::FIRST_WEEKEND_DAY]);
        $this->assertEquals($settings[Setting::SECOND_WEEKEND_DAY], $_settings[Setting::SECOND_WEEKEND_DAY]);
        $this->assertEquals($settings[Setting::MAX_EXTRA_HOURS], $_settings[Setting::MAX_EXTRA_HOURS]);
        $this->assertEquals($settings[Setting::HOUR_VALUE], $_settings[Setting::HOUR_VALUE]);
    }

}
