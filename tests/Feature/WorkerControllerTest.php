<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Utils\GlobalVariables;
use App\Utils\UserUtils;
use Tests\Feature\Utils\TestUtil;
use Illuminate\Support\Facades\Auth;
use App\Models\Job;
use App\Models\Setting;

class WorkerControllerTest extends TestCase {

    use RefreshDatabase;

    const NON_EXIST_ID = 1000;

    private $response;

    public function setUp(): void {
        $this->response = null;
        parent::setUp();
    }

    public function test_givenEmptyWorkerDataAndAccountantUser_whenCreate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidWorker = TestUtil::getDefaultUserExcept([
                    User::NAME => '',
                    User::EMAIL => '',
                    User::PASSWORD => '',
                    User::JOB_ID => '',
                    User::WORKING_DATE => '',
                    User::SALARY => '',
                    User::START_CONTRACT_DATE => '',
                    User::END_CONTRACT_DATE => '',
        ]);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE), $invalidWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::NAME => 'The :attribute is required.',
            User::EMAIL => 'The :attribute is required.',
            User::PASSWORD => 'The :attribute is required.',
            User::JOB_ID => 'The :attribute is required.',
            User::WORKING_DATE => 'The :attribute is required.',
            User::SALARY => 'The :attribute is required.',
            User::START_CONTRACT_DATE => 'The :attribute is required.',
            User::END_CONTRACT_DATE => 'The :attribute is required.',
        ]);
    }

    public function test_1_givenInvalidWorkerDataAndAccountantUser_whenCreate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidWorker = TestUtil::getDefaultUserExcept([
                    User::EMAIL => 'wrongEmailFormat',
                    User::PASSWORD => '111',
                    User::JOB_ID => '-1',
                    User::WORKING_DATE => 'wrongDateFormat',
                    User::SALARY => '-1',
                    User::START_CONTRACT_DATE => 'wrongDateFormat',
                    User::END_CONTRACT_DATE => 'wrongDateFormat',
        ]);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE), $invalidWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::EMAIL => 'The :attribute must be a valid email address.',
            User::PASSWORD => 'The :attribute must be at least 8 characters.',
            User::JOB_ID => 'The :attribute must be at least 0.',
            User::WORKING_DATE => 'The :attribute does not match the format Y-m-d.',
            User::SALARY => 'The :attribute must be at least 0.',
            User::START_CONTRACT_DATE => 'The :attribute does not match the format Y-m-d.',
            User::END_CONTRACT_DATE => 'The :attribute does not match the format Y-m-d.',
        ]);
    }

    public function test_2_givenInvalidWorkerDataAndAccountantUser_whenCreate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidWorker = TestUtil::getDefaultUserExcept([
                    User::PASSWORD => '11111111',
                    User::PASSWORD_CONFIRMATION => '11122112',
                    User::JOB_ID => 'non-numeric_job_id',
                    User::WORKING_DATE => '2019-02-01',
                    User::SALARY => 'non_numeric_salary',
                    User::START_CONTRACT_DATE => '2019-02-01',
                    User::END_CONTRACT_DATE => '2019-02-02',
        ]);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE), $invalidWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::PASSWORD => 'The :attribute does not match.',
            User::JOB_ID => 'The :attribute must be a number.',
            User::SALARY => 'The :attribute must be a number.',
            User::WORKING_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
            User::START_CONTRACT_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
            User::END_CONTRACT_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
        ]);
    }

    public function test_3_givenInvalidWorkerAndAccountantUser_whenCreate_thenWillGiveValidationException() {
        //Given
        $accountantUser = $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidWorker = TestUtil::getDefaultUserExcept([
                    User::EMAIL => $accountantUser[User::EMAIL],
                    User::START_CONTRACT_DATE => date('Y-m-d'),
                    User::END_CONTRACT_DATE => date('Y-m-d'),
        ]);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE), $invalidWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::EMAIL => 'The :attribute has already been taken.',
            User::END_CONTRACT_DATE . ',' . User::START_CONTRACT_DATE => 'The :attribute must be a date after :attribute.'
        ]);
    }

    public function test_givenValidDataAndAccuntantUser_whenCreate_thenWillCreated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $job = Job::create([Job::NAME => 'job']);
        $validWorker = TestUtil::getDefaultUserExcept([User::JOB_ID => $job->id]);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE), $validWorker);
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_MANAGING_VIEW);
        $createdWorker = User::find($this->response->viewData(GlobalVariables::WORKER)->id);
        $this->assertWorkerEquals($createdWorker, $validWorker, $job, true, true, false, false);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$validWorker], 1);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
    }

    public function test_givenValidDataAndWorkerUser_whenCreate_thenWillAbortUnthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_STORE));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenNonExistWorkerAndAccountantUser_whenRequestUpdateView_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_EDIT, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenExistWorkerAndAccountantUser_whenRequestUpdateView_thenViewWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $job = Job::create([Job::NAME => 'job']);
        $existedWorker = TestUtil::createUser([User::JOB_ID => $job->id]);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_EDIT, $existedWorker->id));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_UPDATE_VIEW);
        $_existWorker = $this->response->viewData(GlobalVariables::WORKER);
        $this->assertWorkerEquals($_existWorker, $existedWorker, $job, false, true, true, true);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
        $this->assertEquals($this->response->viewData(GlobalVariables::COUNTRIES)[0], 'Egypt');
    }

    public function test_givenWorkerAndWokrerUser_whenRequestUpdateView_thenAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_EDIT, 1));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenEmptyUserDataAndAccountantUser_whenUpdate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::NAME => '',
                    User::JOB_ID => '',
                    User::WORKING_DATE => '',
                    User::SALARY => '',
                    User::START_CONTRACT_DATE => '',
                    User::END_CONTRACT_DATE => '',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, 1), $invalidUpdatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::NAME => 'The :attribute is required.',
            User::JOB_ID => 'The :attribute is required.',
            User::WORKING_DATE => 'The :attribute is required.',
            User::SALARY => 'The :attribute is required.',
            User::START_CONTRACT_DATE => 'The :attribute is required.',
            User::END_CONTRACT_DATE => 'The :attribute is required.',
        ]);
    }

    public function test_1_givenInvalidWorkerDataAndAccountantUser_whenUpdate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::JOB_ID => -1,
                    User::SALARY => -1,
                    User::PHONE => '1111',
                    User::WORKING_DATE => 'wrong-date-format',
                    User::START_CONTRACT_DATE => 'wrong-date-format',
                    User::END_CONTRACT_DATE => 'wrong-date-format',
                    User::PASSPORT_RELEASE_DATE => 'wrong-date-format',
                    User::PASSPORT_EXPIRY_DATE => 'wrong-date-format',
                    User::RESIDENCY_RELEASE_DATE => 'wrong-date-format',
                    User::RESIDENCY_EXPIRY_DATE => 'wrong-date-format',
                    User::WORK_LICENSE_RELEASE_DATE => 'wrong-date-format',
                    User::WORK_LICENSE_EXPIRY_DATE => 'wrong-date-format',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, 1), $invalidUpdatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::JOB_ID => 'The :attribute must be at least 0.',
            User::SALARY => 'The :attribute must be at least 0.',
            User::PHONE => 'The :attribute must be 11 digits.',
            User::WORKING_DATE => 'The :attribute does not match the format Y-m-d.',
            User::START_CONTRACT_DATE => 'The :attribute does not match the format Y-m-d.',
            User::END_CONTRACT_DATE => 'The :attribute does not match the format Y-m-d.',
            User::PASSPORT_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::PASSPORT_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
            User::RESIDENCY_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::RESIDENCY_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
            User::WORK_LICENSE_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::WORK_LICENSE_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
        ]);
    }

    public function test_2_givenInvalidWorkerAndAccountantUser_whenUpdate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::WORKING_DATE => '2019-01-01',
                    User::SALARY => 'non-numeric-salary',
                    User::JOB_ID => 'non-numeric-job-id',
                    User::PHONE => 'non-numeric-phone',
                    User::START_CONTRACT_DATE => '2019-01-01',
                    User::END_CONTRACT_DATE => '2019-02-01',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, 1), $invalidUpdatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::WORKING_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
            User::SALARY => 'The :attribute must be a number.',
            User::JOB_ID => 'The :attribute must be a number.',
            User::PHONE => 'The :attribute must be a number.',
            User::START_CONTRACT_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
            User::END_CONTRACT_DATE => 'The :attribute must be a date after or equal to ' . date('Y-m-d') . '.',
        ]);
    }

    public function test_3_givenInvalidWorkerAndAccountantUser_whenUpdate_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $invalidUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::START_CONTRACT_DATE => '2019-01-01',
                    User::END_CONTRACT_DATE => '2019-01-01',
                    User::PASSPORT_RELEASE_DATE => '2019-01-01',
                    User::PASSPORT_EXPIRY_DATE => '2019-01-01',
                    User::RESIDENCY_RELEASE_DATE => '2019-01-01',
                    User::RESIDENCY_EXPIRY_DATE => '2019-01-01',
                    User::WORK_LICENSE_RELEASE_DATE => '2019-01-01',
                    User::WORK_LICENSE_EXPIRY_DATE => '2019-01-01',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, 1), $invalidUpdatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::END_CONTRACT_DATE . ',' . User::START_CONTRACT_DATE => "The :attribute must be a date after :attribute.",
            User::PASSPORT_EXPIRY_DATE . ',' . User::PASSPORT_RELEASE_DATE => "The :attribute must be a date after :attribute.",
            User::RESIDENCY_EXPIRY_DATE . ',' . User::RESIDENCY_RELEASE_DATE => "The :attribute must be a date after :attribute.",
            User::WORK_LICENSE_EXPIRY_DATE . ',' . User::WORK_LICENSE_RELEASE_DATE => "The :attribute must be a date after :attribute.",
        ]);
    }

    public function test_givenValidDataAndAccuntantUser_whenUpdate_thenWillUpdated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $jobOne = Job::create([Job::NAME => 'job-one']);
        $jobTow = Job::create([Job::NAME => 'job-tow']);
        $worker = TestUtil::createUser([User::JOB_ID => $jobOne->id]);
        $validUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::NAME => 'updated_name',
                    User::JOB_ID => $jobTow->id,
                    User::INSURANCE_NUMBER => '1111',
                    User::WORKING_DATE => date('Y-m-d'),
                    User::SALARY => 100,
                    User::START_CONTRACT_DATE => date('Y-m-d'),
                    User::END_CONTRACT_DATE => TestUtil::addToDate(date('Y-m-d'), '+1 day'),
                    User::NATIONALITY => 'Libya',
                    User::BIRTH_DATE => '2000-09-19',
                    User::ADDRESS => 'updated_address',
                    User::PHONE => '01122222222',
                    User::PASSPORT_NUMBER => 'updated_passport_no',
                    User::PASSPORT_RELEASE_DATE => '2019-03-01',
                    User::PASSPORT_EXPIRY_DATE => '2019-04-01',
                    User::PASSPORT_RELEASE_PLACE => 'updated_passport_place',
                    User::RESIDENCY_NUMBER => 'updated_residency_no',
                    User::RESIDENCY_RELEASE_DATE => '2019-03-01',
                    User::RESIDENCY_EXPIRY_DATE => '2019-04-01',
                    User::RESIDENCY_RELEASE_PLACE => 'updated_residency_place',
                    User::WORK_LICENSE_NUMBER => 'updated_work_license_no',
                    User::WORK_LICENSE_RELEASE_DATE => '2019-03-01',
                    User::WORK_LICENSE_EXPIRY_DATE => '2019-04-01',
                    User::WORK_LICENSE_RELEASE_PLACE => 'updated_work_license_place',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, $worker->id), $validUpdatedWorker);
        $updatedWorker = User::find($worker->id);
        //Then
        $this->assertWorkerEquals($updatedWorker, $validUpdatedWorker, $jobTow, false, true, true, true);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$validUpdatedWorker], 1);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$jobOne, $jobTow], 2);
    }

    public function test_givenValidDataAndWorkerUser_whenUpdate_thenWillAbortUnthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When 
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE, 1));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestWorkers_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $existUser = TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        $job = Job::create([Job::NAME => 'job']);
        //When
        $this->response = $this->getJson(route(GlobalVariables::WORKERS_INDEX));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_MANAGING_VIEW);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$existUser], 1);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
    }

    public function test_givenWorkerUser_whenRequestWorkers_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->getJson(route(GlobalVariables::WORKERS_INDEX));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistWorkerAndAccountantUser_whenDeleteWorker_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $worker = TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_DELETE, $worker->id));
        //Then
        $this->assertNotNull(User::withTrashed()->find($worker->id));
        $this->assertNull(User::find($worker->id));
    }

    public function test_givenNonExistWorkerAndAccountantUser_whenDeleteWorker_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_DELETE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerUser_whenDeleteWorker_thenWillAbortNotUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_DELETE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistWorkerAndAccountantUser_whenRestoreWorker_thenWillRestored() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $worker = TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        $worker->delete();
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_RESTORE, $worker->id));
        //Then
        $this->assertNull(User::onlyTrashed()->find($worker->id));
        $this->assertNotNull(User::find($worker->id));
    }

    public function test_givenNonExistWorkerAndAccountantUser_whenRestoreWorker_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_RESTORE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerAndWorkerUser_whenRestoreWorker_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_RESTORE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestDeletedWorkers_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        $deletedWorker = TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        $deletedWorker->delete();
        $job = Job::create([Job::NAME => 'job']);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_ONLY_DELETED));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_MANAGING_VIEW);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$deletedWorker], 1);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
    }

    public function test_givenWorkerUser_whenRequestDeletedWorkers_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_ONLY_DELETED));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenRequestConfirmDeleteView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_CONFIRM_DELETE, 1));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_CONFIRM_DELETE_VIEW);
        $this->assertEquals($this->response->viewData(User::ID), 1);
    }

    public function test_givenWorkerUser_whenRequestConfirmDeleteView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_CONFIRM_DELETE, 1));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistWorkerAndAccountantUser_whenForceDelete_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE])->delete();
        $worker = TestUtil::createUser([User::TYPE => GlobalVariables::WORKER_TYPE]);
        $worker->delete();
        $job = Job::create([Job::NAME => 'job']);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_FORCE_DELETE, $worker->id));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_MANAGING_VIEW);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$worker], 1);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
        $this->assertNull(User::find($worker->id));
        $this->assertNull(User::onlyTrashed()->find($worker->id));
    }

    public function test_givenNonExistWorkerAndAccountantUser_whenForceDelete_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_FORCE_DELETE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::NOT_FOUND);
    }

    public function test_givenWorkerAndWorkerUser_whenForceDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_FORCE_DELETE, self::NON_EXIST_ID));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenWorkersAndAccountantUser_whenSearchWorkers_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $workerOne = TestUtil::createUser([User::NAME => 'name', User::TYPE => GlobalVariables::WORKER_TYPE]);
        $workerTow = TestUtil::createUser([User::NAME => 'name', User::TYPE => GlobalVariables::WORKER_TYPE]);
        $workerTow->delete();
        TestUtil::createUser([User::NAME => 'name', User::TYPE => GlobalVariables::ACOOUNTANT_TYPE]);
        TestUtil::createUser([User::NAME => 'xxx', User::TYPE => GlobalVariables::WORKER_TYPE]);
        $job = Job::create([Job::NAME => 'job']);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_SEARCH), [User::NAME => 'name']);
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_MANAGING_VIEW);
        $this->assertViewDataEquals($this->response, GlobalVariables::WORKERS, [$workerOne, $workerTow], 2);
        $this->assertViewDataEquals($this->response, GlobalVariables::JOBS, [$job], 1);
    }

    public function test_givenWorkersAndWorkerUser_whenSearchWorkers_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $this->response = $this->postJson(route(GlobalVariables::WORKERS_SEARCH), [User::NAME => 'name']);
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenWorkerUser_whenRequestEditProfileView_thenViewWillDisplayed() {
        //Given
        $this->withoutExceptionHandling();
        $job = Job::create([Job::NAME => 'job']);
        $worker = TestUtil::createUser([User::JOB_ID => $job->id]);
        $this->postJson(route('login'), [User::EMAIL => $worker[User::EMAIL], User::PASSWORD => '12345678']);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_EDIT_PROFILE));
        //Then
        $this->response->assertViewIs(GlobalVariables::WORKERS_UPDATE_VIEW);
        $_worker = $this->response->viewData(GlobalVariables::WORKER);
        $this->assertWorkerEquals($_worker, $worker, $job, false, true, true, true);
        $this->assertEquals($this->response->viewData(GlobalVariables::COUNTRIES)[0], 'Egypt');
    }

    public function test_givenAccountantUser_whenRequestEditProfileView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->get(route(GlobalVariables::WORKERS_EDIT_PROFILE));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_1_givenInvalidWorkerDataAndWorkerUser_whenUpdateProfile_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::PASSPORT_RELEASE_DATE => 'wrong-date-format',
                    User::PASSPORT_EXPIRY_DATE => 'wrong-date-format',
                    User::BIRTH_DATE => 'wrong-date-format',
                    User::RESIDENCY_RELEASE_DATE => 'wrong-date-format',
                    User::RESIDENCY_EXPIRY_DATE => 'wrong-date-format',
                    User::WORK_LICENSE_RELEASE_DATE => 'wrong-date-format',
                    User::WORK_LICENSE_EXPIRY_DATE => 'wrong-date-format',
                    User::PHONE => '1111',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE_PROFILE), $invalidUpdatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::PASSPORT_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::PASSPORT_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
            User::BIRTH_DATE => 'The :attribute does not match the format Y-m-d.',
            User::RESIDENCY_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::RESIDENCY_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
            User::WORK_LICENSE_RELEASE_DATE => 'The :attribute does not match the format Y-m-d.',
            User::WORK_LICENSE_EXPIRY_DATE => 'The :attribute does not match the format Y-m-d.',
            User::PHONE => 'The :attribute must be 11 digits.',
        ]);
    }

    public function test_2_givenInvalidWorkerDataAndWorkerUser_whenUpdateProfile_thenWillGiveValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $updatedWorker = TestUtil::getDefaultUserExcept([
                    User::PHONE => 'wrong-phone-format',
                    User::PASSPORT_RELEASE_DATE => '2019-01-01',
                    User::PASSPORT_EXPIRY_DATE => '2019-01-01',
                    User::RESIDENCY_RELEASE_DATE => '2019-01-01',
                    User::RESIDENCY_EXPIRY_DATE => '2019-01-01',
                    User::WORK_LICENSE_RELEASE_DATE => '2019-01-01',
                    User::WORK_LICENSE_EXPIRY_DATE => '2019-01-01',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE_PROFILE), $updatedWorker);
        //Then
        $this->assertInvalidation($this->response, [
            User::PHONE => 'The :attribute must be a number.',
            User::PASSPORT_EXPIRY_DATE . ',' . User::PASSPORT_RELEASE_DATE => 'The :attribute must be a date after :attribute.',
            User::RESIDENCY_EXPIRY_DATE . ',' . User::RESIDENCY_RELEASE_DATE => 'The :attribute must be a date after :attribute.',
            User::WORK_LICENSE_EXPIRY_DATE . ',' . User::WORK_LICENSE_RELEASE_DATE => 'The :attribute must be a date after :attribute.',
        ]);
    }

    public function test_givenValidDataAndWorkerUser_whenUpdateProfile_thenWillUpdated() {
        //Given
        $job = Job::create([Job::NAME => 'job']);
        $worker = TestUtil::createUser([User::JOB_ID => $job->id]);
        $this->postJson(route('login'), [User::EMAIL => $worker[User::EMAIL], User::PASSWORD => '12345678']);
        $validUpdatedWorker = TestUtil::getDefaultUserExcept([
                    User::NAME => 'updated_name',
                    User::NATIONALITY => 'Libya',
                    User::BIRTH_DATE => '2000-09-19',
                    User::ADDRESS => 'updated_address',
                    User::PHONE => '01122222222',
                    User::PASSPORT_NUMBER => 'updated_passport_no',
                    User::PASSPORT_RELEASE_DATE => '2019-03-01',
                    User::PASSPORT_EXPIRY_DATE => '2019-04-01',
                    User::PASSPORT_RELEASE_PLACE => 'updated_passport_place',
                    User::RESIDENCY_NUMBER => 'updated_residency_no',
                    User::RESIDENCY_RELEASE_DATE => '2019-03-01',
                    User::RESIDENCY_EXPIRY_DATE => '2019-04-01',
                    User::RESIDENCY_RELEASE_PLACE => 'updated_residency_place',
                    User::WORK_LICENSE_NUMBER => 'updated_work_license_no',
                    User::WORK_LICENSE_RELEASE_DATE => '2019-03-01',
                    User::WORK_LICENSE_EXPIRY_DATE => '2019-04-01',
                    User::WORK_LICENSE_RELEASE_PLACE => 'updated_work_license_place',
        ]);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE_PROFILE), $validUpdatedWorker);
        //Then
        $_worker = User::find($worker->id);
        $this->response->assertViewIs('home');
        $this->assertWorkerEquals($_worker, $validUpdatedWorker, $job, FALSE, false, true, true);
    }

    public function test_givenValidDataAndAccountantUser_whenUpdateProfile_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $this->response = $this->putJson(route(GlobalVariables::WORKERS_UPDATE_PROFILE));
        //Then
        $this->response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }
    //utils
    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), ['email' => $user['email'], 'password' => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function assertInvalidation($response, $data) {
        $errors = (array) $response->getData()->errors;
        foreach ($data as $field => $message) {
            if (strpos($field, ',') !== false) {
                $fields = explode(',', $field);
                $this->assertEquals($errors[$fields[0]][0], TestUtil::getErrorMessage($message, $fields));
            } else {
                if ($field === User::PASSWORD && $message === 'The :attribute does not match.') {
                    $this->assertEquals($errors[$field][0], TestUtil::getErrorMessage($message, [User::PASSWORD_CONFIRMATION]));
                } else {
                    $this->assertEquals($errors[$field][0], TestUtil::getErrorMessage($message, [$field]));
                }
            }
        }
    }

    public function assertWorkerEquals($worker, $_worker, $job, $isRegisterWorkerSection, $isWorkingFinancialSection
    , $isPersonalSection, $isDocumentsSection) {
        $this->assertEquals($worker[User::NAME], $_worker[User::NAME]);
        if ($isRegisterWorkerSection) {
            $this->assertTrue(Auth::attempt([User::EMAIL => $_worker[User::EMAIL], User::PASSWORD => $_worker[User::PASSWORD]]));
        }
        if ($isWorkingFinancialSection) {
            $this->assertEquals($worker[User::JOB][Job::NAME], $job[Job::NAME]);
            $this->assertEquals($worker[User::JOB_ID], $_worker[User::JOB_ID]);
            $this->assertEquals($worker[User::INSURANCE_NUMBER], $_worker[User::INSURANCE_NUMBER]);
            $this->assertEquals($worker[User::WORKING_DATE], $_worker[User::WORKING_DATE]);
            $this->assertEquals($worker[User::SALARY], $_worker[User::SALARY]);
            $this->assertEquals($worker[User::START_CONTRACT_DATE], $_worker[User::START_CONTRACT_DATE]);
            $this->assertEquals($worker[User::END_CONTRACT_DATE], $_worker[User::END_CONTRACT_DATE]);
            $this->assertEquals($worker[User::TYPE], GlobalVariables::WORKER_TYPE);
        }
        if ($isPersonalSection) {
            $this->assertEquals($worker[User::NATIONALITY], $_worker[User::NATIONALITY]);
            $this->assertEquals($worker[User::BIRTH_DATE], $_worker[User::BIRTH_DATE]);
            $this->assertEquals($worker[User::ADDRESS], $_worker[User::ADDRESS]);
            $this->assertEquals($worker[User::PHONE], $_worker[User::PHONE]);
        }
        if ($isDocumentsSection) {
            $this->assertEquals($worker[User::PASSPORT_NUMBER], $_worker[User::PASSPORT_NUMBER]);
            $this->assertEquals($worker[User::PASSPORT_RELEASE_DATE], $_worker[User::PASSPORT_RELEASE_DATE]);
            $this->assertEquals($worker[User::PASSPORT_RELEASE_PLACE], $_worker[User::PASSPORT_RELEASE_PLACE]);
            $this->assertEquals($worker[User::RESIDENCY_NUMBER], $_worker[User::RESIDENCY_NUMBER]);
            $this->assertEquals($worker[User::RESIDENCY_RELEASE_DATE], $_worker[User::RESIDENCY_RELEASE_DATE]);
            $this->assertEquals($worker[User::RESIDENCY_EXPIRY_DATE], $_worker[User::RESIDENCY_EXPIRY_DATE]);
            $this->assertEquals($worker[User::RESIDENCY_RELEASE_PLACE], $_worker[User::RESIDENCY_RELEASE_PLACE]);
            $this->assertEquals($worker[User::WORK_LICENSE_NUMBER], $_worker[User::WORK_LICENSE_NUMBER]);
            $this->assertEquals($worker[User::WORK_LICENSE_RELEASE_DATE], $_worker[User::WORK_LICENSE_RELEASE_DATE]);
            $this->assertEquals($worker[User::WORK_LICENSE_EXPIRY_DATE], $_worker[User::WORK_LICENSE_EXPIRY_DATE]);
            $this->assertEquals($worker[User::WORK_LICENSE_RELEASE_PLACE], $_worker[User::WORK_LICENSE_RELEASE_PLACE]);
        }
    }

    public function assertViewDataEquals($response, $viewData, $data, $size) {
        if ($viewData === GlobalVariables::JOBS) {
            $workers = $response->viewData($viewData)->toArray();
            $name = Job::NAME;
        } else if ($viewData === GlobalVariables::WORKERS) {
            $workers = $response->viewData($viewData)->toArray()['data'];
            $name = User::NAME;
        }
        for ($i = 0; $i < $size; $i++) {
            $this->assertEquals($workers[$i][$name], $data[$i][$name]);
        }
        $this->assertEquals(count($workers), $size);
    }
    

}
