<?php

namespace Tests\Feature\Utils;

use App\Utils\GlobalVariables;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Setting;
use App\Models\FinancialVacation;
use Tests\TestCase;

class TestUtil extends TestCase {

    private static function str_replace_first($search, $replace, $subject) {
        $pos = strpos($subject, $search);
        if ($pos !== false) {
            return substr_replace($subject, $replace, $pos, strlen($search));
        }
        return $subject;
    }

    public static function getErrorMessage($message, $fields) {
        foreach ($fields as $field) {
            $field = str_replace("_", ' ', $field);
            $message = self::str_replace_first(":attribute", "$field", $message);
        }
        return $message;
    }

    public static function createUser($userData = array(), $isRegisteredMethod = false) {
        if ($isRegisteredMethod) {
            $userData[User::EMAIL] = "another_email@yahoo.com";
        }
        $user = self::getDefaultUserExcept($userData);
        $user[User::PASSWORD] = Hash::make($user[User::PASSWORD]);
        $user[User::TYPE] = isset($userData[User::TYPE]) ? $userData[User::TYPE] : GlobalVariables::WORKER_TYPE;
        return User::create($user);
    }

    public static function addToDate($date, $duration) {
        return date('Y-m-d', strtotime("$date $duration"));
    }

    public static function getDefaultUserExcept($userData = array()) {
        return [
            User::NAME => isset($userData[User::NAME]) ? $userData[User::NAME] : 'name',
            User::EMAIL => isset($userData[User::EMAIL]) ? $userData[User::EMAIL] : 'email@yahoo.com',
            User::PASSWORD => isset($userData[User::PASSWORD]) ? $userData[User::PASSWORD] : '12345678',
            User::PASSWORD_CONFIRMATION => isset($userData[User::PASSWORD_CONFIRMATION]) ?
            $userData[User::PASSWORD_CONFIRMATION] : (isset($userData[User::PASSWORD]) ? $userData[User::PASSWORD] : '12345678'),
            User::JOB_ID => isset($userData[User::JOB_ID]) ? $userData[User::JOB_ID] : null,
            User::INSURANCE_NUMBER => isset($userData[User::INSURANCE_NUMBER]) ? $userData[User::INSURANCE_NUMBER] : '11',
            User::WORKING_DATE => isset($userData[User::WORKING_DATE]) ? $userData[User::WORKING_DATE] : date('Y-m-d'),
            User::SALARY => isset($userData[User::SALARY]) ? $userData[User::SALARY] : '1.00',
            User::START_CONTRACT_DATE => isset($userData[User::START_CONTRACT_DATE]) ? $userData[User::START_CONTRACT_DATE] : date('Y-m-d'),
            User::END_CONTRACT_DATE => isset($userData[User::END_CONTRACT_DATE]) ? $userData[User::END_CONTRACT_DATE] : self::addToDate(date('Y-m-d'), '+1 day'),
            User::NATIONALITY => isset($userData[User::NATIONALITY]) ? $userData[User::NATIONALITY] : 'Egypt',
            User::BIRTH_DATE => isset($userData[User::BIRTH_DATE]) ? $userData[User::BIRTH_DATE] : '1995-01-01',
            User::ADDRESS => isset($userData[User::ADDRESS]) ? $userData[User::ADDRESS] : 'address',
            User::PHONE => isset($userData[User::PHONE]) ? $userData[User::PHONE] : '11111111111',
            User::PASSPORT_NUMBER => isset($userData[User::PASSPORT_NUMBER]) ? $userData[User::PASSPORT_NUMBER] : '1221',
            User::PASSPORT_RELEASE_DATE => isset($userData[User::PASSPORT_RELEASE_DATE]) ? $userData[User::PASSPORT_RELEASE_DATE] : '2019-01-01',
            User::PASSPORT_EXPIRY_DATE => isset($userData[User::PASSPORT_EXPIRY_DATE]) ? $userData[User::PASSPORT_EXPIRY_DATE] : '2019-01-02',
            User::PASSPORT_RELEASE_PLACE => isset($userData[User::PASSPORT_RELEASE_PLACE]) ? $userData[User::PASSPORT_RELEASE_PLACE] : 'passport_release_place',
            User::RESIDENCY_NUMBER => isset($userData[User::RESIDENCY_NUMBER]) ? $userData[User::RESIDENCY_NUMBER] : '1111',
            User::RESIDENCY_RELEASE_DATE => isset($userData[User::RESIDENCY_RELEASE_DATE]) ? $userData[User::RESIDENCY_RELEASE_DATE] : '2019-01-01',
            User::RESIDENCY_EXPIRY_DATE => isset($userData[User::RESIDENCY_EXPIRY_DATE]) ? $userData[User::RESIDENCY_EXPIRY_DATE] : '2019-01-02',
            User::RESIDENCY_RELEASE_PLACE => isset($userData[User::RESIDENCY_RELEASE_PLACE]) ? $userData[User::RESIDENCY_RELEASE_PLACE] : 'residency_release_place',
            User::WORK_LICENSE_NUMBER => isset($userData[User::WORK_LICENSE_NUMBER]) ? $userData[User::WORK_LICENSE_NUMBER] : '1111',
            User::WORK_LICENSE_RELEASE_DATE => isset($userData[User::WORK_LICENSE_RELEASE_DATE]) ? $userData[User::WORK_LICENSE_RELEASE_DATE] : '2019-01-01',
            User::WORK_LICENSE_EXPIRY_DATE => isset($userData[User::WORK_LICENSE_EXPIRY_DATE]) ? $userData[User::WORK_LICENSE_EXPIRY_DATE] : '2019-01-02',
            User::WORK_LICENSE_RELEASE_PLACE => isset($userData[User::WORK_LICENSE_RELEASE_PLACE]) ? $userData[User::WORK_LICENSE_RELEASE_PLACE] : 'work_license_release_place'
        ];
    }

    public static function getDefaultSettingsExcept($data = array()) {
        $settings[Setting::MAXIMUM_BORROWING] = isset($data[Setting::MAXIMUM_BORROWING]) ? $data[Setting::MAXIMUM_BORROWING] : 0.0;
        $settings[Setting::MAXIMUM_PREMIUMS_NUMBERS] = isset($data[Setting::MAXIMUM_PREMIUMS_NUMBERS]) ? $data[Setting::MAXIMUM_PREMIUMS_NUMBERS] : 1.0;
        $settings[Setting::MAXIMUM_VACATION_DAYS] = isset($data[Setting::MAXIMUM_VACATION_DAYS]) ? $data[Setting::MAXIMUM_VACATION_DAYS] :1;
        $settings[Setting::FIRST_WEEKEND_DAY] = isset($data[Setting::FIRST_WEEKEND_DAY]) ? $data[Setting::FIRST_WEEKEND_DAY] :"Fri";
        $settings[Setting::SECOND_WEEKEND_DAY] = isset($data[Setting::SECOND_WEEKEND_DAY]) ? $data[Setting::SECOND_WEEKEND_DAY] : "Sat";
        return $settings;
    }

    public static function createDefaultSttingsExcept($data = array()) {
        $settings = self::getDefaultSettingsExcept($data);
        return Setting::create($settings);
    }

    public static function getDefaultFinancialVacationsExcept($data = array()) {
        $financialVacation[FinancialVacation::SETTING_ID] = isset($data[FinancialVacation::SETTING_ID]) ? $data[FinancialVacation::SETTING_ID] : null;
        $financialVacation[FinancialVacation::DATE] = isset($data[FinancialVacation::DATE]) ? $data[FinancialVacation::DATE] : date('Y-m-d');
        $financialVacation[FinancialVacation::NAME] = isset($data[FinancialVacation::NAME]) ? $data[FinancialVacation::NAME] : "name";
        $financialVacation[FinancialVacation::DESCRIPTION] = isset($data[FinancialVacation::DESCRIPTION]) ? $data[FinancialVacation::DESCRIPTION] : "description";
        return $financialVacation;
    }
    
    public static function createDefaultFinancialVacationsExcept($data = array()) {
        $financialVacations= self::getDefaultFinancialVacationsExcept($data);
        return FinancialVacation::create($financialVacations);
    }

    public static function assertInvalidation($response, $data) {
        $errors = (array) $response->getData()->errors;
        foreach ($data as $field => $message) {
            if (strpos($field, ',') !== false) {
                $fields = explode(',', $field);
                self::assertEquals($errors[$fields[0]][0], self::getErrorMessage($message, $fields));
            } else {
                self::assertEquals($errors[$field][0], self::getErrorMessage($message, [$field]));
            }
        }
    }

}
