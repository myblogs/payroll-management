<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Feature\Utils\TestUtil;
use App\Models\FinancialVacation;
use App\Utils\GlobalVariables;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Utils\Messages\ErrorMessages;
use App\Utils\Messages\SucessMessges;
use App\Models\Setting;

class FinancialVacationsControllerTest extends TestCase {

    use RefreshDatabase;

    const SETTINGS_ID = 1;

    public function test_givenEmptyData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => '',
                    FinancialVacation::NAME => '',
                    FinancialVacation::DESCRIPTION => '',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::FINANCIAL_VACATIONS_STORE), $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            FinancialVacation::NAME => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            FinancialVacation::DESCRIPTION => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => 'Non valid date',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::FINANCIAL_VACATIONS_STORE), $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::IS_NOT_DATE,
        ]);
    }

    public function test_2_givenInvalidData_whenCreate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => '2019-01-01',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::FINANCIAL_VACATIONS_STORE), $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::BEFORE_CURRENT_DATE(),
        ]);
    }

    public function test_givenvalidData_whenCreate_thenWillCreated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept();
        //When
        $response = $this->postJson(route(GlobalVariables::FINANCIAL_VACATIONS_STORE), $financialVacation);
        //Then
        $createdFinancialVacation = FinancialVacation::get()->first();
        $successMessage = $response->getContent();
        $this->assertFinancialVacationEquals($financialVacation, $createdFinancialVacation);
        $this->assertEquals($successMessage, SucessMessges::CREARTED_SUCCESSFULLY);
    }

    public function test_givenNotAccountantUser_whenCreate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::FINANCIAL_VACATIONS_STORE));
        //Then
        $response->assertForbidden();
    }

    public function test_givenFinancialVacation_whenRequestUpdateViewWith_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $settings = TestUtil::createDefaultSttingsExcept();
        $existFinancialVacation = TestUtil::createDefaultFinancialVacationsExcept([
                    FinancialVacation::SETTING_ID => $settings[Setting::ID]
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_EDIT
                        , $existFinancialVacation[FinancialVacation::ID]));
        //Then
        $response->assertViewIs(GlobalVariables::FINANCIAL_VACATIONS_UPDATE_VIEW);
        $response->assertViewHas(GlobalVariables::FINANCIAL_VACATION, $existFinancialVacation);
    }

    public function test_givenNonExistFinancialVacation_whenRequestUpdateViewWith_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_EDIT
                        , 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenNonAccountantUser_whenRequestUpdateViewWith_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_EDIT
                        , 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenEmptyData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => '',
                    FinancialVacation::NAME => '',
                    FinancialVacation::DESCRIPTION => '',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE, 1)
                , $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            FinancialVacation::NAME => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            FinancialVacation::DESCRIPTION => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => 'Non valid date',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE, 1)
                , $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::IS_NOT_DATE,
        ]);
    }

    public function test_2_givenInvalidData_whenUpdate_thenWillThrowsValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => '2019-01-01',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE, 1)
                , $financialVacation);
        //Then
        TestUtil::assertInvalidation($response, [
            FinancialVacation::DATE => ErrorMessages::BEFORE_CURRENT_DATE(),
        ]);
    }

    public function test_givenvalidData_whenUpdate_thenWillUpdated() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $existFinancialVacation = TestUtil::createDefaultFinancialVacationsExcept();
        $validFinancialVacation = TestUtil::getDefaultFinancialVacationsExcept([
                    FinancialVacation::DATE => TestUtil::addToDate(date('Y-m-d'), '+1 month'),
                    FinancialVacation::NAME => 'Updated name',
                    FinancialVacation::DESCRIPTION => 'Updated desc',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE
                        , $existFinancialVacation[FinancialVacation::ID]), $validFinancialVacation);
        //Then
        $updatedFinancialVacation = FinancialVacation::get()->first();
        $successMessage = $response->getContent();
        $this->assertFinancialVacationEquals($validFinancialVacation, $updatedFinancialVacation);
        $this->assertEquals($successMessage, SucessMessges::UPDATED_SUCCESSFULLY);
    }

    public function test_givenNonAccountantUser_whenUpdate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->putJson(route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonAccountantUser_whenDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_DELETE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonExistFinancialVacation_whenDelete_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_DELETE, 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExistFinancialVacation_whenDelete_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::createDefaultFinancialVacationsExcept();
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_DELETE
                        , $financialVacation[FinancialVacation::ID]));
        //Then
        $this->assertDeleted($financialVacation[FinancialVacation::ID]);
    }

    public function test_givenNonAccountantUser_whenRestore_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_RESTORE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonExistFinancialVacation_whenRestore_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_RESTORE, 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExistFinancialVacation_whenRestore_thenWillRestored() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::createDefaultFinancialVacationsExcept();
        $financialVacation->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_RESTORE
                        , $financialVacation[FinancialVacation::ID]));
        //Then
        $this->assertRetrieved($financialVacation[FinancialVacation::ID]);
    }

    public function test_givenNonAccountantUser_whenRequestConfirmDeleteView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE
                        , [1, self::SETTINGS_ID]));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonExistFinancialVacation_whenRequestConfirmDeleteView_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE, [1, self::SETTINGS_ID]));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExistFinancialVacation_whenRequestConfirmDeleteView_thenWillDisplayed() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::createDefaultFinancialVacationsExcept();
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE
                        , [$financialVacation[FinancialVacation::ID], self::SETTINGS_ID]));
        //Then
        $response->assertViewIs(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE_VIEW);
        $response->assertViewHasAll([
            FinancialVacation::ID => $financialVacation[FinancialVacation::ID],
            FinancialVacation::SETTING_ID => self::SETTINGS_ID,
        ]);
    }

    public function test_givenNonAccountantUser_whenForceDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_FORCE_DELETE, 1));
        //Then
        $response->assertForbidden();
    }

    public function test_givenNonExistFinancialVacation_whenForceDelete_thenWillAbortNotFoundPage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_FORCE_DELETE, 1));
        //Then
        $response->assertNotFound();
    }

    public function test_givenExistFinancialVacation_whenForceDelete_thenWillDeleted() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $financialVacation = TestUtil::createDefaultFinancialVacationsExcept();
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_FORCE_DELETE
                        , $financialVacation[FinancialVacation::ID]));
        //Then
        $response->assertSee(SucessMessges::DELETED_SUCCESSFULLY);
    }

    public function test_givenNonAccountantUser_whenGetAllFinancialVacation_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL, self::SETTINGS_ID));
        //Then
        $response->assertForbidden();
    }

    public function test_givenAccountantUser_whenGetAllFinancialVacations_thenWillReturned() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        TestUtil::createDefaultFinancialVacationsExcept();
        TestUtil::createDefaultFinancialVacationsExcept()->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL, self::SETTINGS_ID));
        //Then
        $response->assertViewIs(GlobalVariables::FINANCIAL_VACATIONS_MANAGING_VIEW);
        $financialVacations = FinancialVacation::withTrashed()->simplePaginate(GlobalVariables::FINANCIAL_VACATIONS_PAGE_SIZE)
                ->fragment('table-data');
        $response->assertViewHasAll([
            GlobalVariables::FINANCIAL_VACATIONS => $financialVacations,
            FinancialVacation::SETTING_ID => self::SETTINGS_ID,
        ]);
        $this->assertEquals($financialVacations->count(), 2);
    }

    //Utils
    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), [User::EMAIL => $user[User::EMAIL], User::PASSWORD
            => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function assertFinancialVacationEquals($financialVacation, $_financialVacation) {
        $this->assertEquals($financialVacation[FinancialVacation::SETTING_ID]
                , $_financialVacation[FinancialVacation::SETTING_ID]);
        $this->assertEquals($financialVacation[FinancialVacation::NAME]
                , $_financialVacation[FinancialVacation::NAME]);
        $this->assertEquals($financialVacation[FinancialVacation::DATE]
                , $_financialVacation[FinancialVacation::DATE]);
        $this->assertEquals($financialVacation[FinancialVacation::DESCRIPTION]
                , $_financialVacation[FinancialVacation::DESCRIPTION]);
    }

    private function assertDeleted($id) {
        $this->assertNull(FinancialVacation::find($id));
        $this->assertNotNull(FinancialVacation::onlyTrashed()->find($id));
    }

    private function assertRetrieved($id) {
        $this->assertNotNull(FinancialVacation::find($id));
        $this->assertNull(FinancialVacation::onlyTrashed()->find($id));
    }

}
