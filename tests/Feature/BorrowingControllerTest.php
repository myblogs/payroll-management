<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Borrowing;
use App\Utils\GlobalVariables;
use App\Utils\Messages\ErrorMessages;
use Tests\Feature\Utils\TestUtil;
use App\User;
use App\Models\Setting;
use App\Utils\Messages\SucessMessges;

class BorrowingControllerTest extends TestCase {

    use RefreshDatabase;

    public function test_givenEmptyDataAndWorkerUser_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);

        $emptyBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::BORRWOING_VALUE => '',
            Borrowing::FISRT_DATE_PREMIUM => '',
            Borrowing::PREMIUMS_NUMBER => '',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $emptyBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::IS_NOT_DATE,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidDataAndWorkerUser_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::BORRWOING_VALUE => '-1',
            Borrowing::FISRT_DATE_PREMIUM => 'wrong-date-format',
            Borrowing::PREMIUMS_NUMBER => '0',
        ]);

        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::IS_LESS_THAN_ZERO,
            Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::IS_NOT_DATE,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::IS_LESS_THAN_ONE,
        ]);
    }

    public function test_2_givenInvalidDataAndWorkerUser_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::BORRWOING_VALUE => 'non-numeric-value',
            Borrowing::PREMIUMS_NUMBER => 'non-numeric-value',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::IS_NOT_NUMBER,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::IS_NOT_NUMBER,
        ]);
    }

    public function test_givenBorrowingExceedMaximumBorrowingAndWorkerUser_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::getDefaultSettingsExcept([Setting::MAXIMUM_BORROWING => 200]);
        $this->createBorrowing([Borrowing::BORRWOING_VALUE => 200]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([Borrowing::BORRWOING_VALUE => 200]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE . ',' . Setting::MAXIMUM_BORROWING => ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
        ]);
    }

    public function test_givenInvalidPremiumDateFormatAndWorkerUser_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::FISRT_DATE_PREMIUM => '2019-07-01',
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::FISRT_DATE_PREMIUM . ',' . Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::INVALID_FIRST_DATE_PREMIUM
        ]);
    }

    public function test_givenPremiumsNumbersExceedMaximumPremiumsNumber_whenCreate_thenWillThrowValidationException() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([Setting::MAXIMUM_PREMIUMS_NUMBERS => 3]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([Borrowing::PREMIUMS_NUMBER => 4]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::PREMIUMS_NUMBER . ',' . Setting::MAXIMUM_PREMIUMS_NUMBERS => ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
        ]);
    }

    public function test_givenWorkerBorrowBeforeAndWorkerUser_whenCreate_thenWillAbortBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept();
        $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser->id,
        ]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::USER_ID => $authenticatedUser->id
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $invalidBorrowing);
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::AUTHENTICATED_USER_BORROW_BEFORE, false);
    }

    public function test_givenValidDataAndWorkerUser_whenCreate_thenWillCreated() {
        //Given
        $this->withoutExceptionHandling();
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept();
        //To verify that query take for current date year
        $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 300,
            'created_at' => $this->modifyCurrentDate('+1 year')
        ])->delete();

        $validBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE), $validBorrowing);
        //Then
        $createdBorrowing = Borrowing::with(Borrowing::USER)->get()->first();
        $createdSuccessMessage = $response->getContent();
        $this->assertBorrowingEquals($createdBorrowing, $validBorrowing, $authenticatedUser);
        $this->assertEquals($createdSuccessMessage, SucessMessges::CREARTED_SUCCESSFULLY);
    }

    public function test_givenDataAndAccountantUser_whenCreate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_STORE));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdEqualtToWorkerUserId_whenRequestUpdateView_thenWillDisplayed() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $settings = TestUtil::createDefaultSttingsExcept([
                    Setting::MAXIMUM_PREMIUMS_NUMBERS => 2,
        ]);
        $_existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser->id,
        ]);
        //When
        $response = $this->get(route(GlobalVariables::BORROWINGS_EDIT, $_existBorrowing->id));
        //Then
        $existBorrowing = $response->viewData(GlobalVariables::BORROWING);
        $response->assertViewIs(GlobalVariables::BORROWINGS_UPDATE_VIEW);
        $this->assertBorrowingEquals($existBorrowing, $_existBorrowing, $authenticatedUser);
        $this->assertAvailablePremiumsNumbersEquals($response, GlobalVariables::PREMIUMS_NUMBERS
                , $settings[Setting::MAXIMUM_PREMIUMS_NUMBERS]);
    }

    public function test_givenNonExistBorrowingAndWorkerUser_whenRequestUpdateView_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->get(route(GlobalVariables::BORROWINGS_EDIT, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdNotEqualsWorkerUserId_whenRequestUpdateView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->get(route(GlobalVariables::BORROWINGS_EDIT, $existBorrowing->id));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenCurrentDateBiggerThanCreatedAtByAtleastOneMonth_whenRequestUpdateView_thenWillAbortBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            'created_at' => $this->modifyCurrentDate('-1 month'),
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_EDIT, $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        $this->assertEquals($response->getData()->message, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenEmptyDataAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        $emptyBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => '',
            Borrowing::FISRT_DATE_PREMIUM => '',
            Borrowing::PREMIUMS_NUMBER => '',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $emptyBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::ATTRIBUTE_IS_EMPTY,
            Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::IS_NOT_DATE,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::ATTRIBUTE_IS_EMPTY,
        ]);
    }

    public function test_1_givenInvalidDataAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => '-1',
            Borrowing::FISRT_DATE_PREMIUM => 'wrong-date-format',
            Borrowing::PREMIUMS_NUMBER => '0',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::IS_LESS_THAN_ZERO,
            Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::IS_NOT_DATE,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::IS_LESS_THAN_ONE,
        ]);
    }

    public function test_2_givenInvalidDataAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser->id,
            Borrowing::BORRWOING_VALUE => 'non-numeric-value',
            Borrowing::PREMIUMS_NUMBER => 'non-numeric-value',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE => ErrorMessages::IS_NOT_NUMBER,
            Borrowing::PREMIUMS_NUMBER => ErrorMessages::IS_NOT_NUMBER,
        ]);
    }

    public function test_givenBorrowingExceedMaximumBorrowingAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_BORROWING => 300
        ]);
        //created 2 borrowings to verify that system do not sum the borrowing value of updated borrowing
        $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 200,
        ]);

        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 200,
        ]);

        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 200,
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::BORRWOING_VALUE . ',' . Setting::MAXIMUM_BORROWING => ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
        ]);
    }

    public function test_givenInvalidPremiumDateFormatAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::FISRT_DATE_PREMIUM => '2019-07-01',
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE, $existBorrowing[Borrowing::ID]), $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::FISRT_DATE_PREMIUM . ',' . Borrowing::FISRT_DATE_PREMIUM => ErrorMessages::INVALID_FIRST_DATE_PREMIUM
        ]);
    }

    public function test_givenPremiumsNumbersExceedMaximumPremiumsNumber_whenUpdate_thenWillThrowValidationException() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_PREMIUMS_NUMBERS => 3,
        ]);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser->id,
        ]);
        $invalidBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser->id,
            Borrowing::PREMIUMS_NUMBER => 4,
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE, $existBorrowing[Borrowing::ID])
                , $invalidBorrowing);
        //Then
        $this->assertInvalidation($response, [
            Borrowing::PREMIUMS_NUMBER . ',' . Setting::MAXIMUM_PREMIUMS_NUMBERS => ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
        ]);
    }

    public function test_givenCurrentDateBiggerThanCreatedAtByAtleastOneMonth_whenUpdate_thenWillAbortBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept();

        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            'created_at' => $this->modifyCurrentDate('-1 month'),
        ]);

        $validBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser->id,
        ]);

        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $validBorrowing);
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenValidDataAndBorrowingUserIdEqualsAuthenticatedUserId_whenUpdate_thenWillUpdated() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        TestUtil::createDefaultSttingsExcept([
            Setting::MAXIMUM_BORROWING => 3000,
            Setting::MAXIMUM_PREMIUMS_NUMBERS => 10,
        ]);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $validBorrowing = $this->getDefualtBorrowingExcept([
            Borrowing::ID => $existBorrowing[Borrowing::ID],
            Borrowing::USER_ID => $authenticatedUser->id,
            Borrowing::BORRWOING_VALUE => 2000,
            Borrowing::FISRT_DATE_PREMIUM => $this->modifyDate($this->getValidDatePremiumFormat(), '+1 month'),
            Borrowing::PREMIUMS_NUMBER => 7,
        ]);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]), $validBorrowing);
        //Then
        $updatedBorrowing = Borrowing::find($existBorrowing[Borrowing::ID]);
        $updatedSuccessMessage = $response->getContent();
        $this->assertBorrowingEquals($updatedBorrowing, $validBorrowing, $authenticatedUser);
        $this->assertEquals($updatedSuccessMessage, SucessMessges::UPDATED_SUCCESSFULLY);
    }

    public function test_givenNonExistBorrowingAndWorkerUser_whenUpdate_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowingUserIdNotEqualsWorkerUserId_whenUpdate_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->putJson(route(GlobalVariables::BORROWINGS_UPDATE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdEqualsWorkerUserId_whenDelete_thenWillDeleted() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser->id,
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_DELETE, $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertNull(Borrowing::find($existBorrowing[Borrowing::ID]));
        $this->assertNotNull(Borrowing::withTrashed()->find($existBorrowing[Borrowing::ID]));
    }

    public function test_givenCurrentDateBiggerThanCreatedAtByAtleastOneMonth_whenDelete_thenWillAbortBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            'created_at' => $this->modifyCurrentDate('-1 month'),
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_DELETE, $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenNonExistBorrowingAndWorkerUser_whenDelete_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_DELETE, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowingUserIdNotEqualsWorkerUserId_whenDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdNotEqualsWorkerUserId_whenRestore_thenWillRestored() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_RESTORE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertNotNull(Borrowing::find($existBorrowing[Borrowing::ID]));
    }

    public function test_givenUserBorrowBefore_whenRestore_thenWillAbordBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_RESTORE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::AUTHENTICATED_USER_BORROW_BEFORE);
    }

    public function test_givenCurrentDateBiggerThanCreatedAtDateByAtLeastOneMonth_whenRestore_thenWillAbordBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            'created_at' => $this->modifyCurrentDate('-1 month'),
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_RESTORE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenNonExistDeletedBorrowing_whenRestore_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_RESTORE, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowingUserIdNotEqualsWorkerUserId_whenRestore_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_RESTORE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdNotEqualsWorkerUserId_whenRequestConfirmDeleteView_thenWillDisplayed() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_CONFIRM_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertViewIs(GlobalVariables::BORROWINGS_CONFIRM_DELETE_VIEW);
        $borrowingId = $response->viewData(Borrowing::ID);
        $this->assertEquals($borrowingId, $existBorrowing[Borrowing::ID]);
    }

    public function test_givenCurrentDateGreaterThanCreatedAtDateByAtLeastOneMonth_whenRequestConfirmDeleteView_thenWillAborBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            'created_at' => $this->modifyCurrentDate('-1 month'),
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_CONFIRM_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenNonExistDeletedBorrowing_whenRequestConfirmDeleteView_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_CONFIRM_DELETE, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowingUserIdNotEqualsWorkerUserId_whenRequestConfirmDeleteView_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_CONFIRM_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenExistBorrowingUserIdNotEqualsWorkerUserId_whenForceDelete_thenWillDeleted() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_FORCE_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertNull(Borrowing::find($existBorrowing[Borrowing::ID]));
        $this->assertNull(Borrowing::withTrashed()->find($existBorrowing[Borrowing::ID]));
        $this->assertEquals($response->getContent(), SucessMessges::DELETED_SUCCESSFULLY);
    }

    public function test_givenCurrentDateGreaterThanCreatedAtDateByAtLeastOneMonth_whenForceDelete_thenWillAborBadRequestPage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing([
            'created_at' => $this->modifyCurrentDate('-1 month'),
            Borrowing::USER_ID => $authenticatedUser[User::ID]
        ]);
        $existBorrowing->delete();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_FORCE_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $this->assertThrowingBadRequest($response, ErrorMessages::DATE_IS_EXPIRE);
    }

    public function test_givenNonExistDeletedBorrowing_whenForceDelete_thenWillAbortUnAuthorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_FORCE_DELETE, 1));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowingUserIdNotEqualsWorkerUserId_whenForceDelete_thenWillAbortUnathorizePage() {
        //Given
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $existBorrowing = $this->createBorrowing();
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_FORCE_DELETE
                        , $existBorrowing[Borrowing::ID]));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowings_whenWorkerRequestItsBorrowings_thenWillReturned() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        $settings = TestUtil::createDefaultSttingsExcept([
                    Setting::MAXIMUM_PREMIUMS_NUMBERS => 2,
        ]);

        $existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 110,
        ]);

        $this->createBorrowing([
            Borrowing::BORRWOING_VALUE => 120,
        ]);

        $_existBorrowing = $this->createBorrowing([
            Borrowing::USER_ID => $authenticatedUser[User::ID],
            Borrowing::BORRWOING_VALUE => 130,
        ]);

        $_existBorrowing->delete();

        $this->createBorrowing([
            Borrowing::BORRWOING_VALUE => 140,
        ]);

        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_INDEX));
        //Then
        $response->assertViewIs(GlobalVariables::BORROWINGS_MANAGING_VIEW);
        $this->assertViewDataEquals($response, GlobalVariables::BORROWINGS
                , [$_existBorrowing, $existBorrowing], [$authenticatedUser,$authenticatedUser], 2);
        $this->assertAvailablePremiumsNumbersEquals($response, GlobalVariables::PREMIUMS_NUMBERS
                , $settings[Setting::MAXIMUM_PREMIUMS_NUMBERS]);
        $this->assertEquals($response->viewData(GlobalVariables::IS_AUTHENTICATED_USER_BORROW), true);
    }

    public function test_givenBorrowingsAndAccountantUser_whenWorkerRequestItsBorrowings_thenWillAbortUnathurizePage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_INDEX));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenBorrowings_whenAccountantRequestBorrowings_thenWillReturned() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);
        $existBorrowing = Borrowing::create([
                    Borrowing::USER_ID => $authenticatedUser[User::ID],
                    Borrowing::BORRWOING_VALUE => 103,
                    Borrowing::FISRT_DATE_PREMIUM => '2019-01-01',
                    Borrowing::PREMIUMS_NUMBER => 12 - (date('m') + 1),
        ]);
        $_existBorrowing = Borrowing::create([
                    Borrowing::USER_ID => $authenticatedUser[User::ID],
                    Borrowing::BORRWOING_VALUE => 101,
                    Borrowing::FISRT_DATE_PREMIUM => '2019-01-01',
                    Borrowing::PREMIUMS_NUMBER => 12 - (date('m') + 1),
        ]);
        $_existBorrowing->delete();
        
        //When
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_GET_ALL));
        //Then
        $response->assertViewIs(GlobalVariables::BORROWINGS_MANAGING_VIEW);
        $this->assertViewDataEquals($response, GlobalVariables::BORROWINGS, [$existBorrowing, $_existBorrowing]
                , [$authenticatedUser,$authenticatedUser], 2);
    }

    public function test_givenBorrowingsAndWorkerUser_whenAllBorrowings_thenWillAbortUnathurizePage() {
        //Given
        $authenticatedUser = $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //Whenٍ
        $response = $this->getJson(route(GlobalVariables::BORROWINGS_GET_ALL));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenWorkerUser_whenSearchBorrowings_thenWillAbortUnathorizePage() {
        $this->registerThenLogin(GlobalVariables::WORKER_TYPE);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_SEARCH));
        //Then
        $response->assertStatus(GlobalVariables::UNAUTHORIZE);
    }

    public function test_givenAccountantUser_whenSearchBorrowings_thenResultWillReturned() {
        //Given
        $needle = "name";
        $this->registerThenLogin(GlobalVariables::ACOOUNTANT_TYPE);

        $userOne = TestUtil::createUser([User::NAME => $needle]);
        $existBorrowingOne = $this->createBorrowing([
            Borrowing::USER_ID => $userOne->id,
        ]);

        $userTow = TestUtil::createUser([User::NAME => $needle]);
        $existBorrowingTow = $this->createBorrowing([
            Borrowing::USER_ID => $userTow->id,
        ]);
        $existBorrowingTow->delete();

        $userThree = TestUtil::createUser([User::NAME => 'not like needle']);
        $this->createBorrowing([
            Borrowing::USER_ID => $userThree->id,
        ]);
        //When
        $response = $this->postJson(route(GlobalVariables::BORROWINGS_SEARCH)
                , [User::NAME => $needle]);
        //Then
        $response->assertViewIs(GlobalVariables::BORROWINGS_MANAGING_VIEW);
        $this->assertViewDataEquals($response, GlobalVariables::BORROWINGS
                ,[$existBorrowingOne,$existBorrowingTow],[$userOne,$userTow], 2);
    }

    //Utils
    private function registerThenLogin($type) {
        $user = TestUtil::createUser([User::TYPE => $type], true);
        $this->postJson(route('login'), [User::EMAIL => $user[User::EMAIL], User::PASSWORD
            => GlobalVariables::PASSWORD]);
        return $user;
    }

    private function assertInvalidation($response, $data) {
        $errors = (array) $response->getData()->errors;
        foreach ($data as $field => $message) {
            if (strpos($field, ',') !== false) {
                $fields = explode(',', $field);
                $this->assertEquals($errors[$fields[0]][0], TestUtil::getErrorMessage($message, $fields));
            } else {
                $this->assertEquals($errors[$field][0], TestUtil::getErrorMessage($message, [$field]));
            }
        }
    }

    private function assertBorrowingEquals($borrowing, $_borrowing, $user) {
        $this->assertEquals($borrowing[Borrowing::USER_ID], $_borrowing[Borrowing::USER_ID]);
        if (isset($borrowing[Borrowing::USER])) {
            $this->assertEquals($borrowing[Borrowing::USER][User::NAME], $user[User::NAME]);
        } else {
            $this->assertEquals($borrowing[User::NAME], $user[User::NAME]);
        }
        $this->assertEquals($borrowing[Borrowing::BORRWOING_VALUE], $_borrowing[Borrowing::BORRWOING_VALUE]);
        $this->assertEquals($borrowing[Borrowing::FISRT_DATE_PREMIUM], $_borrowing[Borrowing::FISRT_DATE_PREMIUM]);
        $this->assertEquals($borrowing[Borrowing::PREMIUMS_NUMBER], $_borrowing[Borrowing::PREMIUMS_NUMBER]);
    }

    private function getDefualtBorrowingExcept($data, $object = false) {
        $borrowing = ($object) ? new Borrowing() : array();
        $borrowing[Borrowing::BORRWOING_VALUE] = isset($data[Borrowing::BORRWOING_VALUE]) ? $data[Borrowing::BORRWOING_VALUE] : 0.0;
        $borrowing[Borrowing::FISRT_DATE_PREMIUM] = isset($data[Borrowing::FISRT_DATE_PREMIUM]) ? $data[Borrowing::FISRT_DATE_PREMIUM] : $this->getValidDatePremiumFormat();
        $borrowing[Borrowing::PREMIUMS_NUMBER] = isset($data[Borrowing::PREMIUMS_NUMBER]) ? $data[Borrowing::PREMIUMS_NUMBER] : 1.0;
        $borrowing[Borrowing::USER_ID] = isset($data[Borrowing::USER_ID]) ? $data[Borrowing::USER_ID] : null;
        if (isset($data['created_at'])) {
            $borrowing['created_at'] = $data['created_at'];
        }
        if (isset($data[Borrowing::ID])) {
            $borrowing[Borrowing::ID] = $data[Borrowing::ID];
        }
        return $borrowing;
    }

    private function createBorrowing($data = array()) {
        $borrowing = $this->getDefualtBorrowingExcept($data, true);
        $borrowing->save();
        return $borrowing;
    }

    private function modifyCurrentDate($duration) {
        return date('Y-m-d', strtotime(date('Y-m-d') . $duration));
    }

    private function getValidDatePremiumFormat() {
        return date('Y') . '-0' . (date('m') + 2) . '-' . '01';
    }

    private function modifyDate($date, $duration) {
        return date('Y-m-d', strtotime($date . $duration));
    }

    public function assertThrowingBadRequest($response, $message, $isAbort = true) {
        $response->assertStatus(GlobalVariables::BAD_REQUEST);
        if ($isAbort) {
            $this->assertEquals($response->getData()->message, $message);
        } else {
            $this->assertEquals($response->getContent(), $message);
        }
    }

    public function assertViewDataEquals($response, $viewData, $data, $users, $size) {
        $borrowings = $response->viewData(GlobalVariables::BORROWINGS)->toArray()['data'];
        $this->assertEquals(count($borrowings), 2);
        for ($i = 0; $i < $size; $i++) {
            $this->assertBorrowingEquals($borrowings[$i], $data[$i], $users[$i]);
        }
    }

    public function assertAvailablePremiumsNumbersEquals($response, $viewData, $maxPremuimsNumbers) {
        $availablePremiumsNumbers = $response->viewData($viewData);
        for ($i = 0; $i < $maxPremuimsNumbers; $i++) {
            $this->assertEquals($availablePremiumsNumbers[$i], $i + 1);
        }
        $this->assertEquals(count($availablePremiumsNumbers), $maxPremuimsNumbers);
    }

}
