<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Utils\GlobalVariables;
use App\Repository\WorkerRepository;
use App\Repository\SettingsRepository;
use App\User;
use App\Models\Setting;

class DatabaseSeeder extends Seeder {

    private $workerRepository;
    private $settingsRepository;

    function __construct(WorkerRepository $workerRepository, SettingsRepository $settingsRepository) {
        $this->workerRepository = $workerRepository;
        $this->settingsRepository = $settingsRepository;
    }

    public function run() {
        $this->workerRepository->store(
                [
                    User::NAME => 'Accountant name',
                    User::EMAIL => 'ahmedattaf111@gmail.com',
                    User::PASSWORD => Hash::make('12345678'),
                    User::TYPE => GlobalVariables::ACOOUNTANT_TYPE
        ]);
        $this->settingsRepository->store([
            Setting::MAXIMUM_BORROWING => 0.00,
            Setting::MAXIMUM_PREMIUMS_NUMBERS=> 1.00,
            Setting::MAXIMUM_VACATION_DAYS=> 0.00,
            Setting::FIRST_WEEKEND_DAY=> "Fri",
            Setting::SECOND_WEEKEND_DAY=>"Sat",
        ]);
    }

}
