<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Utils\GlobalVariables;
use App\Models\Job;
class AddingJobsFinancialDataToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()    
    {
        Schema::table(GlobalVariables::USERS, function (Blueprint $table) {
            $table->unsignedBigInteger(User::JOB_ID)->nullable();
            $table->foreign(User::JOB_ID)->references(Job::ID)->on(GlobalVariables::JOBS);
            $table->string(User::INSURANCE_NUMBER)->nullable();
            $table->date(User::WORKING_DATE)->nullable();
            $table->decimal(User::SALARY)->nullable();
            $table->date(User::START_CONTRACT_DATE)->nullable();
            $table->date(User::END_CONTRACT_DATE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(GlobalVariables::USERS, function (Blueprint $table) {
            $table->dropForeign(GlobalVariables::USERS . '_' . User::JOB_ID . '_foreign');
            $table->dropColumn(User::JOB_ID);
            $table->dropColumn(User::INSURANCE_NUMBER)->nullable();
            $table->dropColumn(User::WORKING_DATE);
            $table->dropColumn(User::SALARY);
            $table->dropColumn(User::START_CONTRACT_DATE);
            $table->dropColumn(User::END_CONTRACT_DATE);
        });
    }
}
