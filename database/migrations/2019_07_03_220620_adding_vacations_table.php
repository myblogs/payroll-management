<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Vacation;
use App\User;
class AddingVacationsTable extends Migration
{
    public function up()
    {
        Schema::create(GlobalVariables::VACATIONS, function (Blueprint $table) {
            $table->bigIncrements(Vacation::ID);
            $table->unsignedBigInteger(Vacation::USER_ID)->nullable();
            $table->foreign(Vacation::USER_ID)->references(User::ID)->on(GlobalVariables::USERS);
            $table->date(Vacation::START_DATE)->nullable();
            $table->date(Vacation::END_DATE)->nullable();
            $table->integer(Vacation::VACATION_DAYS)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists(GlobalVariables::VACATIONS);
    }
}
