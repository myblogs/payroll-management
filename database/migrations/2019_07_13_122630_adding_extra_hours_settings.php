<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\Setting;
class AddingExtraHoursSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(GlobalVariables::SETTINGS, function (Blueprint $table) {
            $table->integer(Setting::MAX_EXTRA_HOURS)->nullable();
            $table->decimal(Setting::HOUR_VALUE)->nullable();
        });
    }
    public function down()
    {
        Schema::table(GlobalVariables::SETTINGS, function (Blueprint $table) {
            $table->dropColumn(Setting::MAX_EXTRA_HOURS);
            $table->dropColumn(Setting::HOUR_VALUE);
        });
    }
}
