<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Utils\GlobalVariables;

class AddingAdditionalColumnsToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(GlobalVariables::USERS, function (Blueprint $table) {
            $table->string(User::NATIONALITY)->nullable();
            $table->date(User::BIRTH_DATE)->nullable();
            $table->string(User::ADDRESS)->nullable();
            $table->string(User::PHONE)->nullable();
            $table->string(User::PASSPORT_NUMBER)->nullable();
            $table->date(User::PASSPORT_RELEASE_DATE)->nullable();
            $table->date(User::PASSPORT_EXPIRY_DATE)->nullable();
            $table->string(User::PASSPORT_RELEASE_PLACE)->nullable();
            $table->string(User::RESIDENCY_NUMBER)->nullable();
            $table->date(User::RESIDENCY_RELEASE_DATE)->nullable();
            $table->date(User::RESIDENCY_EXPIRY_DATE)->nullable();
            $table->string(User::RESIDENCY_RELEASE_PLACE)->nullable();
            $table->string(User::WORK_LICENSE_NUMBER)->nullable();
            $table->date(User::WORK_LICENSE_RELEASE_DATE)->nullable();
            $table->date(User::WORK_LICENSE_EXPIRY_DATE)->nullable();
            $table->string(User::WORK_LICENSE_RELEASE_PLACE)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(GlobalVariables::USERS, function (Blueprint $table) {
            $table->dropColumn(User::NATIONALITY);
            $table->dropColumn(User::BIRTH_DATE);
            $table->dropColumn(User::ADDRESS);
            $table->dropColumn(User::PHONE);
            $table->dropColumn(User::PASSPORT_NUMBER);
            $table->dropColumn(User::PASSPORT_RELEASE_DATE);
            $table->dropColumn(User::PASSPORT_EXPIRY_DATE);
            $table->dropColumn(User::PASSPORT_RELEASE_PLACE);
            $table->dropColumn(User::RESIDENCY_NUMBER);
            $table->dropColumn(User::RESIDENCY_RELEASE_DATE);
            $table->dropColumn(User::RESIDENCY_EXPIRY_DATE);
            $table->dropColumn(User::RESIDENCY_RELEASE_PLACE);
            $table->dropColumn(User::WORK_LICENSE_NUMBER);
            $table->dropColumn(User::WORK_LICENSE_RELEASE_DATE);
            $table->dropColumn(User::WORK_LICENSE_EXPIRY_DATE);
            $table->dropColumn(User::WORK_LICENSE_RELEASE_PLACE);
        });
    }

}
