<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\Setting;

class AddingVacationsSettings extends Migration {

    public function up() {
        Schema::table(GlobalVariables::SETTINGS, function (Blueprint $table) {
            $table->decimal(Setting::MAXIMUM_VACATION_DAYS)->nullable();
            $table->string(Setting::FIRST_WEEKEND_DAY)->nullable();
            $table->string(Setting::SECOND_WEEKEND_DAY)->nullable();
        });
    }

    public function down() {
        Schema::table(GlobalVariables::SETTINGS, function (Blueprint $table) {
            $table->dropColumn(Setting::MAXIMUM_VACATION_DAYS);
            $table->dropColumn(Setting::FIRST_WEEKEND_DAY);
            $table->dropColumn(Setting::SECOND_WEEKEND_DAY);
        });
    }

}
