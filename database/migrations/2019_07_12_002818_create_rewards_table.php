<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\Reward;
use App\User;
class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(GlobalVariables::REWARDS, function (Blueprint $table) {
            $table->bigIncrements(Reward::ID);
            $table->unsignedBigInteger(Reward::USER_ID)->nullable();
            $table->foreign(Reward::USER_ID)->references(User::ID)->on(GlobalVariables::USERS);
            $table->decimal(Reward::REWARD_VALUE)->nullable();
            $table->text(Reward::DESCRIPTION)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(GlobalVariables::REWARDS);
    }
}
