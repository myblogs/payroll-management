<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\ExtraHours;
use App\User;

class AddingExtraHoursTable extends Migration {

    public function up() {
        Schema::create(GlobalVariables::EXTRA_HOURS, function (Blueprint $table) {
            $table->bigIncrements(ExtraHours::ID);
            $table->unsignedBigInteger(ExtraHours::USER_ID)->nullable();
            $table->foreign(ExtraHours::USER_ID)->references(User::ID)->on(GlobalVariables::USERS);
            $table->integer(ExtraHours::EXTRA_HOURS)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists(GlobalVariables::EXTRA_HOURS);
    }

}
