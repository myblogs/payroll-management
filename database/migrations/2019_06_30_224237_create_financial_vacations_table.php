<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\FinancialVacation;
use App\Models\Setting;
class CreateFinancialVacationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(GlobalVariables::FINANCIAL_VACATIONS, function (Blueprint $table) {
            $table->bigIncrements(FinancialVacation::ID);
            $table->unsignedBigInteger(FinancialVacation::SETTING_ID)->nullable();
            $table->foreign(FinancialVacation::SETTING_ID)
                    ->references(Setting::ID)->on(GlobalVariables::SETTINGS);
            $table->date(FinancialVacation::DATE)->nullable();
            $table->string(FinancialVacation::NAME)->nullable();
            $table->text(FinancialVacation::DESCRIPTION)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(GlobalVariables::FINANCIAL_VACATIONS);
    }
}
