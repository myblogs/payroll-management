<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\Setting;
class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(GlobalVariables::SETTINGS, function (Blueprint $table) {
            $table->bigIncrements(Setting::ID);
            $table->decimal(Setting::MAXIMUM_BORROWING)->nullable();
            $table->decimal(Setting::MAXIMUM_PREMIUMS_NUMBERS)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(GlobalVariables::SETTINGS);
    }
}
