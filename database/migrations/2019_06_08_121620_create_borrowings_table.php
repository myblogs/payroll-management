<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Utils\GlobalVariables;
use App\Models\Borrowing;
use App\User;

class CreateBorrowingsTable extends Migration {

    public function up() {
        Schema::create(GlobalVariables::BORROWINGS, function (Blueprint $table) {
            $table->bigIncrements(Borrowing::ID);
            $table->decimal(Borrowing::BORRWOING_VALUE)->nullable();
            $table->integer(Borrowing::PREMIUMS_NUMBER)->nullable();
            $table->date(Borrowing::FISRT_DATE_PREMIUM)->nullable();
            $table->unsignedBigInteger(Borrowing::USER_ID)->nullable();
            $table->foreign(Borrowing::USER_ID)->references(User::ID)->on(GlobalVariables::USERS);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists(GlobalVariables::BORROWINGS);
    }

}
