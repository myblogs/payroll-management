<?php
use App\Utils\GlobalVariables;
use App\Models\Borrowing;
use App\User;
use App\Utils\BorrowingUtils\BusinesUtil;
use Illuminate\Support\Facades\Auth;
use App\Models\Job;
?>
@extends('layouts.app')
@section('title',"Borrowings")
@section('content')
<div class="borrowings-managing">
    @can(GlobalVariables::WORKER_TYPE,Job::class)
    <div class="borrowings-create-update">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Update worker</div>
            </div>
        </div>
        
        <form id="form" data-url="{{route(GlobalVariables::BORROWINGS_STORE)}}" 
              data-token="{{csrf_token()}}" data-redirect={{route(GlobalVariables::BORROWINGS_INDEX)}}>
            <input type="hidden" class="form-control"  id="user_id" name="{{Borrowing::USER_ID}}" value="{{Auth::user()->id}}">
            <div class="row">
                <div class="col-md-5">
                    <label>Borrowing value</label>
                    <input type="text" class="form-control"  id="borrowing_value" name="{{Borrowing::BORRWOING_VALUE}}">
                    <div id="borrowing_value_errors" class="text-danger"></div>
                </div>
                <div class="col-md-5">
                    <label>Premiums number</label>
                    <select id="premiums_number" name="{{Borrowing::PREMIUMS_NUMBER}}" class="form-control">
                        @foreach($premiumNumbers as $premiumNumber)
                        <option value="{{$premiumNumber}}">{{$premiumNumber}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <label>First date borrowing</label>
                    <input type="text" class="form-control" id="first_date_premium" name="{{Borrowing::FISRT_DATE_PREMIUM}}">
                    <div id="first_date_premium_errors" class="text-danger"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button  onclick="putBorrowing(event, 'post');" type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
    @endcan
    @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
    <form class="search form-inline" action="{{route(GlobalVariables::BORROWINGS_SEARCH)}}" method="post">
        @csrf
        <input type="text" class="form-control mb-2  mr-2" name='name' placeholder="Search">
    </form>
    @endcan
    
    <div class="header">
        <div class="row">
            <div class="col-12">
                <i class="fas fa-tag"></i> @yield('title')
            </div>
        </div>
    </div>
    <div id="table-data" class="table-data table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Borrowing value</th>
                    <th>Premiums number</th>
                    <th>First date borrowing</th>
                    @can(GlobalVariables::WORKER_TYPE,Job::class)
                    <th>Actions</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @foreach($borrowings as $borrowing)
                <tr id ="{{$borrowing->id . '1'}}">
                    <td id ="{{$borrowing->id . '2'}}">
                        @if($borrowing->trashed()||BusinesUtil::subCurrentDateFromInMonth($borrowing->created_at)>0
                        ||!Auth::user()->can(GlobalVariables::WORKER_TYPE,Job::class))
                        {{$borrowing->user->name}}
                        @else
                        <a  href="{{route(GlobalVariables::BORROWINGS_EDIT,$borrowing->id)}}">{{$borrowing->user->name}}</a>
                        @endif
                    </td>
                    <td id="{{$borrowing->id . '5'}}">{{$borrowing->created_at}}</td>
                    <td id="{{$borrowing->id . '6'}}">{{$borrowing->borrowing_value}}</td>
                    <td id="{{$borrowing->id . '7'}}">{{$borrowing->perimiums_number}}</td>
                    <td id="{{$borrowing->id . '8'}}">{{$borrowing->first_date_premium}}</td>
                    @can(GlobalVariables::WORKER_TYPE,Job::class)
                    <td id="{{$borrowing->id . '3'}}">
                        @if(BusinesUtil::subCurrentDateFromInMonth($borrowing->created_at)==="0")
                        @if($borrowing->trashed())
                        @if(!$isAuthenticatedUserBorrow)
                        <a onclick="performAjax({{$borrowing -> id}})" id="{{$borrowing->id . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::BORROWINGS_RESTORE,$borrowing->id)}}"><i class="fas fa-undo"></i></a>
                        @else
                        <i class="fas fa-undo"></i> 
                        @endif
                        <a  href="{{route(GlobalVariables::BORROWINGS_CONFIRM_DELETE,$borrowing->id)}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                        @else
                        <a id="{{$borrowing->id . '4'}}" onclick="performAjax({{$borrowing -> id}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::BORROWINGS_DELETE,$borrowing->id)}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                        @endif
                        @endif  
                    </td>
                    @endcan
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {{$borrowings->fragment('table-data')->links()}}
    </div>
</div>
@endsection