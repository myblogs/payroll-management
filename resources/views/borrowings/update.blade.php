<?php
use App\Models\Borrowing;
use App\Utils\GlobalVariables;
?>
@extends('layouts.app')
@section('title',"Borrowing update")
@section('content')
    <div class="borrowings-create-update">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Update worker</div>
            </div>
        </div>
        <form id="form" data-url="{{route(GlobalVariables::BORROWINGS_UPDATE,$borrowing[Borrowing::ID])}}" 
              data-token="{{csrf_token()}}" data-redirect={{route(GlobalVariables::BORROWINGS_INDEX)}}>
            <input type="hidden" class="form-control"  id="user_id" name="{{Borrowing::USER_ID}}" value="{{Auth::user()->id}}">
            <input type="hidden" class="form-control"  id="borrowing_id" name="{{Borrowing::ID}}" value="{{$borrowing[Borrowing::ID]}}">
            <div class="row">
                <div class="col-md-5">
                    <label>Borrowing value</label>
                    <input type="text" class="form-control"  id="borrowing_value" name="{{Borrowing::BORRWOING_VALUE}}" 
                           value="{{$borrowing[Borrowing::BORRWOING_VALUE]}}">
                    <div id="borrowing_value_errors" class="text-danger"></div>
                </div>
                <div class="col-md-5">
                    <label>Premiums number</label>
                    <select id="premiums_number" name="{{Borrowing::PREMIUMS_NUMBER}}" class="form-control">
                        @foreach($premiumNumbers as $premiumNumber)
                        <option @if($borrowing[Borrowing::PREMIUMS_NUMBER]===$premiumNumber){{'selected'}}@endif
                                 value="{{$premiumNumber}}">{{$premiumNumber}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <label>First date borrowing</label>
                    <input type="text" class="form-control" id="first_date_premium" name="{{Borrowing::FISRT_DATE_PREMIUM}}"
                           value="{{$borrowing[Borrowing::FISRT_DATE_PREMIUM]}}">
                    <div id="first_date_premium_errors" class="text-danger"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button  onclick="putBorrowing(event,'put');" type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>

@endsection