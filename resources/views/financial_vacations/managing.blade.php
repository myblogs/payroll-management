@extends('layouts.app')
@section('title','Financial vacations managing')
@section('content')
<?php

use App\Utils\GlobalVariables;
use App\Models\FinancialVacation;
?>
<div class="financial-vacations-managing">
    <div class="create-update">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i>Create Financial vacation</div>
            </div>
        </div>
        <form id="form" data-url="{{route(GlobalVariables::FINANCIAL_VACATIONS_STORE)}}" 
              data-token="{{csrf_token()}}" data-redirect={{route(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL,$setting_id)}}>
            <input type="hidden" id="setting_id" name="{{FinancialVacation::SETTING_ID}}" value="{{$setting_id}}">
            <div class="row">
                <div class="col-md-5">
                    <label>Date</label>
                    <input type="text" class="form-control"  id="date" name="{{FinancialVacation::DATE}}">
                    <div id="date_errors" class="text-danger"></div>
                </div>
                <div class="col-md-5">
                    <label>Name</label>
                    <input type="text" class="form-control" id="name" name="{{FinancialVacation::NAME}}">
                    <div id="name_errors" class="text-danger"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <label>Description</label>
                    <textarea  class="form-control" id="description" name="{{FinancialVacation::DESCRIPTION}}">
                    </textarea>
                    <div id="description_errors" class="text-danger"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button  onclick="postPutFinancialVacation(event, 'post');" type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div id="table-data" class="table-data table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($financial_vacations as $financial_vacation)
                <tr id ="{{$financial_vacation->id . '1'}}">
                    <td id ="{{$financial_vacation->id . '2'}}">
                        @if($financial_vacation->trashed())
                        {{$financial_vacation->name}}
                        @else
                        <a  href="{{route(GlobalVariables::FINANCIAL_VACATIONS_EDIT,$financial_vacation->id)}}">{{$financial_vacation->name}}</a>
                        @endif
                    </td>
                    <td id="{{$financial_vacation->id . '5'}}">{{$financial_vacation->date}}</td>
                    <td style="max-width:10px; overflow:hidden" id="{{$financial_vacation->id . '6'}}">{{$financial_vacation->description}}</td>
                    <td id="{{$financial_vacation->id . '7'}}">{{$financial_vacation->created_at}}</td>
                    <td id="{{$financial_vacation->id . '3'}}">
                        @if($financial_vacation->trashed())
                        <a onclick="performAjax({{$financial_vacation -> id}})" id="{{$financial_vacation->id . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::FINANCIAL_VACATIONS_RESTORE,$financial_vacation->id)}}"><i class="fas fa-undo"></i></a>
                        <a href="{{route(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE,[$financial_vacation->id,$setting_id])}}" 
                           class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                        @else
                        <a id="{{$financial_vacation->id . '4'}}" onclick="performAjax({{$financial_vacation -> id}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::FINANCIAL_VACATIONS_DELETE,$financial_vacation->id)}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {{$financial_vacations->fragment('table-data')->links()}}
    </div>
</div>
@endsection