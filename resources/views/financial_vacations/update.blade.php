@extends('layouts.app')
@section('title','Financial vacations managing')
@section('content')
<?php

use App\Utils\GlobalVariables;
use App\Models\FinancialVacation;
?>
<div class="create-update">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i>Create Financial vacation</div>
        </div>
    </div>
    <form id="form" data-url="{{route(GlobalVariables::FINANCIAL_VACATIONS_UPDATE,$financial_vacation[FinancialVacation::ID])}}" 
          data-token="{{csrf_token()}}" 
          data-redirect={{route(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL,$financial_vacation[FinancialVacation::SETTING_ID])}}>
        <div class="row">
            <div class="col-md-5">
                <label>Date</label>
                <input type="text" class="form-control"  id="date" name="{{FinancialVacation::DATE}}"
                       value="{{$financial_vacation[FinancialVacation::DATE]}}">
                <div id="date_errors" class="text-danger"></div>
            </div>
            <div class="col-md-5">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="{{FinancialVacation::NAME}}" 
                       value="{{$financial_vacation[FinancialVacation::NAME]}}">
                <div id="name_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label>Description</label>
                <textarea  class="form-control" id="description" name="{{FinancialVacation::DESCRIPTION}}">{{$financial_vacation[FinancialVacation::DESCRIPTION]}}</textarea>
                <div id="description_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button  onclick="postPutFinancialVacation(event, 'put');" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
