<?php
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Vacation;
use App\User;
?>
@extends('layouts.app')
@section('title',"Vacations managing")
@section('content')
<div class='vacations-managing'>
    @can(GlobalVariables::WORKER_TYPE,Job::class)
    <div class="create-update">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Create Vacation</div>
            </div>
        </div>
        <form id="form" data-token="{{csrf_token()}}" data-url="{{route(GlobalVariables::VACATIONS_STORE)}}" 
              data-redirect="{{route(GlobalVariables::VACATIONS_INDEX)}}">
            <input type="hidden" id="user_id" name="{{Vacation::USER_ID}}" value="{{Auth::user()->id}}">
            <div class="row">
                <div class="col-md-5">
                    <label>Start date</label>
                    <input type="text" class="form-control" id="start_date" name="{{Vacation::START_DATE}}">
                    <div id="start_date_errors" class="text-danger">{{$errors->first(Vacation::START_DATE)}}</div>
                </div>
                <div class="col-md-5">
                    <label>End date</label>
                    <input type="text" class="form-control"  id="end_date" name="{{Vacation::END_DATE}}">
                    <div id="end_date_errors" class="text-danger">{{$errors->first(Vacation::END_DATE)}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button onclick="putPostVacation(event, 'post')" type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
    @endcan
    @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
    <form class="search form-inline" action="{{route(GlobalVariables::VACATIONS_SEARCH)}}" method="get">
        @csrf
        <input type="text" class="form-control mb-2  mr-2" name='name' placeholder="Search">
    </form>
    @endcan
    <div id="table-data" class="table-data table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Worker Name</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Vacation days</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vacations as $vacation)
                <tr id ="{{$vacation[Vacation::ID] . '1'}}">
                    <td id ="{{$vacation[Vacation::ID] . '2'}}">
                        @can(GlobalVariables::WORKER_TYPE,Job::class)
                        @if($vacation->trashed())
                        {{$vacation[Vacation::USER][User::NAME]}}
                        @else
                        <a  href="{{route(GlobalVariables::VACATIONS_EDIT,$vacation[Vacation::ID])}}">
                            {{$vacation[Vacation::USER][User::NAME]}}
                        </a>
                        @endif
                        @else
                        {{$vacation[Vacation::USER][User::NAME]}}
                        @endcan
                    </td>
                    <td id="{{$vacation->id . '5'}}">{{$vacation[Vacation::START_DATE]}}</td>
                    <td id="{{$vacation->id . '6'}}">{{$vacation[Vacation::END_DATE]}}</td>
                    <td id="{{$vacation->id . '7'}}">{{$vacation[Vacation::VACATION_DAYS]}}</td>
                    @can(GlobalVariables::WORKER_TYPE,Job::class)
                    <td id="{{$vacation->id . '3'}}">
                        @if($vacation->trashed())
                        <a onclick="performAjax({{$vacation[Vacation::ID]}})" id="{{$vacation[Vacation::ID] . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::VACATIONS_RESTORE,$vacation->id)}}"><i class="fas fa-undo"></i></a>
                        <a  href="{{route(GlobalVariables::VACATIONS_CONFIRM_DELETE,$vacation[Vacation::ID])}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                        @else
                        <a id="{{$vacation[Vacation::ID] . '4'}}" onclick="performAjax({{$vacation[Vacation::ID]}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::VACATIONS_DELETE,$vacation[Vacation::ID])}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    @endcan
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {{$vacations->fragment('table-data')->links()}}
    </div>
</div>
@endsection