<?php

use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Vacation;
use App\User;
?>
@extends('layouts.app')
@section('title',"Vacations managing")
@section('content')
<div class="create-update">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> Update Vacation</div>
        </div>
    </div>
    <form id="form" data-token="{{csrf_token()}}" 
          data-url="{{route(GlobalVariables::VACATIONS_UPDATE,$vacation[Vacation::ID])}}" 
          data-redirect="{{route(GlobalVariables::VACATIONS_INDEX)}}">
        <input type="hidden" id="id" name="{{Vacation::ID}}" value="{{$vacation[Vacation::ID]}}">
        <div class="row">
            <div class="col-md-5">
                <label>Start date</label>
                <input type="text" class="form-control" id="start_date" name="{{Vacation::START_DATE}}"
                       value="{{$vacation[Vacation::START_DATE]}}">
                <div id="start_date_errors" class="text-danger">{{$errors->first(Vacation::START_DATE)}}</div>
            </div>
            <div class="col-md-5">
                <label>End date</label>
                <input type="text" class="form-control"  id="end_date" name="{{Vacation::END_DATE}}"
                       value="{{$vacation[Vacation::END_DATE]}}">
                <div id="end_date_errors" class="text-danger">{{$errors->first(Vacation::END_DATE)}}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button onclick="putPostVacation(event, 'put')" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection