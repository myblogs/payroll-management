<?php

use App\Utils\GlobalVariables;
?>
@extends('layouts.app')
@section('title','Category updating')
@section('content')
<div class="update-job">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> @yield('title')</div>
        </div>
    </div>
    <form class="form-inline" action="{{route(GlobalVariables::JOBS_UPDATE,$job->id)}}" method="post">
        @csrf
        @method('put')
        <input type="text" class="form-control mb-2 mr-2" id="name" name="name" 
               value="{{$job->name}}">
        <button type="submit" class="btn btn-success mb-2">Submit</button>
    </form>
    <div class="error-message text-danger">{{$errors->first('name')}}</div>
</div>
@endsection