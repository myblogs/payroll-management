<?php

use App\Utils\GlobalVariables ?>
@extends('layouts.app')
@section('title',"Jobs managing")
@section('content')

<div class="jobs-managing">
    <div class="options">
        <a href="{{route(GlobalVariables::JOBS_INDEX)}}"><i class="fas fa-database"></i> Jobs</a>
        <a href="{{route(GlobalVariables::JOBS_ONLY_DELETED)}}"><i class="fa fa-trash" aria-hidden="true"></i>
            Deleted jobs</a>
    </div>
    
    <div class="create-job">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Create job</div>
            </div>
        </div>
        <form class="form-inline" action="{{route(GlobalVariables::JOBS_STORE)}}" method="post">
            @csrf
            <input type="text" class="form-control mb-2  mr-2" name='name' id="name">
            <button  type="submit" class="btn btn-success mb-2">Submit</button>
        </form>
        <div class="text-danger error-message">{{$errors->first('name')}}</div>
    </div>
    <form class="search form-inline" action="{{route(GlobalVariables::JOBS_SEARCH)}}" method="post">
        @csrf
        <input type="text" class="form-control mb-2  mr-2" name='name' placeholder="Search">
    </form>
    <div class="header">
        <div class="row">
            <div class="col-12">
                <i class="fas fa-tag"></i> @yield('title')
            </div>
        </div>
    </div>
    <div id="table-data" class="table-data table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jobs as $job)
                <tr id ="{{$job->id . '1'}}">
                    <td id ="{{$job->id . '2'}}">
                        @if($job->trashed())
                        {{$job->name}}
                        @else
                        <a  href="{{route(GlobalVariables::JOBS_EDIT,$job->id)}}">{{$job->name}}</a>
                        @endif
                    </td>
                    <td id="{{$job->id . '5'}}">{{$job->created_at}}</td>
                    <td id="{{$job->id . '3'}}">
                        @if($job->trashed())
                        <a onclick="performAjax({{$job -> id}})" id="{{$job->id . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::JOBS_RESTORE,$job->id)}}"><i class="fas fa-undo"></i></a>
                        <a  href="{{route(GlobalVariables::JOBS_VERIFY_DELETE,$job->id)}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                        @else
                        <a id="{{$job->id . '4'}}" onclick="performAjax({{$job -> id}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::JOBS_DESTROY,$job->id)}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {{$jobs->fragment('table-data')->links()}}
    </div>
</div>
@endsection