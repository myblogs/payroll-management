<?php

use App\Utils\GlobalVariables;
use App\Models\Job;
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/borrowing.js') }}" defer></script>
        <script src="{{ asset('js/settings.js') }}" defer></script>
        <script src="{{ asset('js/financial_vacations.js') }}" defer></script>
        <script src="{{ asset('js/vacation.js') }}" defer></script>
        <script src="{{ asset('js/reward.js') }}" defer></script>
        <script src="{{ asset('js/extra-hours.js') }}" defer></script>
        <!-- Fonts -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

        <!-- Styles --> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/job.css') }}" rel="stylesheet">
        <link href="{{ asset('css/home.css') }}" rel="stylesheet">
        <link href="{{ asset('css/worker.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light fixed-top">
                <div class="container-fluid">
                    <a class="navbar-brand" href="{{url('home')}}">
                        <img src="{{asset('images/logo.png')}}" alt="Logo" style="width:40px;"> Payroll system
                    </a>
                    @auth
                    <ul class="right navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="open-close-menu" onclick="openCloseNav()" href="javascript:void(0)"><i class="navbar-toggler-icon"></i></a>
                        </li>
                    </ul>
                    @endauth
                </div>
            </nav>

            <main class="py-4">
                @auth
                <div id="mySidenav" class="sidenav ">
                    <div class="header">
                        <i class="fa fa-cog" aria-hidden="true"></i>  Managing the system
                    </div>

                    <div class="links">
                        <a href="{{url('home')}}">
                            <i class="fas fa-home"></i> Home
                        </a>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::JOBS_INDEX)}}"><i class="fas fa-cogs"></i> Managing jobs</a>
                        <a href="{{route(GlobalVariables::WORKERS_INDEX)}}"><i class="fas fa-users"></i> Managing workers</a>
                        @else
                        <a href="{{route(GlobalVariables::WORKERS_EDIT_PROFILE)}}"><i class="fas fa-user"></i> Editing profile</a>
                        @endcan
                        @can(GlobalVariables::WORKER_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::BORROWINGS_INDEX)}}"><i class="far fa-money-bill-alt"></i> Managing borrowings</a>
                        @endcan
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::BORROWINGS_GET_ALL)}}"><i class="far fa-money-bill-alt"></i> Borrowings</a>
                        @endcan
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::SETTINGS_EDIT)}}"><i class="fa fa-cog"></i> Settings</a>
                        @endcan
                        
                        @can(GlobalVariables::WORKER_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::VACATIONS_INDEX)}}"><i class="fa fa-cog"></i> Managing vacations</a>
                        @endcan
                        
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::VACATIONS_ALL)}}"><i class="fa fa-cog"></i> Vacations</a>
                        @endcan
                        
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::REWARDS_INDEX)}}"><i class="fa fa-cog"></i> Rewards managing</a>
                        @endcan
                        
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <a href="{{route(GlobalVariables::EXTRA_HOURS_INDEX)}}"><i class="fa fa-cog"></i> Extra hours managing</a>
                        @endcan
                        @can(GlobalVariables::WORKER_TYPE,Job::class)
                            <a href="{{route(GlobalVariables::REWARDS_WORKER)}}"><i class="fa fa-cog"></i> Your rewards</a>
                        @endcan
                        
                        @can(GlobalVariables::WORKER_TYPE,Job::class)
                            <a href="{{route(GlobalVariables::WORKER_EXTRA_HOURS)}}"><i class="fa fa-cog"></i> Your extra hours</a>
                        @endcan
                        
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>

                </div>
                @endauth
                <div class='content container-fluid'>
                    @yield('content')
                </div>
            </main>
        </div>
    </body>
</html>
