<?php

use App\Utils\GlobalVariables;
use App\Models\Job;
?>
@extends('layouts.app')
@section('title','Home page')
@section('content')
<div class="home">
    <div class="container">
        <div class="dashboard">
            <div class="header">
                <i class="fas fa-user-circle"></i> Welcome {{Auth::user()->name}}
            </div>
            <div class="links">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{url('home')}}">
                            <i class="fas fa-home"></i> Home
                        </a>
                    </div>
                    @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::JOBS_INDEX)}}"><i class="fas fa-cogs"></i> Managing jobs</a>
                    </div>
                    @endcan

                    @can(GlobalVariables::WORKER_TYPE,Job::class)
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::WORKERS_EDIT_PROFILE)}}"><i class="fas fa-user"></i> Editing profile</a>
                    </div>
                    @endcan

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::WORKERS_INDEX)}}"><i class="fas fa-users"></i> Managing workers</a>
                    </div>
                    @endcan

                    @can(GlobalVariables::WORKER_TYPE,Job::class)
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::BORROWINGS_INDEX)}}"><i class="far fa-money-bill-alt"></i> Managing borrowings</a>
                    </div>
                    @endcan

                </div>
                @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                <div class="row">
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::BORROWINGS_GET_ALL)}}"><i class="far fa-money-bill-alt"></i> Borrowings</a>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{route(GlobalVariables::SETTINGS_EDIT)}}"><i class="fa fa-cog"></i> Settings</a>
                    </div>
                </div>
                @endcan
            </div>

        </div>
    </div>
</div>
@endsection
