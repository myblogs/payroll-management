<?php

use App\Utils\GlobalVariables;
use App\Models\ExtraHours;
use App\User;
use App\Models\Job;
use Illuminate\Support\Facades\Auth;
?>
@extends('layouts.app')
@section('title',"Extra hours managing")
@section('content')
@can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
<div class="options">
    <a href="{{route(GlobalVariables::EXTRA_HOURS_INDEX)}}"><i class="fas fa-database"></i> Extra hours</a>
    <a href="{{route(GlobalVariables::EXTRA_HOURS_ALL_DELETED)}}"><i class="fa fa-trash" aria-hidden="true"></i>
        Deleted extra hours</a>
</div>
@endcan
@can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
<div class="create-update">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> Create extra hours</div>
        </div>
    </div>
    <form id="form" data-token="{{csrf_token()}}" data-url="{{route(GlobalVariables::EXTRA_HOURS_STORE)}}" 
          data-redirect="{{route(GlobalVariables::EXTRA_HOURS_INDEX)}}">
        <div class="row">
            <div class="col-md-5">
                <label>Worker name</label>
                <select class="form-control" id="user_id" name="{{ExtraHours::USER_ID}}">
                    @foreach($workers as $worker)
                    <option value="{{$worker[User::ID]}}">{{$worker[User::NAME]}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-5">
                <label>Extra hours</label>
                <input type="text" class="form-control"  id="extra_hours" name="{{ExtraHours::EXTRA_HOURS}}">
                <div id="extra_hours_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button onclick="putPostExtraHours(event, 'post')" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endcan
@can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
<form class="search form-inline" action="{{route(GlobalVariables::EXTRA_HOURS_SEARCH)}}" method="post">
    @csrf
    <input type="text" class="form-control mb-2  mr-2" name="{{User::NAME}}" placeholder="Search">
</form>
@endcan
<div id="table-data" class="table-data table-responsive">          
    <table class="table">
        <thead>
            <tr>
                <th>Worker Name</th>
                <th>Extra hours</th>
                <th>Created at</th>
                @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                <th>Actions</th>
                @endcan
            </tr>
        </thead>
        <tbody>
            @foreach($extra_hours as $extra_hour)
            <tr id ="{{$extra_hour[ExtraHours::ID] . '1'}}">
                <td id ="{{$extra_hour[ExtraHours::ID] . '2'}}">
                    @if($extra_hour->trashed()||!Auth::user()
                    ->can(GlobalVariables::ACOOUNTANT_TYPE,Job::class))
                    {{$extra_hour[ExtraHours::USER][User::NAME]}}
                    @else
                    <a  href="{{route(GlobalVariables::EXTRA_HOURS_EDIT,$extra_hour[ExtraHours::ID])}}">
                        {{$extra_hour[ExtraHours::USER][User::NAME]}}
                    </a>
                    @endif
                </td>
                <td id="{{$extra_hour[ExtraHours::ID] . '5'}}">{{$extra_hour[ExtraHours::EXTRA_HOURS]}}</td>
                <td id="{{$extra_hour[ExtraHours::ID] . '6'}}">{{date('Y-m-d',strtotime($extra_hour->created_at))}}</td>
                @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                <td id="{{$extra_hour[ExtraHours::ID] . '3'}}">
                    @if($extra_hour->trashed())
                    <a onclick="performAjax({{$extra_hour[ExtraHours::ID]}})" id="{{$extra_hour[ExtraHours::ID] . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::EXTRA_HOURS_RESTORE,$extra_hour[ExtraHours::ID])}}"><i class="fas fa-undo"></i></a>
                    <a  href="{{route(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE,$extra_hour[ExtraHours::ID])}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                    @else
                    <a id="{{$extra_hour[ExtraHours::ID] . '4'}}" onclick="performAjax({{$extra_hour[ExtraHours::ID]}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::EXTRA_HOURS_DELETE,$extra_hour[ExtraHours::ID])}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                    @endif
                </td>
                @endcan
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div>
    {{$extra_hours->fragment('table-data')->links()}}
</div>
@endsection