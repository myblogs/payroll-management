<?php  
use App\Utils\GlobalVariables;
use App\Models\ExtraHours;
use App\User;
?>
@extends('layouts.app')
@section('title',"Vacations managing")
@section('content')
<div class="create-update">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> Update Vacation</div>
        </div>
    </div>
    <form id="form" data-token="{{csrf_token()}}" 
          data-url="{{route(GlobalVariables::EXTRA_HOURS_UPDATE,$extra_hour[ExtraHours::ID])}}" 
          data-redirect="{{route(GlobalVariables::EXTRA_HOURS_EDIT,$extra_hour[ExtraHours::ID])}}">
        <div class="row">
            <div class="col-md-5">
                <label>Worker name</label>
                <select class="form-control" id="user_id" name="{{ExtraHours::USER_ID}}">
                    @foreach($workers as $worker)
                    <option @if($worker[User::ID]===$extra_hour[ExtraHours::USER_ID]) selected @endif
                             value="{{$worker[User::ID]}}">{{$worker[User::NAME]}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-5">
                <label>Extra hours</label>
                <input type="text" class="form-control"  id="extra_hours" name="{{ExtraHours::EXTRA_HOURS}}"
                       value="{{$extra_hour[ExtraHours::EXTRA_HOURS]}}">
                <div id="extra_hours_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button onclick="putPostExtraHours(event, 'put')" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
