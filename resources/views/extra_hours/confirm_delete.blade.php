<?php 
use App\Utils\GlobalVariables;
?>
@extends('layouts.app')
@section('title','Delete confirmation')
@section('content')
<div class="vacations-force-delete">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i>@yield('title')</div>
        </div>
    </div>
    <div class="confirm-delete-message text-center">
        <h3 class="text-danger">Are you sure you want permanently delete this</h3>
        <a onclick="forceDelete()" href="javascript:void(0)" id="force-delete" data-redirect={{route(GlobalVariables::EXTRA_HOURS_INDEX)}} class="btn btn-outline-danger" data-url="{{route(GlobalVariables::EXTRA_HOURS_FORCE_DELETE,$id)}}" >Yes</a>
        <a class="btn btn-outline-primary" href="{{URL::previous()}}">No</a>
    </div>
</div>
@endsection
