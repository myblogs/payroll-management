<?php

use App\Utils\GlobalVariables;
use App\Models\Reward;
use App\User;
use App\Models\Job;
?>
@extends('layouts.app')
@section('title',"Rewards managing")
@section('content')
@can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
<div class="options">
    <a href="{{route(GlobalVariables::REWARDS_INDEX)}}"><i class="fas fa-database"></i> Rewards</a>
    <a href="{{route(GlobalVariables::REWARDS_ALL_DELETED)}}"><i class="fas fa-database"></i> Deleted rewards</a>
</div>
<div class="create-update">
    <form id="form" data-token="{{csrf_token()}}" data-url="{{route(GlobalVariables::REWARDS_STORE)}}" 
          data-redirect="{{route(GlobalVariables::REWARDS_INDEX)}}">
        <div class="row">
            <div class="col-md-5">
                <label>User name</label>
                <select id="user_id" name="{{Reward::USER_ID}}" class="form-control">
                    @foreach($workers as $worker)
                    <option value="{{$worker[User::ID]}}">{{$worker[User::NAME]}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-5">
                <label>Reward value</label>
                <input type="text" class="form-control" id="reward_value" name="{{Reward::REWARD_VALUE}}">
                <div id="reward_value_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label>Description</label>
                <textarea id="description" name="{{Reward::DESCRIPTION}}" class="form-control"></textarea>
                <div id="description_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button onclick="putPostReward(event, 'post')" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endcan
<div id="table-data" class="table-data table-responsive">          
    <table class="table">
        <thead>
            <tr>
                <th>Worker name</th>
                <th>Date</th>
                <th>Reward value</th>
                <th>Description</th>
                @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                <th>Actions</th>
                @endcan
            </tr>
        </thead>
        <tbody>
            @foreach($rewards as $reward)
            <tr id ="{{$reward[Reward::ID] . '1'}}">
                <td id ="{{$reward[Reward::ID] . '2'}}">
                    @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                    @if($reward->trashed())
                    {{$reward[Reward::USER][User::NAME]}}
                    @else
                    <a  href="{{route(GlobalVariables::REWARDS_EDIT,$reward[Reward::ID])}}">{{$reward[Reward::USER][User::NAME]}}</a>
                    @endif
                    @else
                    {{$reward[Reward::USER][User::NAME]}}
                    @endcan
                </td>
                <td id="{{$reward[Reward::ID] . '5'}}">{{$reward->created_at}}</td>
                <td id="{{$reward[Reward::ID] . '6'}}">{{$reward[Reward::REWARD_VALUE]}}</td>
                <td style="max-width:0px;overflow:hidden" id="{{$reward[Reward::ID] . '7'}}">{{$reward[Reward::DESCRIPTION]}}</td>
                @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                <td id="{{$reward[Reward::ID] . '3'}}">
                    @if($reward->trashed())
                    <a onclick="performAjax({{$reward[Reward::ID]}})" id="{{$reward[Reward::ID]. '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::REWARDS_RESTORE,$reward[Reward::ID])}}"><i class="fas fa-undo"></i></a>
                    <a  href="{{route(GlobalVariables::REWARDS_CONFIRM_DELETE,$reward[Reward::ID])}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                    @else
                    <a id="{{$reward[Reward::ID] . '4'}}" onclick="performAjax({{$reward[Reward::ID]}})" class="delete-btn text-danger" href="javascript:void(0)" 
                       data-url="{{route(GlobalVariables::REWARDS_DELETE,$reward[Reward::ID])}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                    @endif
                </td>
                @endcan
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div>
    {{$rewards->fragment('table-data')->links()}}
</div>
@endsection