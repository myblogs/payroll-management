<?php
use App\Utils\GlobalVariables;
use App\Models\Reward;
use App\User;
?>
@extends('layouts.app')
@section('title',"Rewards managing")
@section('content')
<div class="create-update">
    <div class="header">
        <div class="row">
            <div class="col-12"><i class="fas fa-tag"></i> Create reward</div>
        </div>
    </div>
    <form id="form" data-token="{{csrf_token()}}" data-url="{{route(GlobalVariables::REWARDS_UPDATE,$reward[Reward::ID])}}" 
          data-redirect="{{route(GlobalVariables::REWARDS_INDEX)}}">
        <div class="row">
            <div class="col-md-5">
                <label>User name</label>
                <select id="user_id" name="{{Reward::USER_ID}}" class="form-control">
                    @foreach($workers as $worker)
                    <option @if($worker[User::ID]===$reward[Reward::USER_ID]) selected @endif value="{{$worker[User::ID]}}">{{$worker[User::NAME]}}</option>
                    @endforeach
                </select>
                
            </div>
            <div class="col-md-5">
                <label>Reward value</label>
                <input type="text" class="form-control" id="reward_value" name="{{Reward::REWARD_VALUE}}"
                       value="{{$reward[Reward::REWARD_VALUE]}}">
                <div id="reward_value_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label>Description</label>
                <textarea id="description" name="{{Reward::DESCRIPTION}}" class="form-control">{{$reward[Reward::DESCRIPTION]}}</textarea>
                <div id="description_errors" class="text-danger"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button onclick="putPostReward(event,'put')" type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection