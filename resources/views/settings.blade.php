<?php

use App\Utils\GlobalVariables;
use App\Models\Setting;

$weekDays = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
?>
@extends('layouts.app')
@section('title',"Settings")
@section('content')
<div class="settings">
    <div class='create'>
        <form id="form" data-url="{{route(GlobalVariables::SETTINGS_SET)}}" 
              data-token="{{csrf_token()}}" data-redirect={{url('/home')}}>
            <div class="borrowings">
                <div class="header">
                    <div class="row">
                        <div class="col-12"><i class="fas fa-tag"></i> Borrowings settings</div>
                    </div>
                </div>
                <div class="row inputs-field">
                    <div class="col-md-5">
                        <label>Maximum borrowing value</label>
                        <input type="text" class="form-control"  id="max_borrowing_value" name="{{Setting::MAXIMUM_BORROWING}}"
                               value="{{$settings[Setting::MAXIMUM_BORROWING]}}">
                        <div id="max_borrowing_value_errors" class="text-danger"></div>
                    </div>
                    <div class="col-md-5">
                        <label>Maximum premiums numbers</label>
                        <input type="text" class="form-control"  id="max_premiums_numbers" name="{{Setting::MAXIMUM_PREMIUMS_NUMBERS}}"
                               value="{{$settings[Setting::MAXIMUM_PREMIUMS_NUMBERS]}}">
                        <div id="max_premiums_numbers_errors" class="text-danger"></div>
                    </div>
                </div>
            </div>

            <div class="vacations">
                <div class="header">
                    <div class="row">
                        <div class="col-12"><i class="fas fa-tag"></i> Vacation settings</div>
                    </div>
                </div>

                <div class="row inputs-field">
                    <div class="col-md-5">
                        <label>First weekend day</label>
                        <select id="first_weekend_day" name="{{Setting::FIRST_WEEKEND_DAY}}" class="form-control">
                            @foreach($weekDays as $weekDay)
                            <option @if($weekDay==$settings[Setting::FIRST_WEEKEND_DAY]){{'selected'}} @endif>{{$weekDay}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-5">
                        <label>Second weekend day</label>
                        <select id="second_weekend_day" name="{{Setting::SECOND_WEEKEND_DAY}}" class="form-control">
                            @foreach($weekDays as $weekDay)
                            <option @if($weekDay==$settings[Setting::SECOND_WEEKEND_DAY]){{'selected'}} @endif>{{$weekDay}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row inputs-field">
                    <div class="col-md-5">
                        <label>Maximum vacation days</label>
                        <input type="text" class="form-control"  id="max_vacation_days" name="{{Setting::MAXIMUM_VACATION_DAYS}}"
                               value="{{$settings[Setting::MAXIMUM_VACATION_DAYS]}}">
                        <div id="max_vacation_days_errors" class="text-danger"></div>
                    </div>
                    <div class="col-md-5 mt-5">
                        <div class="managing-link">
                            <a href="{{route(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL,$settings[Setting::ID])}}"><i class='fas fa-gift'></i> Managing financial vacations</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="extra-hours">
                <div class="header">
                    <div class="row">
                        <div class="col-12"><i class="fas fa-tag"></i> Extra hours  settings</div>
                    </div>
                </div>
                <div class="row inputs-field">
                    <div class="col-md-5">
                        <label>Max extra hours</label>
                        <input type="text" class="form-control"  id="max_extra_hours" name="{{Setting::MAX_EXTRA_HOURS}}"
                               value="{{$settings[Setting::MAX_EXTRA_HOURS]}}">
                        <div id="max_extra_hours_errors" class="text-danger"></div>
                    </div>
                    <div class="col-md-5">
                        <label>Hour cost</label>
                        <input type="text" class="form-control"  id="hour_value" name="{{Setting::HOUR_VALUE}}"
                               value="{{$settings[Setting::HOUR_VALUE]}}">
                        <div id="hour_value_errors" class="text-danger"></div>
                    </div>
                </div>
            </div>
    </div>

    <div class="row">
        <div class="col-12">
            <button  onclick="setSettings(event);" type="submit" class="submit btn btn-success">Submit</button>
        </div>
    </div>
</form>
</div>
</div>
@endsection