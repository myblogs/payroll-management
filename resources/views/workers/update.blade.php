<?php

use App\Utils\GlobalVariables;
use App\User;
use App\Models\Job;
?>
@extends('layouts.app')
@section('title','worker updating')
@section('content')
<div class="update-worker">
    <form action="@can(GlobalVariables::ACOOUNTANT_TYPE,Job::class){{route(GlobalVariables::WORKERS_UPDATE,$worker->id)}} @else {{route(GlobalVariables::WORKERS_UPDATE_PROFILE,$worker->id)}}@endcan" method="post">
        @csrf
        @method('put')
        <div class="personal-data">
            <div class="header">
                <div class="row">
                    <div class="col-12"><i class="fas fa-tag"></i> Personal data</div>
                </div>
            </div>
            <div class="inputs">
                <div class="row">
                    <div class="col-md-8">
                        <label>Name</label>
                        <input type="text" class="form-control" value="{{$worker->name}}" name="{{User::NAME}}">
                        <div class="text-danger">{{$errors->first(User::NAME)}}</div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-8">
                        <label>Address</label>
                        <input type="text" class="form-control" value="{{$worker->address}}" id="name" name="{{User::ADDRESS}}">
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-4">
                        <label >Nationality</label>
                        <select name="{{User::NATIONALITY}}" class="form-control">
                            @foreach($countries as $country)
                            <option @if($country===$worker->nationality){{'selected'}}@endif value="{{$country}}">{{$country}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Phone number</label>
                        <input type="text" class="form-control" id="name" value="{{$worker->phone}}" name="{{User::PHONE}}" >
                        <div class="text-danger">{{$errors->first(User::PHONE)}}</div>
                    </div>

                </div>  
                <div class="row">
                    <div class="col-md-4">
                        <label>Birth date</label>
                        <input type="text" class="form-control" value="{{$worker->birth_date}}" name="{{User::BIRTH_DATE}}">
                        <div class="text-danger">{{$errors->first(User::BIRTH_DATE)}}</div>
                    </div>
                </div>

            </div>
        </div>
        <div class="working-financial-data">
            <div class="header">
                <div class="row">
                    <div class="col-12"><i class="fas fa-tag"></i> Working and financial data</div>
                </div>
            </div>
            <div class="inputs">
                <div class="row">
                    <div class="col-md-8">
                        <label>Insurance number</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <input type="text" class="form-control" value="{{$worker->insurance_number}}" id="name" name="{{User::INSURANCE_NUMBER}}" >
                        @else
                        <div>
                            {{$worker->insurance_number}}
                        </div>
                        @endcan
                    </div>
                </div>  

                <div class="row">
                    <div class="col-md-4">
                        <label >Job name</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <select name="{{User::JOB_ID}}" class="form-control">
                            @foreach($jobs as $job)
                            <option  @if($job->id===$worker->job_id){{'selected'}}@endif value="{{$job[Job::ID]}}">{{$job[Job::NAME]}}</option>
                            @endforeach
                        </select>
                        <div class="text-danger">{{$errors->first(User::JOB_ID)}}</div>
                        @else
                        <div>
                            {{$worker[User::JOB][Job::NAME]}}
                        </div>
                        @endcan
                    </div>
                    <div class="col-md-4">
                        <label>Salary</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <input type="text" class="form-control" value="{{$worker->salary}}" name="{{User::SALARY}}">
                        <div class="text-danger">{{$errors->first(User::SALARY)}}</div>
                        @else
                        <div>
                            {{$worker[User::SALARY]}}
                        </div>
                        @endcan
                    </div>
                </div>  

                <div class="row">
                    <div class="col-md-4">
                        <label>Start contract date</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <input type="text" class="form-control"  value="{{$worker->start_contract_date}}" name="{{User::START_CONTRACT_DATE}}">
                        <div class="text-danger">{{$errors->first(User::START_CONTRACT_DATE)}}</div>
                        @else
                        <div>
                            {{$worker->start_contract_date}}
                        </div>
                        @endcan
                    </div>
                    <div class="col-md-4">
                        <label>End contract date</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <input type="text" class="form-control" value="{{$worker->end_contract_date}}" name="{{User::END_CONTRACT_DATE}}">
                        <div class="text-danger">{{$errors->first(User::END_CONTRACT_DATE)}}</div>
                        @else
                        <div>
                            {{$worker->end_contract_date}}
                        </div>
                        @endcan
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Working date</label>
                        @can(GlobalVariables::ACOOUNTANT_TYPE,Job::class)
                        <input type="text" class="form-control"  value="{{$worker->working_date}}" name="{{User::WORKING_DATE}}">
                        <div class="text-danger">{{$errors->first(User::WORKING_DATE)}}</div>
                        @else
                        <div>
                            {{$worker->working_date}}
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
        <div class="documents-data">
            <div class="header">
                <div class="row">
                    <div class="col-12"><i class="fas fa-tag"></i> Documents data</div>
                </div>
            </div>
            <div class="inputs">
                <div class="passport">
                    <div class="row">
                        <div class="col-md-8">
                            <label>Passport number</label>
                            <input type="text" class="form-control" value="{{$worker->passport_number}}"  name="{{User::PASSPORT_NUMBER}}" >
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-4">
                            <label>Realease date</label>
                            <input type="text" class="form-control"  value="{{$worker->passport_release_date}}" name="{{User::PASSPORT_RELEASE_DATE}}">
                            <div class="text-danger">{{$errors->first(User::PASSPORT_RELEASE_DATE)}}</div>
                        </div>
                        <div class="col-md-4">
                            <label>Expire date</label>
                            <input type="text" class="form-control"  value="{{$worker->passport_expiry_date}}" name="{{User::PASSPORT_EXPIRY_DATE}}">
                            <div class="text-danger">{{$errors->first(User::PASSPORT_EXPIRY_DATE)}}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Release place</label>
                            <input type="text" class="form-control" value="{{$worker->passport_release_place}}" name="{{User::PASSPORT_RELEASE_PLACE}}">
                        </div>
                    </div>
                </div>
                <div class="residency">
                    <div class="row">
                        <div class="col-md-8">
                            <label>Residency number</label>
                            <input type="text" class="form-control" id="name" value="{{$worker->residency_number}}" name="{{User::RESIDENCY_NUMBER}}" >
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-4">
                            <label>Release date</label>
                            <input type="text" class="form-control"  value="{{$worker->residency_release_date}}" name="{{User::RESIDENCY_RELEASE_DATE}}">
                            <div class="text-danger">{{$errors->first(User::RESIDENCY_RELEASE_DATE)}}</div>
                        </div>

                        <div class="col-md-4">
                            <label>Expiry date</label>
                            <input type="text" class="form-control"  value="{{$worker->residency_expiry_date}}" name="{{User::RESIDENCY_EXPIRY_DATE}}">
                            <div class="text-danger">{{$errors->first(User::RESIDENCY_EXPIRY_DATE)}}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Release place</label>
                            <input type="text" class="form-control" value="{{$worker->residency_release_place}}" name="{{User::RESIDENCY_RELEASE_PLACE}}">
                        </div>
                    </div>
                </div>
                <div class="work-license">
                    <div class="row">
                        <div class="col-md-8">
                            <label>Work license number</label>
                            <input type="text" class="form-control" id="name" value="{{$worker->work_license_number}}" name="{{User::WORK_LICENSE_NUMBER}}" >
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-4">
                            <label>Realease date</label>
                            <input type="text" class="form-control"  value="{{$worker->work_license_release_date}}" name="{{User::WORK_LICENSE_RELEASE_DATE}}">
                            <div class="text-danger">{{$errors->first(User::WORK_LICENSE_RELEASE_DATE)}}</div>
                        </div>

                        <div class="col-md-4">
                            <label>Expire date</label>
                            <input type="text" class="form-control"  value="{{$worker->work_license_expiry_date}}" name="{{User::WORK_LICENSE_EXPIRY_DATE}}">
                            <div class="text-danger">{{$errors->first(User::WORK_LICENSE_EXPIRY_DATE)}}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Release place</label>
                            <input type="text" class="form-control" value="{{$worker->work_license_release_place}}" name="{{User::WORK_LICENSE_RELEASE_PLACE}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <button type="submit" class="submit btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
