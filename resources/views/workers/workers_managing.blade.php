
<?php

use App\Utils\GlobalVariables;
use App\User;
use App\Models\Job;
?>
@extends('layouts.app')
@section('title',"Workers managing")
@section('content')

<div class="workers-managing">
    <div class="options">
        <a href="{{route(GlobalVariables::WORKERS_INDEX)}}"><i class="fas fa-database"></i> Workers</a>
        <a href="{{route(GlobalVariables::WORKERS_ONLY_DELETED)}}"><i class="fa fa-trash" aria-hidden="true"></i>
            Deleted workers</a>
    </div>
    
    <div class="create-worker">
        <div class="header">
            <div class="row">
                <div class="col-12"><i class="fas fa-tag"></i> Create worker</div>
            </div>
        </div>
        <form action="{{route(GlobalVariables::WORKERS_STORE)}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <label>Name</label>
                    <input type="text" class="form-control"  name="{{User::NAME}}">
                    <div class="text-danger">{{$errors->first(User::NAME)}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <label>Email</label>
                    <input type="text" class="form-control"  name="{{User::EMAIL}}">
                    <div class="text-danger">{{$errors->first(User::EMAIL)}}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Password</label>
                    <input type="password" class="form-control"  name="{{User::PASSWORD}}">
                    <div class="text-danger">{{$errors->first(User::PASSWORD)}}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Confirm password</label>
                    <input type="password" class="form-control"  name="{{User::PASSWORD_CONFIRMATION}}">
                    <div class="text-danger">{{$errors->first(User::PASSWORD_CONFIRMATION)}}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <label>Insurance number</label>
                    <input type="text" class="form-control" id="name" name="{{User::INSURANCE_NUMBER}}" >
                </div>
            </div>  

            <div class="row">
                <div class="col-md-4">
                    <label >Job name</label>
                    <select name="{{User::JOB_ID}}" class="form-control">
                        @foreach($jobs as $job)
                        <option value="{{$job[Job::ID]}}">{{$job[Job::NAME]}}</option>
                        @endforeach
                    </select>
                    
                    <div class="text-danger">{{$errors->first(User::JOB_ID)}}</div>
                </div>
                <div class="col-md-4">
                    <label>Salary</label>
                    <input type="text" class="form-control"  name="{{User::SALARY}}">
                    <div class="text-danger">{{$errors->first(User::SALARY)}}</div>
                </div>
            </div>  

            <div class="row">
                <div class="col-md-4">
                    <label>Start contract date</label>
                    <input type="text" class="form-control"  name="{{User::START_CONTRACT_DATE}}">
                    <div class="text-danger">{{$errors->first(User::START_CONTRACT_DATE)}}</div>
                </div>
                
                <div class="col-md-4">
                    <label>End contract date</label>
                    <input type="text" class="form-control" name="{{User::END_CONTRACT_DATE}}">
                    <div class="text-danger">{{$errors->first(User::END_CONTRACT_DATE)}}</div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-4">
                    <label>Working date</label>
                    <input type="text" class="form-control"  name="{{User::WORKING_DATE}}">
                    <div class="text-danger">{{$errors->first(User::WORKING_DATE)}}</div>
                </div>
            
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="submit btn btn-success">Submit</button>
                </div>
            </div>
            
        </form>
    </div>
    <form class="search form-inline" action="{{route(GlobalVariables::WORKERS_SEARCH)}}" method="post">
        @csrf
        <input type="text" class="form-control mb-2  mr-2" name='name' placeholder="Search">
    </form>
    <div class="header">
        <div class="row">
            <div class="col-12">
                <i class="fas fa-tag"></i> @yield('title')
            </div>
        </div>
    </div>
    
    <div id="table-data" class="table-data table-responsive">          
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($workers as $worker)
                <tr id ="{{$worker->id . '1'}}">
                    <td id ="{{$worker->id . '2'}}">
                        @if($worker->trashed())
                        {{$worker->name}}
                        @else
                        <a  href="{{route(GlobalVariables::WORKERS_EDIT,$worker->id)}}">{{$worker->name}}</a>
                        @endif
                    </td>
                    <td id="{{$worker->id . '5'}}">{{$worker->created_at}}</td>
                    <td id="{{$worker->id . '6'}}">{{$worker->email}}</td>
                    <td id="{{$worker->id . '3'}}">
                        @if($worker->trashed())
                        <a onclick="performAjax({{$worker -> id}})" id="{{$worker->id . '4'}}" class="text-success"  href='javascript:void(0)' data-url="{{route(GlobalVariables::WORKERS_RESTORE,$worker->id)}}"><i class="fas fa-undo"></i></a>
                        <a  href="{{route(GlobalVariables::WORKERS_CONFIRM_DELETE,$worker->id)}}" class="text-danger"><i class="btn-sm fas fa-times "></i></a>
                        @else
                        <a id="{{$worker->id . '4'}}" onclick="performAjax({{$worker -> id}})" class="delete-btn text-danger" href="javascript:void(0)" data-url="{{route(GlobalVariables::WORKERS_DELETE,$worker->id)}}"><i class="fa fa-times" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    
                </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
    
    <div>
        {{$workers->fragment('table-data')->links()}}
    </div>
    
</div>
@endsection