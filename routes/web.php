<?php

use App\Utils\GlobalVariables;
use App\User;
use App\Models\Borrowing;
use App\Models\FinancialVacation;
use App\Vacation;
use App\Models\Reward;
use App\Models\ExtraHours;
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
/////////////////////////////////////
//                                 //
//       Job component            //
//                                 //
/////////////////////////////////////
Route::prefix(GlobalVariables::JOBS)->group(function() {
    $jobId = '{jobId}';
    Route::get("$jobId/delete", GlobalVariables::JOB_CONTROLLER . "@delete")->name(GlobalVariables::JOBS_DESTROY);
    Route::get("$jobId/restore", GlobalVariables::JOB_CONTROLLER . "@restore")->name(GlobalVariables::JOBS_RESTORE);
    Route::get("deletedJobs", GlobalVariables::JOB_CONTROLLER . "@getDeletedJobs")->name(GlobalVariables::JOBS_ONLY_DELETED);
    Route::post("search", GlobalVariables::JOB_CONTROLLER . "@search")->name(GlobalVariables::JOBS_SEARCH);
    Route::get("$jobId/verifyDelete", GlobalVariables::JOB_CONTROLLER . "@verifyDelete")->name(GlobalVariables::JOBS_VERIFY_DELETE);
    Route::get("$jobId/forceDelete", GlobalVariables::JOB_CONTROLLER . "@forceDelete")->name(GlobalVariables::JOBS_FORCE_DELETE);
});
Route::resource(GlobalVariables::JOBS, GlobalVariables::JOB_CONTROLLER);

/////////////////////////////////////
//                                 //
//       Worker component          //
//                                 //
/////////////////////////////////////

Route::prefix(GlobalVariables::WORKERS)->group(function() {
    Route::get("{" . User::ID . "}" . "/delete", 'WorkerController@delete')->name(GlobalVariables::WORKERS_DELETE);
    Route::get("{" . User::ID . "}" . '/restore', 'WorkerController@restore')->name(GlobalVariables::WORKERS_RESTORE);
    Route::get('deleted-workers}', 'WorkerController@getDeletedWorkers')->name(GlobalVariables::WORKERS_ONLY_DELETED);
    Route::get("{" . User::ID . "}" . '/confirm-delete', 'WorkerController@confirmDelete')->name(GlobalVariables::WORKERS_CONFIRM_DELETE);
    Route::get("{" . User::ID . "}" . '/force-delete', 'WorkerController@forceDelete')->name(GlobalVariables::WORKERS_FORCE_DELETE);
    Route::post('search', 'WorkerController@getSearchedWorkers')->name(GlobalVariables::WORKERS_SEARCH);
    Route::get('edit-profile', 'WorkerController@editProfile')->name(GlobalVariables::WORKERS_EDIT_PROFILE);
    Route::put('update-profile', 'WorkerController@updateProfile')->name(GlobalVariables::WORKERS_UPDATE_PROFILE);
});

Route::resource(GlobalVariables::WORKERS, 'WorkerController');
/////////////////////////////////////
//                                 //
//       Settings component        //
//                                 //
/////////////////////////////////////
Route::prefix(GlobalVariables::SETTINGS)->group(function() {
    Route::put('', 'SettingsController@setSettings')->name(GlobalVariables::SETTINGS_SET);
    Route::get('edit-settings', 'SettingsController@editSettings')->name(GlobalVariables::SETTINGS_EDIT);
});
/////////////////////////////////////
//                                 //
//       Borrowing component       //
//                                 //
/////////////////////////////////////
Route::prefix(GlobalVariables::BORROWINGS)->group(function() {
    Route::get("{" . Borrowing::ID . "}" . "/delete", 'BorrowingController@delete')->name(GlobalVariables::BORROWINGS_DELETE);
    Route::get("{" . Borrowing::ID . "}" . "/restore", 'BorrowingController@restore')->name(GlobalVariables::BORROWINGS_RESTORE);
    Route::get("{" . Borrowing::ID . "}" . "/confirm-delete", 'BorrowingController@confirmDelete')->name(GlobalVariables::BORROWINGS_CONFIRM_DELETE);
    Route::get("{" . Borrowing::ID . "}" . "/force-delete", 'BorrowingController@forceDelete')->name(GlobalVariables::BORROWINGS_FORCE_DELETE);
    Route::get("get-all", 'BorrowingController@getAllBorrowings')->name(GlobalVariables::BORROWINGS_GET_ALL);
    Route::post("search", 'BorrowingController@search')->name(GlobalVariables::BORROWINGS_SEARCH);
});

Route::resource(GlobalVariables::BORROWINGS, 'BorrowingController');
//////////////////////////////////////////////////
//                                               //
//       Financial vacation component            //
//                                               //
///////////////////////////////////////////////////
Route::prefix(GlobalVariables::FINANCIAL_VACATIONS)->group(function() {
    Route::get("{" . FinancialVacation::ID . "}" . "/delete", 'FinancialVacationController@delete')
            ->name(GlobalVariables::FINANCIAL_VACATIONS_DELETE);
    Route::get("{" . FinancialVacation::ID . "}" . "/restore", 'FinancialVacationController@restore')
            ->name(GlobalVariables::FINANCIAL_VACATIONS_RESTORE);
    Route::get("{" . FinancialVacation::ID . "}" ."/{" . FinancialVacation::SETTING_ID. "}" . "/confirm-delete", 'FinancialVacationController@confirmDelete')
            ->name(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE);
    Route::get("{" . FinancialVacation::ID . "}" . "/force-delete", 'FinancialVacationController@forceDelete')
            ->name(GlobalVariables::FINANCIAL_VACATIONS_FORCE_DELETE);
    Route::get("{" . FinancialVacation::SETTING_ID . "}/get-all", 'FinancialVacationController@getAll')
            ->name(GlobalVariables::FINANCIAL_VACATIONS_GET_ALL);
});

Route::resource(GlobalVariables::FINANCIAL_VACATIONS, 'FinancialVacationController');

//////////////////////////////////////////////////
//                                               //
//       Vacations component            //
//                                               //
///////////////////////////////////////////////////

Route::prefix(GlobalVariables::VACATIONS)->group(function() {
    Route::get("{" . Vacation::ID . "}" . "/delete", 'VacationsController@delete')
            ->name(GlobalVariables::VACATIONS_DELETE);
    Route::get("{" . Vacation::ID . "}" . "/confirm-delete", 'VacationsController@confirmDelete')
            ->name(GlobalVariables::VACATIONS_CONFIRM_DELETE);
    Route::get("{" . Vacation::ID . "}" . "/force-delete", 'VacationsController@forceDelete')
            ->name(GlobalVariables::VACATIONS_FORCE_DELETE);
    Route::get("{" . Vacation::ID . "}" . "/restore", 'VacationsController@restore')
            ->name(GlobalVariables::VACATIONS_RESTORE);
    Route::post("search", 'VacationsController@search')
            ->name(GlobalVariables::VACATIONS_SEARCH);
    Route::get("get-all",'VacationsController@getAll')->name(GlobalVariables::VACATIONS_ALL);
});

Route::resource(GlobalVariables::VACATIONS, 'VacationsController');
///////////////////////////////////////////////////
//                                               //
//       Reward component                        //
//                                               //
///////////////////////////////////////////////////
Route::prefix(GlobalVariables::REWARDS)->group(function() {
    Route::get("{" . Reward::ID . "}" . "/delete", 'RewardsController@delete')
            ->name(GlobalVariables::REWARDS_DELETE);
    Route::get("{" . Reward::ID . "}" . "/restore", 'RewardsController@restore')
            ->name(GlobalVariables::REWARDS_RESTORE);
    Route::get("{" . Reward::ID . "}" . "/confirm-delete", 'RewardsController@confirmDelete')
            ->name(GlobalVariables::REWARDS_CONFIRM_DELETE);
    Route::get("{" . Reward::ID . "}" . "/force-delete", 'RewardsController@forceDelete')
            ->name(GlobalVariables::REWARDS_FORCE_DELETE);
    Route::get("all-deleted", 'RewardsController@getDeletedRewards')
            ->name(GlobalVariables::REWARDS_ALL_DELETED);
    Route::get("auth-worker", 'RewardsController@getWorkerRewards')
            ->name(GlobalVariables::REWARDS_WORKER);
});

Route::resource(GlobalVariables::REWARDS, 'RewardsController');

///////////////////////////////////////////////////
//                                               //
//       Extra hours component                   //
//                                               //
///////////////////////////////////////////////////
Route::prefix(GlobalVariables::EXTRA_HOURS)->group(function() {
    Route::get("{" . ExtraHours::ID . "}" . "/delete", 'ExtraHoursController@delete')
            ->name(GlobalVariables::EXTRA_HOURS_DELETE);
    Route::get("{" . ExtraHours::ID . "}" . "/restore", 'ExtraHoursController@restore')
            ->name(GlobalVariables::EXTRA_HOURS_RESTORE);
    Route::get("{" . ExtraHours::ID . "}" . "/confirm-delete", 'ExtraHoursController@confirmDelete')
            ->name(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE);
    Route::get("{" . ExtraHours::ID . "}" . "/force-delete", 'ExtraHoursController@forceDelete')
            ->name(GlobalVariables::EXTRA_HOURS_FORCE_DELETE);
    Route::get("all-deleted", 'ExtraHoursController@getAllDeleted')
            ->name(GlobalVariables::EXTRA_HOURS_ALL_DELETED);
    Route::get("search", 'ExtraHoursController@search')
            ->name(GlobalVariables::EXTRA_HOURS_SEARCH);
    Route::post("search", 'ExtraHoursController@search')
            ->name(GlobalVariables::EXTRA_HOURS_SEARCH);
    Route::get("worker", 'ExtraHoursController@getWorkersExtraHours')
            ->name(GlobalVariables::WORKER_EXTRA_HOURS);
});
Route::resource(GlobalVariables::EXTRA_HOURS, 'ExtraHoursController');