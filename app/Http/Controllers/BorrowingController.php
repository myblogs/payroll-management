<?php

namespace App\Http\Controllers;

use App\Http\Requests\Borrowing\CreateValidation;
use App\Repository\BorrowingRepository;
use App\Utils\Messages\ErrorMessages;
use App\Utils\GlobalVariables;
use App\Utils\Messages\SucessMessges;
use App\Http\Requests\Borrowing\UpdateValidation;
use App\Models\Borrowing;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Job;
use App\Utils\BorrowingUtils\BusinesUtil;
use App\Repository\SettingsRepository;
use App\Models\Setting;
use Illuminate\Http\Request;

class BorrowingController extends Controller {

    private $borrwoingRepository;
    private $settingsRepository;

    const PAGE_SIZE = 3;

    function __construct(BorrowingRepository $borrwoingRepository, SettingsRepository $settingsRepository) {
        $this->borrwoingRepository = $borrwoingRepository;
        $this->settingsRepository = $settingsRepository;
    }

    public function index() {
        $this->authorize(GlobalVariables::WORKER_TYPE, Job::class);
        $workerBorrowings = $this->borrwoingRepository
                ->getWorkerBorrowings(Auth::user()[User::ID], self::PAGE_SIZE);
        return view(GlobalVariables::BORROWINGS_MANAGING_VIEW)
                        ->with(GlobalVariables::BORROWINGS, $workerBorrowings)
                        ->with(GlobalVariables::PREMIUMS_NUMBERS, $this->getAvailabelPremiumsNumbers())
                        ->with(GlobalVariables::IS_AUTHENTICATED_USER_BORROW, $this->borrwoingRepository->isAuthenticatedUserBorrow());
    }

    public function getAllBorrowings() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $borrowings = $this->borrwoingRepository->getAllBorrowings(self::PAGE_SIZE);
        return view(GlobalVariables::BORROWINGS_MANAGING_VIEW)
                        ->with(GlobalVariables::BORROWINGS, $borrowings);
    }

    public function store(CreateValidation $request) {
        if ($this->borrwoingRepository->isAuthenticatedUserBorrow()) {
            return response(ErrorMessages::AUTHENTICATED_USER_BORROW_BEFORE, GlobalVariables::BAD_REQUEST);
        }
        $this->borrwoingRepository->store($request->input());
        return SucessMessges::CREARTED_SUCCESSFULLY;
    }

    public function edit($id) {
        $borrowing = $this->borrwoingRepository->getBorrowing($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $borrowing);
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        return view(GlobalVariables::BORROWINGS_UPDATE_VIEW)
                        ->with(GlobalVariables::BORROWING, $borrowing)
                        ->with(GlobalVariables::PREMIUMS_NUMBERS, $this->getAvailabelPremiumsNumbers());
    }

    public function update(UpdateValidation $request, $id) {
        $borrowing = $this->borrwoingRepository->getBorrowing($id);
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        $this->borrwoingRepository->update($request->input(), $id);
        return SucessMessges::UPDATED_SUCCESSFULLY;
    }

    public function delete($id) {
        $borrowing = $this->borrwoingRepository->getBorrowing($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $borrowing);
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        $this->borrwoingRepository->delete($borrowing);
    }

    public function restore($id) {
        $borrowing = $this->borrwoingRepository->getDeletedBorrowing($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $borrowing);
        $this->abortBadRequestIfUserBorrow();
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        $this->borrwoingRepository->restore($borrowing);
    }

    public function confirmDelete($id) {
        $borrowing = $this->borrwoingRepository->getDeletedBorrowing($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $borrowing);
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        return view(GlobalVariables::BORROWINGS_CONFIRM_DELETE_VIEW)
                        ->with(Borrowing::ID, $borrowing[Borrowing::ID]);
    }

    public function forceDelete($id) {
        $borrowing = $this->borrwoingRepository->getDeletedBorrowing($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $borrowing);
        $this->abortBadRequestIfBorrowingDateExpire($borrowing->created_at);
        $this->borrwoingRepository->forceDelete($borrowing);
        return SucessMessges::DELETED_SUCCESSFULLY;
    }

    public function search(Request $request) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $borrowings = $this->borrwoingRepository->search($request->input(User::NAME), self::PAGE_SIZE);
        return view(GlobalVariables::BORROWINGS_MANAGING_VIEW)
                        ->with(GlobalVariables::BORROWINGS, $borrowings);
    }

    //Utils
    private function abortBadRequestIfBorrowingDateExpire($date) {
        if (BusinesUtil::subCurrentDateFromInMonth($date) > 0) {
            abort(GlobalVariables::BAD_REQUEST, ErrorMessages::DATE_IS_EXPIRE);
        }
    }

    private function getAvailabelPremiumsNumbers() {
        $maxPremiumsNumbers = $this->settingsRepository->getSettings()[Setting::MAXIMUM_PREMIUMS_NUMBERS];
        $premiumsNumbers = [];
        for ($i = 1; $i <= $maxPremiumsNumbers; $i++) {
            $premiumsNumbers[] = $i;
        }
        return $premiumsNumbers;
    }

    public function abortBadRequestIfUserBorrow() {
        if ($this->borrwoingRepository->isAuthenticatedUserBorrow()) {
            abort(GlobalVariables::BAD_REQUEST, ErrorMessages::AUTHENTICATED_USER_BORROW_BEFORE);
        }
    }

}
