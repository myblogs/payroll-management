<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobValidation;
use App\Repository\JobRepository;
use App\Utils\GlobalVariables;
use App\Models\Job;
use Illuminate\Http\Request;
use App\Utils\AppUtils;
class JobController extends Controller {

    private $jobRepository;
    private $job;

    const PAGE_SIZE = 6;

    function __construct(JobRepository $jobRepository) {
        $this->jobRepository = $jobRepository;
    }

    public function index() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getPagedJobs(self::PAGE_SIZE));
    }

    public function store(JobValidation $request) {
        $this->job = $this->jobRepository->storeJob($request->input());
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getPagedJobs(self::PAGE_SIZE));
    }

    public function edit($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $this->job = AppUtils::getOrAbortIfNotExist($this->jobRepository->getJob($id));
        return view(GlobalVariables::JOBS_UPDATE_VIEW)
                        ->with(GlobalVariables::JOB, $this->job);
    }

    public function update(JobValidation $request, $id) {
        $this->jobRepository->updateJob($request->except(GlobalVariables::PUT
                        , GlobalVariables::TOKEN), $id);
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getPagedJobs(self::PAGE_SIZE));
    }

    public function delete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $this->job = AppUtils::getOrAbortIfNotExist($this->jobRepository->getJob($id));
        $this->jobRepository->destroyJob($this->job);
    }

    public function restore($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $this->job = AppUtils::getOrAbortIfNotExist($this->jobRepository->getDeletedJob($id));
        $this->jobRepository->restoreJob($this->job);
    }

    public function getDeletedJobs() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getDeletedJobs(self::PAGE_SIZE));
    }

    public function search(Request $request) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository
                                ->getSearchedJobs($request->input(Job::NAME), self::PAGE_SIZE));
    }

    public function verifyDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::JOBS_VERIFY_DELETE_VIEW)->with(Job::ID, $id);
    }

    public function forceDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $this->job = AppUtils::getOrAbortIfNotExist($this->jobRepository->getDeletedJob($id));
        $this->jobRepository->forceDeleteJob($this->job);
        return view(GlobalVariables::JOBS_MANAGING_VIEW)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getDeletedJobs(self::PAGE_SIZE));
    }
}
