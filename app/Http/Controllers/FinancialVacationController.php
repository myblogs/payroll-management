<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\FinancialVacation\CreateValidation;
use App\Repository\FinancialVacationRepository;
use App\Utils\Messages\SucessMessges;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Utils\AppUtils;
use App\Models\FinancialVacation;

class FinancialVacationController extends Controller {

    private $financialVacationRepository;

    function __construct(FinancialVacationRepository $financialVacationRepository) {
        $this->financialVacationRepository = $financialVacationRepository;
    }
    
    public function getAll($settingsId) {
        $financialVacations = $this->financialVacationRepository
                ->getPagedAll(GlobalVariables::FINANCIAL_VACATIONS_PAGE_SIZE);
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::FINANCIAL_VACATIONS_MANAGING_VIEW)
                        ->with(GlobalVariables::FINANCIAL_VACATIONS, $financialVacations)
                        ->with(FinancialVacation::SETTING_ID, $settingsId);
    }

    public function store(CreateValidation $request) {
        $this->financialVacationRepository->sotre($request->input());
        return SucessMessges::CREARTED_SUCCESSFULLY;
    }

    public function edit($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $financialVacation = $this->financialVacationRepository->getFinancialVacation($id);
        AppUtils::getOrAbortIfNotExist($financialVacation);
        return view(GlobalVariables::FINANCIAL_VACATIONS_UPDATE_VIEW)
                        ->with(GlobalVariables::FINANCIAL_VACATION, $financialVacation);
    }

    public function update(CreateValidation $request, $id) {
        $this->financialVacationRepository->update($request->input(), $id);
        return SucessMessges::UPDATED_SUCCESSFULLY;
    }

    public function delete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $financialVacation = $this->financialVacationRepository->getFinancialVacation($id);
        AppUtils::getOrAbortIfNotExist($financialVacation);
        $this->financialVacationRepository->delete($financialVacation);
    }

    public function restore($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $financialVacation = $this->financialVacationRepository->getDeletedFinancialVacation($id);
        AppUtils::getOrAbortIfNotExist($financialVacation);
        $this->financialVacationRepository->restore($financialVacation);
    }

    public function confirmDelete($id,$settingId) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $financialVacation = $this->financialVacationRepository->getDeletedFinancialVacation($id);
        AppUtils::getOrAbortIfNotExist($financialVacation);
        return view(GlobalVariables::FINANCIAL_VACATIONS_CONFIRM_DELETE_VIEW)
                        ->with(FinancialVacation::ID, $id)
                        ->with(FinancialVacation::SETTING_ID, $settingId);
    }

    public function forceDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $financialVacation = $this->financialVacationRepository->getDeletedFinancialVacation($id);
        AppUtils::getOrAbortIfNotExist($financialVacation);
        $this->financialVacationRepository->forceDelete($financialVacation);
        return SucessMessges::DELETED_SUCCESSFULLY;
    }

}
