<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ExtraHoursValidation;
use App\Utils\Messages\SucessMessges;
use App\Repository\ExtraHoursRepository;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Utils\AppUtils;
use App\Repository\WorkerRepository;
use App\Models\ExtraHours;
use App\User;
use Illuminate\Support\Facades\Auth;
class ExtraHoursController extends Controller {

    private $extraHoursRepository;
    private $workersRepository;

    const PAGE_SIZE = 3;

    function __construct(ExtraHoursRepository $extraHoursRepository, WorkerRepository $workersRepository) {
        $this->extraHoursRepository = $extraHoursRepository;
        $this->workersRepository = $workersRepository;
    }

    public function index() {
        $workers = $this->workersRepository->getWorkers();
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getPagingExtraHours(self::PAGE_SIZE);
        return view(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW)
                        ->with(GlobalVariables::EXTRA_HOURS, $extraHours)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function store(ExtraHoursValidation $request) {
        $this->extraHoursRepository->store($request->input());
        return SucessMessges::CREARTED_SUCCESSFULLY;
    }

    public function edit($id) {
        $workers = $this->workersRepository->getWorkers();
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getExtraHours($id);
        AppUtils::getOrAbortIfNotExist($extraHours);
        return view(GlobalVariables::EXTRA_HOURS_EDIT_VIEW)
                        ->with(GlobalVariables::EXTRA_HOUR, $extraHours)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function update(ExtraHoursValidation $request, $id) {
        $this->extraHoursRepository->update($request->input(), $id);
        return SucessMessges::UPDATED_SUCCESSFULLY;
    }

    public function delete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getExtraHours($id);
        AppUtils::getOrAbortIfNotExist($extraHours);
        $this->extraHoursRepository->delete($extraHours);
    }

    public function restore($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getDeletedExtraHours($id);
        AppUtils::getOrAbortIfNotExist($extraHours);
        $this->extraHoursRepository->restore($extraHours);
    }

    public function confirmDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getDeletedExtraHours($id);
        AppUtils::getOrAbortIfNotExist($extraHours);
        return view(GlobalVariables::EXTRA_HOURS_CONFIRM_DELETE_VIEW)
                        ->with(ExtraHours::ID, $id);
    }

    public function forceDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getDeletedExtraHours($id);
        AppUtils::getOrAbortIfNotExist($extraHours);
        $this->extraHoursRepository->forceDelete($extraHours);
        return SucessMessges::DELETED_SUCCESSFULLY;
    }

    public function getAllDeleted() {
        $workers = $this->workersRepository->getWorkers();
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $extraHours = $this->extraHoursRepository->getPagingDeletedExtraHours(self::PAGE_SIZE);
        return view(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW)
                        ->with(GlobalVariables::EXTRA_HOURS, $extraHours)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function search(Request $request) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $workers = $this->workersRepository->getWorkers();
        $extraHours = $this->extraHoursRepository->search(self::PAGE_SIZE
                ,$request);
        return view(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW)
                        ->with(GlobalVariables::EXTRA_HOURS, $extraHours)
                        ->with(GlobalVariables::WORKERS, $workers);
    }
    
    public function getWorkersExtraHours(){
        $extraHours=$this->extraHoursRepository
                ->getWorkerExtraHours(self::PAGE_SIZE,Auth::user()[User::ID]);
        $this->authorize(GlobalVariables::WORKER_TYPE, Job::class);
        return view(GlobalVariables::EXTRA_HOURS_MANAGING_VIEW)
                        ->with(GlobalVariables::EXTRA_HOURS, $extraHours);
    }
    
}
