<?php

namespace App\Http\Controllers;

use App\Http\Requests\Worker\CreateValidation;
use Illuminate\Http\Request;
use App\Repository\WorkerRepository;
use App\Utils\GlobalVariables;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Job;
use App\Http\Requests\Worker\UpdateValidation;
use App\Utils\UserUtils;
use App\Utils\AppUtils;
use App\Repository\JobRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Worker\ProfileValidation;
class WorkerController extends Controller {

    private $workerRepository;
    private $jobRepository;
    
    const PAGE_SIZE = 2;
    const COUNTRIES=['Egypt','Lybia','sudan'];
    function __construct(WorkerRepository $workerRepository, JobRepository $jobRepository) {
        $this->workerRepository = $workerRepository;
        $this->jobRepository = $jobRepository;
    }

    public function index() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::WORKERS_MANAGING_VIEW)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository->getAllWorkers(self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function getDeletedWorkers() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::WORKERS_MANAGING_VIEW)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository->getDeletedWorkers(self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function store(CreateValidation $request) {
        $worker = $this->workerRepository->store($this->prepareStoreInputs($request));
        return view(GlobalVariables::WORKERS . '.workers_managing')
                        ->with(GlobalVariables::WORKER, $worker)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository->getAllWorkers(self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function edit($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $worker = AppUtils::getOrAbortIfNotExist($this->workerRepository->getWorker($id));
        return view(GlobalVariables::WORKERS_UPDATE_VIEW)
                        ->with(GlobalVariables::WORKER, $worker)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs())
                        ->with(GlobalVariables::COUNTRIES,self::COUNTRIES);
    }

    public function update(UpdateValidation $request, $id) {
        $this->workerRepository->updateWorker($request->input(), $id);
        return view(GlobalVariables::WORKERS_MANAGING_VIEW)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository->getAllWorkers(self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function delete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $worker = AppUtils::getOrAbortIfNotExist($this->workerRepository->getWorker($id));
        $this->workerRepository->deleteWorker($worker);
    }

    public function restore($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $worker = AppUtils::getOrAbortIfNotExist($this->workerRepository->getDeletedWorker($id));
        $this->workerRepository->restoreWorker($worker);
    }

    public function confirmDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::WORKERS_CONFIRM_DELETE_VIEW)->with(User::ID, $id);
    }

    public function forceDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $worker = AppUtils::getOrAbortIfNotExist($this->workerRepository->getDeletedWorker($id));
        $this->workerRepository->forceDelete($worker);
        return view(GlobalVariables::WORKERS_MANAGING_VIEW)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository->getDeletedWorkers(self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function getSearchedWorkers(Request $request) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::WORKERS_MANAGING_VIEW)
                        ->with(GlobalVariables::WORKERS, $this->workerRepository
                                ->getSearchedWorkers($request->input(User::NAME), self::PAGE_SIZE))
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs());
    }

    public function editProfile() {
        $this->authorize(GlobalVariables::WORKER_TYPE, Job::class);
        $worker = $this->workerRepository->getWorker(Auth::user()->id);
        return view(GlobalVariables::WORKERS_UPDATE_VIEW)
                        ->with(GlobalVariables::WORKER, $worker)
                        ->with(GlobalVariables::JOBS, $this->jobRepository->getAllJobs())
                        ->with(GlobalVariables::COUNTRIES, self::COUNTRIES);
    }

    public function updateProfile(ProfileValidation $request) {
        $this->workerRepository->updateProfile($request->input(),Auth::user()->id);
        return view('home');
    }

    //Utils
    private function prepareStoreInputs($request) {
        $inputs = $request->except(User::PASSWORD, User::TYPE);
        $inputs[User::PASSWORD] = Hash::make($request->input(User::PASSWORD));
        $inputs[User::TYPE] = GlobalVariables::WORKER_TYPE;
        return $inputs;
    }

}
