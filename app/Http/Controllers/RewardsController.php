<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\rewards\CreateValidation;
use App\Repository\RewardsRepository;
use App\Utils\Messages\SucessMessges;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Utils\AppUtils;
use App\Repository\WorkerRepository;
use App\Utils\BorrowingUtils\BusinesUtil;
use App\Utils\Messages\ErrorMessages;
use App\Models\Reward;
use Illuminate\Support\Facades\Auth;
class RewardsController extends Controller {

    private $rewardsRepository;
    private $workersRepository;

    const PAGE_SIZE = 3;

    function __construct(RewardsRepository $rewardsRepository, WorkerRepository $workersRepositroy) {
        $this->rewardsRepository = $rewardsRepository;
        $this->workersRepository = $workersRepositroy;
    }

    public function index() {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $rewards = $this->rewardsRepository->getAllRewards(self::PAGE_SIZE);
        $workers = $this->workersRepository->getWorkers();
        return view(GlobalVariables::REWARDS_MANAGING_VIEW)
                        ->with(GlobalVariables::REWARDS, $rewards)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function store(CreateValidation $request) {
        $this->rewardsRepository->store($request->input());
        return SucessMessges::CREARTED_SUCCESSFULLY;
    }

    public function edit($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $reward = $this->rewardsRepository->getReward($id);
        $workers = $this->workersRepository->getWorkers();
        AppUtils::getOrAbortIfNotExist($reward);
        return view(GlobalVariables::REWARDS_UPDATE_VIEW)
                        ->with(GlobalVariables::REWARD, $reward)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function update(CreateValidation $request, $id) {
        $this->rewardsRepository->update($request->input(), $id);
        return SucessMessges::UPDATED_SUCCESSFULLY;
    }

    public function delete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $reward = $this->rewardsRepository->getReward($id);
        AppUtils::getOrAbortIfNotExist($reward);
        $this->rewardsRepository->delete($reward);
    }

    public function getDeletedRewards() {
        $workers = $this->workersRepository->getWorkers();
        $rewards = $this->rewardsRepository->getDeletedRewards(self::PAGE_SIZE);
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::REWARDS_MANAGING_VIEW)
                        ->with(GlobalVariables::REWARDS, $rewards)
                        ->with(GlobalVariables::WORKERS, $workers);
    }

    public function restore($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $reward = $this->rewardsRepository->getDeletedReward($id);
        AppUtils::getOrAbortIfNotExist($reward);
        $this->rewardsRepository->restore($reward);
    }

    public function confirmDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $reward = $this->rewardsRepository->getDeletedReward($id);
        AppUtils::getOrAbortIfNotExist($reward);
        return view(GlobalVariables::REWARDS_CONFIRM_DELETE_VIEW)
                        ->with(Reward::ID, $id);
    }

    public function forceDelete($id) {
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        $reward = $this->rewardsRepository->getDeletedReward($id);
        AppUtils::getOrAbortIfNotExist($reward);
        $this->rewardsRepository->forceDelete($reward);
        return SucessMessges::DELETED_SUCCESSFULLY;
    }

    public function getWorkerRewards() {
        $this->authorize(GlobalVariables::WORKER_TYPE, Job::class);
        $rewards=$this->rewardsRepository->getWorkerRewards(Auth::user()->id, self::PAGE_SIZE);
        return view(GlobalVariables::REWARDS_MANAGING_VIEW)
                        ->with(GlobalVariables::REWARDS, $rewards);
    }
}
