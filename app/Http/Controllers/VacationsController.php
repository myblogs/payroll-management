<?php

namespace App\Http\Controllers;

use App\Http\Requests\vacations\CreateValidation;
use App\Repository\VacationsRepositroy;
use App\Utils\Messages\SucessMessges;
use App\Utils\VacationsUtils\VacationControllerUtil;
use App\Vacation;
use App\Utils\GlobalVariables;
use App\Http\Requests\vacations\UpdateValidation;
use App\Models\Job;
use Illuminate\Http\Request;
use App\User;

class VacationsController extends Controller {

    private $vacationsRepository;

    const PAGE_SIZE = 3;

    function __construct(VacationsRepositroy $vacationsRepository) {
        $this->vacationsRepository = $vacationsRepository;
    }

    public function index() {
        $vacations = $this->vacationsRepository->getWorkerVacations(self::PAGE_SIZE);
        $this->authorize(GlobalVariables::WORKER_TYPE, Job::class);
        return view(GlobalVariables::VACATIONS_MANAGING_VIEW)
                        ->with(GlobalVariables::VACATIONS, $vacations);
    }

    public function store(CreateValidation $request) {
        $this->vacationsRepository->store(VacationControllerUtil::prepareInputs($request->input()));
        return SucessMessges::CREARTED_SUCCESSFULLY;
    }

    public function edit($id) {
        $vacation = $this->vacationsRepository->getVacation($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $vacation);
        VacationControllerUtil::abortIfVacationStart($vacation[Vacation::START_DATE]);
        return view(GlobalVariables::VACATIONS_UPDATE_VIEW)
                        ->with(GlobalVariables::VACATION, $vacation);
    }

    public function update(UpdateValidation $request, $id) {
        $vacation = $this->vacationsRepository->getVacation($id);
        VacationControllerUtil::abortIfVacationStart($vacation[Vacation::START_DATE]);
        $this->vacationsRepository->update(VacationControllerUtil::prepareInputs($request->input()), $id);
        return SucessMessges::UPDATED_SUCCESSFULLY;
    }

    public function delete($id) {
        $vacation = $this->vacationsRepository->getVacation($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $vacation);
        VacationControllerUtil::abortIfVacationStart($vacation[Vacation::START_DATE]);
        $this->vacationsRepository->delete($vacation);
    }

    public function confirmDelete($id) {
        $vacation = $this->vacationsRepository->getDeletedVacation($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $vacation);
        return view(GlobalVariables::VACATIONS_CONFIRM_DELETE_VIEW)
                        ->with(Vacation::ID, $id);
    }

    public function forceDelete($id) {
        $vacation = $this->vacationsRepository->getDeletedVacation($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $vacation);
        $this->vacationsRepository->forceDelete($vacation);
        return SucessMessges::DELETED_SUCCESSFULLY;
    }

    public function restore($id) {
        $vacation = $this->vacationsRepository->getDeletedVacation($id);
        $this->authorize(GlobalVariables::WORKER_TYPE, $vacation);
        VacationControllerUtil::abortIfNotValid($vacation);
        $this->vacationsRepository->restore($vacation);
    }

    public function search(Request $request) {
        $vacations = $this->vacationsRepository->search($request->input(User::NAME), self::PAGE_SIZE);
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::VACATIONS_MANAGING_VIEW)
                        ->with(GlobalVariables::VACATIONS, $vacations);
    }

    public function getAll() {
        $vacations = $this->vacationsRepository->getAll(self::PAGE_SIZE);
        $this->authorize(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
        return view(GlobalVariables::VACATIONS_MANAGING_VIEW)
                        ->with(GlobalVariables::VACATIONS, $vacations);
    }

}
