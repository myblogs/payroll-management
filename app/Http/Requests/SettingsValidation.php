<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Models\Setting;
class SettingsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can(GlobalVariables::ACOOUNTANT_TYPE,Job::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Setting::MAXIMUM_BORROWING=>'required|numeric|min:0',
            Setting::MAXIMUM_PREMIUMS_NUMBERS=>'required|numeric|min:0',
            Setting::MAXIMUM_VACATION_DAYS=>'required|numeric|min:0',
            Setting::FIRST_WEEKEND_DAY=>'required',
            Setting::SECOND_WEEKEND_DAY=>'required',
            Setting::MAX_EXTRA_HOURS=>'required|numeric|min:0',
            Setting::HOUR_VALUE=>'required|numeric|min:0',
        ];
    }
}
