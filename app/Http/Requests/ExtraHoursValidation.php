<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Models\ExtraHours;
use App\Rules\ExtraHours\NotExeedMaxExtraHours;
class ExtraHoursValidation extends FormRequest
{
    public function authorize()
    {
        return Auth::user()->can(GlobalVariables::ACOOUNTANT_TYPE,Job::class);
    }

    public function rules()
    {
        return [
            ExtraHours::EXTRA_HOURS=>['required','numeric','min:0',new NotExeedMaxExtraHours]
        ];
    }
}
