<?php

namespace App\Http\Requests\rewards;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Models\Reward;
class CreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->can(GlobalVariables::ACOOUNTANT_TYPE,Job::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Reward::REWARD_VALUE=>'required|numeric|min:0',
            Reward::DESCRIPTION=>'required'
        ];
    }
}
