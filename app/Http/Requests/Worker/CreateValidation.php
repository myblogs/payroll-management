<?php

namespace App\Http\Requests\Worker;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\User;
class CreateValidation extends FormRequest {

    public function authorize() {
        return Auth::user()->can(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
    }

    public function rules() {
        return [
            User::NAME=> 'required',
            User::EMAIL => 'required|email|unique:users',
            User::PASSWORD => 'required|min:8|confirmed',
            User::JOB_ID=> 'required|numeric|min:0',
            User::SALARY => 'required|numeric|min:0',
            User::WORKING_DATE => 'required|date_format:Y-m-d|after_or_equal:'.date('Y-m-d'),
            User::START_CONTRACT_DATE => 'required|date_format:Y-m-d|after_or_equal:'.date('Y-m-d'),
            User::END_CONTRACT_DATE => 'required|date_format:Y-m-d|after:'.User::START_CONTRACT_DATE.'|after_or_equal:' .date('Y-m-d'),
        ];
    }

}
