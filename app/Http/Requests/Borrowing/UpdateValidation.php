<?php

namespace App\Http\Requests\Borrowing;

use Illuminate\Foundation\Http\FormRequest;
use App\Utils\GlobalVariables;
use Illuminate\Support\Facades\Auth;
use App\Utils\BorrowingUtils\ValidationUtil;
use App\Models\Borrowing;
use App\Repository\BorrowingRepository;
use App\Utils\BorrowingUtils\groups\Update;
class UpdateValidation extends FormRequest {
    private $borrowingRepository;
    function __construct(BorrowingRepository $borrowingRepository) {
        $this->borrowingRepository = $borrowingRepository;
    }
    public function authorize() {
        $borrowing=$this->borrowingRepository->getBorrowing($this[Borrowing::ID]);
        return Auth::user()->can(GlobalVariables::WORKER_TYPE,$borrowing);
    }
    public function rules() {
        return ValidationUtil::getRules(Update::class);
    }

}
