<?php

namespace App\Http\Requests\Borrowing;

use Illuminate\Foundation\Http\FormRequest;
use App\Utils\GlobalVariables;
use App\Models\Job;
use Illuminate\Support\Facades\Auth;
use App\Utils\BorrowingUtils\ValidationUtil;
use App\Utils\BorrowingUtils\groups\Create;
class CreateValidation extends FormRequest {

    public function authorize() {
        return Auth::user()->can(GlobalVariables::WORKER_TYPE, Job::class);
    }

    public function rules() {
        return ValidationUtil::getRules(Create::class);
    }

}
