<?php

namespace App\Http\Requests\vacations;

use Illuminate\Foundation\Http\FormRequest;
use App\Repository\VacationsRepositroy;
use App\Vacation;
use App\Utils\GlobalVariables;
use Illuminate\Support\Facades\Auth;
use App\Utils\VacationsUtils\BusinessUtil;
use App\Utils\BorrowingUtils\groups\Update;
class UpdateValidation extends FormRequest{
    private $vacationsRepository;
    function __construct(VacationsRepositroy $vacationRepository) {
        $this->vacationsRepository = $vacationRepository;
    }

    public function authorize(){
        $vacation = $this->vacationsRepository->getVacation($this[Vacation::ID]);
        return Auth::user()->can(GlobalVariables::WORKER_TYPE,$vacation);
    }
    
    public function rules(){
        return BusinessUtil::getValidationRules(Update::class);
    }
}
