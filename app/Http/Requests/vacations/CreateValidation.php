<?php

namespace App\Http\Requests\vacations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\GlobalVariables;
use App\Models\Job;
use App\Utils\VacationsUtils\BusinessUtil;
use App\Utils\BorrowingUtils\groups\Create;
class CreateValidation extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::user()->can(GlobalVariables::WORKER_TYPE, Job::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return BusinessUtil::getValidationRules(Create::class);
    }

}
