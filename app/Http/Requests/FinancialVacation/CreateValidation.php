<?php

namespace App\Http\Requests\FinancialVacation;

use Illuminate\Foundation\Http\FormRequest;
use App\Utils\GlobalVariables;
use Illuminate\Support\Facades\Auth;
use App\Models\Job;
use App\Models\FinancialVacation;

class CreateValidation extends FormRequest {
public function authorize() {
        return Auth::user()->can(GlobalVariables::ACOOUNTANT_TYPE, Job::class);
    }
    public function rules() {
        return [
            FinancialVacation::DATE => 'required|date_format:"Y-m-d"|after_or_equal:' . date('Y-m-d'),
            FinancialVacation::NAME => 'required',
            FinancialVacation::DESCRIPTION => 'required',
        ];
    }

}
