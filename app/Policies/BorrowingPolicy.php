<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Borrowing;
class BorrowingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function worker(User $user,Borrowing $borrowing){
        return ($user[User::ID]===$borrowing[Borrowing::USER_ID]);
    }
}
