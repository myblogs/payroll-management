<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Vacation;
class VacationPolicy
{
    use HandlesAuthorization;
    public function __construct()
    {
    }
    public function worker(User $user, Vacation $vacation){
        return $vacation[Vacation::USER_ID]===$user[User::ID];
    }
}
