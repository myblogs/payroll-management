<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Utils\GlobalVariables;
class JobPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    public function accountant(User $user){
        return $user->type=== GlobalVariables::ACOOUNTANT_TYPE;
    }
    
    public function worker(User $user){
        return $user->type=== GlobalVariables::WORKER_TYPE;
    }
}
