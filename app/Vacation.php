<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Vacation extends Model {
    use SoftDeletes;
    const ID = "id";
    const USER_ID = "user_id";
    const START_DATE = "start_date";
    const END_DATE = "end_date";
    const VACATION_DAYS="vacation_days";
    const USER="user";
    protected $fillable = [
        self::ID,
        self::USER_ID,
        self::START_DATE,
        self::END_DATE,
        self::VACATION_DAYS,
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
