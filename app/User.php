<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Job;
use Illuminate\Support\Facades\Hash;
class User extends Authenticatable {

    const ID = "id";
    const NAME = "name";
    const EMAIL = "email";
    const PASSWORD = "password";
    const PASSWORD_CONFIRMATION = "password_confirmation";
    const TYPE = "type";
    const JOB_ID = 'job_id';
    const INSURANCE_NUMBER = 'insurance_number';
    const WORKING_DATE = 'working_date';
    const SALARY = "salary";
    const START_CONTRACT_DATE = "start_contract_date";
    const END_CONTRACT_DATE = "end_contract_date";
    const NATIONALITY = "nationality";
    const BIRTH_DATE = "birth_date";
    const ADDRESS = "address";
    const PHONE = "phone";
    const PASSPORT_NUMBER = "passport_number";
    const PASSPORT_RELEASE_DATE = "passport_release_date";
    const PASSPORT_EXPIRY_DATE = "passport_expiry_date";
    const PASSPORT_RELEASE_PLACE = "passport_release_place";
    const RESIDENCY_NUMBER = "residency_number";
    const RESIDENCY_RELEASE_DATE = "residency_release_date";
    const RESIDENCY_EXPIRY_DATE = "residency_expiry_date";
    const RESIDENCY_RELEASE_PLACE = "residency_release_place";
    const WORK_LICENSE_NUMBER = "work_license_number";
    const WORK_LICENSE_RELEASE_DATE = "work_license_release_date";
    const WORK_LICENSE_EXPIRY_DATE = "work_license_expiry_date";
    const WORK_LICENSE_RELEASE_PLACE = "work_license_release_place";
    const JOB = "job";

    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        User::NAME, User::EMAIL, User::PASSWORD, User::JOB_ID,
        User::INSURANCE_NUMBER, User::WORKING_DATE, User::SALARY, User::START_CONTRACT_DATE,
        User::END_CONTRACT_DATE, User::TYPE, User::NATIONALITY, User::BIRTH_DATE, User::ADDRESS
        , User::PHONE, User::PASSPORT_NUMBER, User::PASSPORT_RELEASE_DATE, User::PASSPORT_EXPIRY_DATE
        , User::PASSPORT_RELEASE_PLACE, User::RESIDENCY_NUMBER, User::RESIDENCY_RELEASE_DATE,
        User::RESIDENCY_EXPIRY_DATE, User::RESIDENCY_RELEASE_PLACE, User::WORK_LICENSE_NUMBER,
        User::WORK_LICENSE_RELEASE_DATE, User::WORK_LICENSE_EXPIRY_DATE, User::WORK_LICENSE_RELEASE_PLACE
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function job() {
        return $this->belongsTo(Job::class);
    }
}
