<?php

namespace App\Rules\vacations;

use Illuminate\Contracts\Validation\Rule;
use App\Utils\Messages\ErrorMessages;
class SameCurrentYear implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return date('Y', strtotime($value))===date('Y');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ErrorMessages::DATE_YEAR_IS_NOT_CURRENT_YEAR;
    }
}
