<?php

namespace App\Rules\vacations;

use Illuminate\Contracts\Validation\Rule;
use App\Repository\VacationsRepositroy;
use App\Utils\Messages\ErrorMessages;
use App\Utils\BorrowingUtils\groups\Update;
use App\Utils\BorrowingUtils\groups\Create;
use Illuminate\Support\Facades\Input;
use App\Vacation;
class AfterLastVacation implements Rule {

    private $vacationsRepository;
    private $group;

    public function __construct($group) {
        $this->vacationsRepository = new VacationsRepositroy();
        $this->group = $group;
    }

    public function passes($attribute, $value) {
        if ($this->group === Create::class) {
            return $this->vacationsRepository->isAfterLastVacation($value);
        } else if ($this->group === Update::class) {
            return $this->vacationsRepository->isAfterLastVacationExcept($value,Input::get(Vacation::ID));
        }
    }

    public function message() {
        return ErrorMessages::NEW_VACATION_BEFORE_LAST_VACATION;
    }

}
