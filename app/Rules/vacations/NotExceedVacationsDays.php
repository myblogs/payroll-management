<?php

namespace App\Rules\vacations;

use Illuminate\Contracts\Validation\Rule;
use App\Repository\VacationsRepositroy;
use Illuminate\Support\Facades\Input;
use App\Vacation;
use App\Repository\SettingsRepository;
use App\Models\Setting;
use App\Utils\Messages\ErrorMessages;
use App\Utils\VacationsUtils\BusinessUtil;
use App\Repository\FinancialVacationRepository;
use App\Utils\BorrowingUtils\groups\Update;
use App\Utils\BorrowingUtils\groups\Create;

class NotExceedVacationsDays implements Rule {

    private $vacationsRepository;
    private $settingsRepository;
    private $financialVacationsRepository;
    private $group;
    private $startDate;

    public function __construct($group,$startDate) {
        $this->vacationsRepository = new VacationsRepositroy();
        $this->settingsRepository = new SettingsRepository();
        $this->financialVacationsRepository = new FinancialVacationRepository();
        $this->group = $group;
        $this->startDate= $startDate;
    }

    public function passes($attribute, $value) {
        $this->startDate=($this->startDate===null)?Input::get(Vacation::START_DATE):$this->startDate;
        $setting = $this->settingsRepository->getSettings();
        $maxVacationDays = $setting[Setting::MAXIMUM_VACATION_DAYS];
        $financialVacations = $this->financialVacationsRepository->getAll();
        $insertedVacationDays = BusinessUtil::getVacationDays($value, $this->startDate, $financialVacations, $setting);
        if ($this->group === Create::class) {
            $userVacationsDays = $this->vacationsRepository->sumUserVacationsDays();
        } else if ($this->group === Update::class) {
            $userVacationsDays = $this->vacationsRepository
                    ->sumUserVacationsDaysExcept(Input::get(Vacation::ID));
        }
        return $insertedVacationDays + $userVacationsDays <= $maxVacationDays;
    }

    public function message() {
        return ErrorMessages::EXCEED_MAX_VACATIONS_DAYS;
    }

}
