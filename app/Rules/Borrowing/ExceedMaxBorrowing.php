<?php

namespace App\Rules\Borrowing;

use Illuminate\Contracts\Validation\Rule;
use App\Utils\Messages\ErrorMessages;
use App\Repository\BorrowingRepository;
use App\Repository\SettingsRepository;
use App\Models\Setting;
use Tests\Feature\Utils\TestUtil;
use App\Models\Borrowing;
use App\Utils\BorrowingUtils\groups\Create;
use App\Utils\BorrowingUtils\groups\Update;
use Illuminate\Support\Facades\Input;
class ExceedMaxBorrowing implements Rule {

    private $borrowingRepository;
    private $settingsRepository;
    private $group;

    function __construct($group) {
        $this->borrowingRepository = new BorrowingRepository();
        $this->settingsRepository = new SettingsRepository();
        $this->group = $group;
    }

    public function passes($attribute, $value) {
        if ($this->group === Create::class) {
            $totalBorrowings = $this->borrowingRepository->getUserTotalBorrowings();
        }
        else if($this->group === Update::class){
            $totalBorrowings = $this->borrowingRepository->getUserTotalBorrowingsExcept(Input::get(Borrowing::ID));
        }
        $maximumBorrowing = $this->settingsRepository->getSettings()[Setting::MAXIMUM_BORROWING];
        if (is_numeric($value)) {
            return $value + $totalBorrowings <= $maximumBorrowing;
        }
        return true;
    }

    public function message() {
        return TestUtil::getErrorMessage(ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
                        , [Borrowing::BORRWOING_VALUE, Setting::MAXIMUM_BORROWING]);
    }

}
