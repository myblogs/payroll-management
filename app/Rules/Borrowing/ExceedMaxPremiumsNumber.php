<?php

namespace App\Rules\Borrowing;

use Illuminate\Contracts\Validation\Rule;
use App\Repository\SettingsRepository;
use App\Utils\Messages\ErrorMessages;
use Tests\Feature\Utils\TestUtil;
use App\Models\Setting;
use App\Models\Borrowing;
class ExceedMaxPremiumsNumber implements Rule {

    private $settingsRepository;

    public function __construct() {
        $this->settingsRepository = new SettingsRepository();
    }
    public function passes($attribute, $value) {
        $maxPremiumsNumber=$this->settingsRepository->getSettings()[Setting::MAXIMUM_PREMIUMS_NUMBERS];
        return $value<=$maxPremiumsNumber;
    }
    public function message() {
        return TestUtil::getErrorMessage(ErrorMessages::ATTRIBUTE_EXCEED_ATTRIBUTE
                ,[Borrowing::PREMIUMS_NUMBER, Setting::MAXIMUM_PREMIUMS_NUMBERS]);
    }
}
