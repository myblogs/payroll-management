<?php

namespace App\Rules\Borrowing;

use Illuminate\Contracts\Validation\Rule;
use App\Utils\Messages\ErrorMessages;
use App\Utils\BorrowingUtils\BusinesUtil;
class ValidPremiumDate implements Rule {

    public function __construct() {
        
    }

    public function passes($attribute, $value) {
        $value = strtotime($value);
        $premiumDateDay = date('d', $value);
        $premiumDate = date('Y-m-d', $value);
        return($premiumDateDay === '01' && BusinesUtil::subCurrentDateFromInMonth($premiumDate)>=2);
    }

    public function message() {
        return ErrorMessages::INVALID_FIRST_DATE_PREMIUM;
    }

}
