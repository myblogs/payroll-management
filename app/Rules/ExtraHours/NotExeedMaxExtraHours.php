<?php

namespace App\Rules\ExtraHours;

use Illuminate\Contracts\Validation\Rule;
use App\Utils\Messages\ErrorMessages;
use App\Repository\SettingsRepository;
use App\Models\Setting;
class NotExeedMaxExtraHours implements Rule
{
    private $settingsRepository;
    public function __construct()
    {
        $this->settingsRepository=new SettingsRepository();
    }

    public function passes($attribute, $value)
    {
        $max_extra_hours=$this->settingsRepository->getSettings()[Setting::MAX_EXTRA_HOURS];
        return $value<=$max_extra_hours;
    }

    public function message()
    {
        return ErrorMessages::EXCEED_MAX_EXTRA_HOURS;
    }
}
