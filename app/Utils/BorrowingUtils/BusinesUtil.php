<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils\BorrowingUtils;

/**
 * Description of Businessutil
 *
 * @author Ahmed Atef
 */
class BusinesUtil {
    public static function subCurrentDateFromInMonth($date){
            $currentDate = date_create(date('Y-m').'-01');
            $date = date_create(date('Y-m', strtotime($date)).'-01');
            $diffInMonth = date_diff($currentDate, $date)->format('%m');
            return $diffInMonth;
        }
}
