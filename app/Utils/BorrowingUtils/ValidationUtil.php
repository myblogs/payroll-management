<?php

namespace App\Utils\BorrowingUtils;

use App\Rules\Borrowing\ExceedMaxBorrowing;
use App\Rules\Borrowing\ValidPremiumDate;
use App\Models\Borrowing;
use App\Rules\Borrowing\ExceedMaxPremiumsNumber;

class ValidationUtil {

    public static function getRules($group) {
        return [
            Borrowing::BORRWOING_VALUE => ['required', 'numeric', 'min:0', new ExceedMaxBorrowing($group)],
            Borrowing::PREMIUMS_NUMBER => ['required', 'numeric', 'min:1', new ExceedMaxPremiumsNumber],
            Borrowing::FISRT_DATE_PREMIUM => ['date_format:Y-m-d', new ValidPremiumDate],
        ];
    }

}
