<?php

namespace App\Utils\VacationsUtils;

use App\Models\FinancialVacation;
use App\Models\Setting;
use Tests\Feature\Utils\TestUtil;
use App\Utils\AppUtils;
use App\Vacation;
use App\Rules\vacations\AfterLastVacation;
use App\Rules\vacations\NotExceedVacationsDays;
use App\Rules\vacations\SameCurrentYear;
class BusinessUtil {

    public static function getVacationDays($endDate, $startDate, $financialVacations, $settings) {
        $dates = self::getDates($financialVacations);
        $firstWeekEndDay = $settings[Setting::FIRST_WEEKEND_DAY];
        $secondWeekEndDay = $settings[Setting::SECOND_WEEKEND_DAY];
        $abolishedDays = 0;
        $dateCounter = $startDate;
        while (AppUtils::getDiffInDays($dateCounter, $endDate) <= 0) {
            if (in_array($dateCounter, $dates) || date('D', strtotime($dateCounter)) === $firstWeekEndDay || date('D', strtotime($dateCounter)) === $secondWeekEndDay) {
                $abolishedDays++;
            }
            $dateCounter = TestUtil::addToDate($dateCounter, '+1 day');
        }
        return abs(AppUtils::getDiffInDays($endDate, $startDate) + 1 - $abolishedDays);
    }

    private static function getDates($financialVacations) {
        $dates = array();
        foreach ($financialVacations as $financialVacation) {
            $dates[] = $financialVacation[FinancialVacation::DATE];
        }
        return $dates;
    }

    public static function getValidationRules($group,$startDate=null) {
        return [
            Vacation::START_DATE => ['required', 'date_format:Y-m-d', 'after_or_equal:' . date('Y-m-d')
                ,new SameCurrentYear, new AfterLastVacation($group)],
            Vacation::END_DATE => ['required', 'date_format:Y-m-d', 'after:' . Vacation::START_DATE,new SameCurrentYear
                , new NotExceedVacationsDays($group,$startDate)]
        ];
    }

}
