<?php

namespace App\Utils\VacationsUtils;

use App\Repository\FinancialVacationRepository;
use App\Repository\SettingsRepository;
use App\Vacation;
use App\Utils\AppUtils;
use App\Utils\Messages\ErrorMessages;
use App\Utils\GlobalVariables;
use Illuminate\Support\Facades\Validator;
use App\Utils\VacationsUtils\BusinessUtil;
use App\Utils\BorrowingUtils\groups\Create;

class VacationControllerUtil {

    public static function prepareInputs($inputs) {
        $financailVacations = (new FinancialVacationRepository())->getAll();
        $settings = (new SettingsRepository())->getSettings();
        $inputs[Vacation::VACATION_DAYS] = BusinessUtil::getVacationDays($inputs[Vacation::END_DATE]
                        , $inputs[Vacation::START_DATE], $financailVacations, $settings);
        return $inputs;
    }

    public static function abortIfVacationStart($date) {
        if ((AppUtils::getDiffInDays($date, date('Y-m-d'))) <= 0) {
            abort(GlobalVariables::BAD_REQUEST, ErrorMessages::VACATION_START);
        }
    }

    public static function abortIfNotValid($vacation) {
        $inputs=self::convertToRequestInputs($vacation);
        $validator = Validator::make($inputs, 
                BusinessUtil::getValidationRules(Create::class,$inputs[Vacation::START_DATE]));
        if ($validator->fails()) {
            abort(GlobalVariables::BAD_REQUEST, ErrorMessages::NOT_VALID);
        }
    }
    private static function convertToRequestInputs($vacation){
        $inputs[Vacation::START_DATE]=$vacation[Vacation::START_DATE];
        $inputs[Vacation::END_DATE]=$vacation[Vacation::END_DATE];
       return $inputs;
    }
}
