<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils\Messages;

/**
 * Description of SucessMessges
 *
 * @author Ahmed Atef
 */
class SucessMessges {
    const CREARTED_SUCCESSFULLY="Created successfully";
    const UPDATED_SUCCESSFULLY = "Updated successfully";
    const DELETED_SUCCESSFULLY  = "Deleted successfully";
    const SETTINGS_HAS_SET_SUCCESSFULLY="Settings has set successfully";
}
