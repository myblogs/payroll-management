<?php

namespace App\Utils\Messages;

class ErrorMessages {
    const DATE_IS_BEFORE_DATE="The :attribute must be a date after :attribute.";
    const ATTRIBUTE_IS_EMPTY = "The :attribute is required.";
    const IS_LESS_THAN_ZERO = "The :attribute must be at least 0.";
    const IS_LESS_THAN_ONE = "The :attribute must be at least 1.";
    const IS_NOT_NUMBER = "The :attribute must be a number.";
    const IS_NOT_DATE = "The :attribute does not match the format Y-m-d.";
    const ATTRIBUTE_EXCEED_ATTRIBUTE = "The :attribute exceed The :attribute.";
    const EXCEED_MAX_VACATIONS_DAYS= "Sorry, you are exceed maximum vacations days";
    const INVALID_FIRST_DATE_PREMIUM = "The :attribute is not valid :attribute.";
    const AUTHENTICATED_USER_BORROW_BEFORE = "Authenticated user borrow before";
    const DATE_IS_EXPIRE = "date is expire";
    const NEW_VACATION_BEFORE_LAST_VACATION = "New vacation start date is after last vacation end date";
    public static function BEFORE_CURRENT_DATE(){
        return "The :attribute must be a date after or equal to " . date('Y-m-d') . ".";
    }
    const VACATION_START="Sorry,vaction started";
    const NOT_VALID="Sorry,vacation you want to restore is not valid";
    const DATE_YEAR_IS_NOT_CURRENT_YEAR = "Date year is not current year";
    const EXCEED_MAX_EXTRA_HOURS="Sorry,you are exceed maximum extra hours";
}
