<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils;

class AppUtils {

    public static function getOrAbortIfNotExist($model) {
        if ($model === null) {
            abort(GlobalVariables::NOT_FOUND);
        } else {
            return $model;
        }
    }

    public static function getDiffInDays($date1, $date2) {
        return (strtotime($date1) - strtotime($date2)) / (60 * 60 * 24);
    }

}
