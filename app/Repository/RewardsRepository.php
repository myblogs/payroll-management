<?php

namespace App\Repository;

use App\Models\Reward;

class RewardsRepository {

    public function store($inputs) {
        Reward::create($inputs);
    }

    public function getReward($id) {
        return Reward::find($id);
    }

    public function update($inputs, $id) {
        $reward = Reward::find($id);
        $reward[Reward::USER_ID] = $inputs[Reward::USER_ID];
        $reward[Reward::REWARD_VALUE] = $inputs[Reward::REWARD_VALUE];
        $reward[Reward::DESCRIPTION] = $inputs[Reward::DESCRIPTION];
        $reward->save();
    }

    public function getAllRewards($pageSize) {
        return Reward::with(Reward::USER)->simplePaginate($pageSize);
    }

    public function delete($reward) {
        $reward->delete();
    }

    public function getDeletedRewards($pageSize) {
        return Reward::onlyTrashed()->with(Reward::USER)->simplePaginate($pageSize);
    }

    public function getDeletedReward($id) {
        return Reward::onlyTrashed()->find($id);
    }

    public function restore($reward) {
        $reward->restore();
    }

    public function forceDelete($reward) {
        $reward->forceDelete();
    }
    public function getWorkerRewards($id,$pageSize){
        return Reward::withTrashed()
                ->where(Reward::USER_ID,$id)
                ->simplePaginate($pageSize);
    }

}
