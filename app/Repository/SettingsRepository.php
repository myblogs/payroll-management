<?php

namespace App\Repository;
use App\Models\Setting;
class SettingsRepository{
    public function store($inputs){
        return Setting::create($inputs);
    }
    
    public function setSettings($inputs,$id){
        $setting = Setting::find($id);
        $setting[Setting::MAXIMUM_BORROWING]=$inputs[Setting::MAXIMUM_BORROWING];
        $setting[Setting::MAXIMUM_PREMIUMS_NUMBERS]=$inputs[Setting::MAXIMUM_PREMIUMS_NUMBERS];
        $setting[Setting::MAXIMUM_VACATION_DAYS]=$inputs[Setting::MAXIMUM_VACATION_DAYS];
        $setting[Setting::FIRST_WEEKEND_DAY]=$inputs[Setting::FIRST_WEEKEND_DAY];
        $setting[Setting::SECOND_WEEKEND_DAY]=$inputs[Setting::SECOND_WEEKEND_DAY];
        $setting[Setting::MAX_EXTRA_HOURS]=$inputs[Setting::MAX_EXTRA_HOURS];
        $setting[Setting::HOUR_VALUE]=$inputs[Setting::HOUR_VALUE];
        $setting->save();
    }

    public function getSettings() {
        return Setting::get()->first();
    }

}
