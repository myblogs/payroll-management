<?php

namespace App\Repository;

use App\Models\Borrowing;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Utils\GlobalVariables;
class BorrowingRepository {

    public function store($inputs) {
        $createdBorrowing = Borrowing::create($inputs);
        $createdBorrowing[Borrowing::USER];
        return $createdBorrowing;
    }

    public function getUserTotalBorrowings() {
        return Borrowing::withTrashed()
                        ->whereYear('created_at', date('Y'))
                        ->Where(Borrowing::USER_ID, $this->getUserId())
                        ->groupBy(Borrowing::USER_ID)
                        ->sum(Borrowing::BORRWOING_VALUE);
    }

    public function isAuthenticatedUserBorrow() {
        return(Borrowing::where(Borrowing::USER_ID, $this->getUserId())->count() > 0);
    }

    public function getBorrowing($id) {
        return Borrowing::find($id);
    }

    public function update($inputs, $id) {
        $borrowing = Borrowing::find($id);
        $borrowing[Borrowing::BORRWOING_VALUE] = $inputs[Borrowing::BORRWOING_VALUE];
        $borrowing[Borrowing::PREMIUMS_NUMBER] = $inputs[Borrowing::PREMIUMS_NUMBER];
        $borrowing[Borrowing::FISRT_DATE_PREMIUM] = $inputs[Borrowing::FISRT_DATE_PREMIUM];
        $borrowing->save();
    }

    public function delete($borrowing) {
        $borrowing->delete();
    }

    public function getDeletedBorrowing($id) {
        return Borrowing::onlyTrashed()->find($id);
    }

    public function restore($borrowing) {
        $borrowing->restore();
    }

    public function forceDelete($borrowing) {
        $borrowing->forceDelete();
    }

    public function getWorkerBorrowings($id, $pageSize) {
        return Borrowing::with(Borrowing::USER)
                        ->withTrashed()
                        ->where(Borrowing::USER_ID, $id)
                        ->orderBy(Borrowing::ID,'desc')
                        ->simplePaginate($pageSize);
    }

    public function getAllBorrowings($pageSize) {
        return Borrowing::with(Borrowing::USER)->withTrashed()->simplePaginate($pageSize);
    }

    public function getUserTotalBorrowingsExcept($id) {
        return Borrowing::withTrashed()
                        ->where(Borrowing::ID, '!=', $id)
                        ->whereYear('created_at', date('Y'))
                        ->Where(Borrowing::USER_ID, $this->getUserId())
                        ->groupBy(Borrowing::USER_ID)
                        ->sum(Borrowing::BORRWOING_VALUE);
    }

    public function search($name,$pageSize) {
        return Borrowing::join(
                GlobalVariables::USERS, 
                GlobalVariables::BORROWINGS . '.' . Borrowing::USER_ID,
                '=',
                GlobalVariables::USERS . '.' . User::ID
                )
                ->withTrashed()
                ->where(User::NAME,'like',"%$name%")
                ->simplePaginate($pageSize);
    }
    
    //Utils
    private function getUserId() {
        return Auth::user()[User::ID];
    }

}
