<?php

namespace App\Repository;

use App\User;
use App\Utils\GlobalVariables;

class WorkerRepository {

    public function store($inputs) {
        return User::create($inputs);
    }

    public function getWorker($id) {
        return User::find($id);
    }

    public function getAllWorkers($pageSize) {
        return User::where(User::TYPE, GlobalVariables::WORKER_TYPE)->simplePaginate($pageSize);
    }

    public function getWorkers() {
        return User::where(User::TYPE, GlobalVariables::WORKER_TYPE)->get();
    }

    public function updateWorker($inputs, $id) {
        $user = User::find($id);
        $user[User::NAME] = $inputs[User::NAME];
        $user[User::JOB_ID] = $inputs[User::JOB_ID];
        $user[User::INSURANCE_NUMBER] = $inputs[User::INSURANCE_NUMBER];
        $user[User::WORKING_DATE] = $inputs[User::WORKING_DATE];
        $user[User::SALARY] = $inputs[User::SALARY];
        $user[User::START_CONTRACT_DATE] = $inputs[User::START_CONTRACT_DATE];
        $user[User::END_CONTRACT_DATE] = $inputs[User::END_CONTRACT_DATE];
        $user[User::NATIONALITY] = $inputs[User::NATIONALITY];
        $user[User::PHONE] = $inputs[User::PHONE];
        $user[User::BIRTH_DATE] = $inputs[User::BIRTH_DATE];
        $user[User::ADDRESS] = $inputs[User::ADDRESS];
        $user[User::PASSPORT_NUMBER] = $inputs[User::PASSPORT_NUMBER];
        $user[User::PASSPORT_RELEASE_DATE] = $inputs[User::PASSPORT_RELEASE_DATE];
        $user[User::PASSPORT_EXPIRY_DATE] = $inputs[User::PASSPORT_EXPIRY_DATE];
        $user[User::PASSPORT_RELEASE_PLACE] = $inputs[User::PASSPORT_RELEASE_PLACE];
        $user[User::RESIDENCY_NUMBER] = $inputs[User::RESIDENCY_NUMBER];
        $user[User::RESIDENCY_RELEASE_DATE] = $inputs[User::RESIDENCY_RELEASE_DATE];
        $user[User::RESIDENCY_EXPIRY_DATE] = $inputs[User::RESIDENCY_EXPIRY_DATE];
        $user[User::RESIDENCY_RELEASE_PLACE] = $inputs[User::RESIDENCY_RELEASE_PLACE];
        $user[User::WORK_LICENSE_NUMBER] = $inputs[User::WORK_LICENSE_NUMBER];
        $user[User::WORK_LICENSE_RELEASE_DATE] = $inputs[User::WORK_LICENSE_RELEASE_DATE];
        $user[User::WORK_LICENSE_EXPIRY_DATE] = $inputs[User::WORK_LICENSE_EXPIRY_DATE];
        $user[User::WORK_LICENSE_RELEASE_PLACE] = $inputs[User::WORK_LICENSE_RELEASE_PLACE];
        $user->save();
    }

    public function updateProfile($inputs, $id) {
        $user = User::find($id);
        $user[User::NAME] = $inputs[User::NAME];
        $user[User::NATIONALITY] = $inputs[User::NATIONALITY];
        $user[User::PHONE] = $inputs[User::PHONE];
        $user[User::BIRTH_DATE] = $inputs[User::BIRTH_DATE];
        $user[User::ADDRESS] = $inputs[User::ADDRESS];
        $user[User::PASSPORT_NUMBER] = $inputs[User::PASSPORT_NUMBER];
        $user[User::PASSPORT_RELEASE_DATE] = $inputs[User::PASSPORT_RELEASE_DATE];
        $user[User::PASSPORT_EXPIRY_DATE] = $inputs[User::PASSPORT_EXPIRY_DATE];
        $user[User::PASSPORT_RELEASE_PLACE] = $inputs[User::PASSPORT_RELEASE_PLACE];
        $user[User::RESIDENCY_NUMBER] = $inputs[User::RESIDENCY_NUMBER];
        $user[User::RESIDENCY_RELEASE_DATE] = $inputs[User::RESIDENCY_RELEASE_DATE];
        $user[User::RESIDENCY_EXPIRY_DATE] = $inputs[User::RESIDENCY_EXPIRY_DATE];
        $user[User::RESIDENCY_RELEASE_PLACE] = $inputs[User::RESIDENCY_RELEASE_PLACE];
        $user[User::WORK_LICENSE_NUMBER] = $inputs[User::WORK_LICENSE_NUMBER];
        $user[User::WORK_LICENSE_RELEASE_DATE] = $inputs[User::WORK_LICENSE_RELEASE_DATE];
        $user[User::WORK_LICENSE_EXPIRY_DATE] = $inputs[User::WORK_LICENSE_EXPIRY_DATE];
        $user[User::WORK_LICENSE_RELEASE_PLACE] = $inputs[User::WORK_LICENSE_RELEASE_PLACE];
        $user->save();
    }

    public function deleteWorker($worker) {
        $worker->delete();
    }

    public function restoreWorker($worker) {
        $worker->restore();
    }

    public function getDeletedWorker($id) {
        return User::withTrashed()->find($id);
    }

    public function getDeletedWorkers($pageSize) {
        return User::onlyTrashed()->where(User::TYPE, GlobalVariables::WORKER_TYPE)->simplePaginate($pageSize);
    }

    public function forceDelete($worker) {
        $worker->forceDelete();
    }

    public function getSearchedWorkers($name, $pageSize) {
        return User::withTrashed()
                        ->where(User::NAME, 'LIKE', "%" . $name . "%")
                        ->where(User::TYPE, GlobalVariables::WORKER_TYPE)
                        ->simplePaginate($pageSize);
    }

}
