<?php
namespace App\Repository;
use App\Models\FinancialVacation;
class FinancialVacationRepository {
    public function sotre($inputs){
        FinancialVacation::create($inputs);
    }

    public function getFinancialVacation($id) {
        return FinancialVacation::find($id);
    }
    public function update($input, $id) {
        $financialVacation= FinancialVacation::find($id);
        $financialVacation[FinancialVacation::DATE]=$input[FinancialVacation::DATE];
        $financialVacation[FinancialVacation::NAME]=$input[FinancialVacation::NAME];
        $financialVacation[FinancialVacation::DESCRIPTION]=$input[FinancialVacation::DESCRIPTION];
        $financialVacation->save();
    }

    public function delete($financialVacation) {
        $financialVacation->delete();
    }

    public function restore($financialVacation) {
        $financialVacation->restore();
    }

    public function getDeletedFinancialVacation($id) {
        return FinancialVacation::withTrashed()->find($id);
    }

    public function forceDelete($financialVacation) {
        $financialVacation->forceDelete();
    }

    public function getPagedAll($pageSize) {
        return FinancialVacation::withTrashed()->simplePaginate($pageSize);
    }
    public function getAll(){
        return FinancialVacation::get();
    }
}
