<?php

namespace App\Repository;

use App\Models\Job;

class JobRepository {

    public function storeJob($jobData) {
        return Job::create($jobData);
    }

    public function getJob($id) {
        return Job::find($id);
    }

    public function updateJob($jobData, $id) {
        Job::where(Job::ID, $id)->update($jobData);
    }

    public function getPagedJobs($pageSize) {
        return Job::simplePaginate($pageSize);
    }
    
    public function getAllJobs() {
        return Job::get();
    }
    
    public function destroyJob($job) {
        $job->delete();
    }

    public function restoreJob($job) {
        $job->restore();
    }

    public function getDeletedJob($id) {
        return Job::withTrashed()->find($id);
    }

    public function getDeletedJobs($pageSize) {
        return Job::onlyTrashed()->simplePaginate($pageSize);
    }

    public function getSearchedJobs($name, $pageSize) {
        return Job::withTrashed()->where(Job::NAME,'LIKE',"%" . $name ."%")
                ->simplePaginate($pageSize);
    }

    public function forceDeleteJob($job) {
        $job->forceDelete();
    }

}
