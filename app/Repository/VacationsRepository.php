<?php

namespace App\Repository;

use App\Vacation;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Utils\GlobalVariables;
class VacationsRepositroy {

    public function isAfterLastVacation($startDate) {
        if (Vacation::count() === 0) {
            return true;
        }
        $count = Vacation::whereDate(Vacation::END_DATE, '<', $startDate)->count();
            return Vacation::count()-$count==0;
    }

    public function sumUserVacationsDays() {
        return Vacation
                        ::whereYear('created_at', date('Y'))
                        ->where(Vacation::USER_ID, $this->getUserId())
                        ->groupBy(Vacation::USER_ID)
                        ->sum(Vacation::VACATION_DAYS);
    }

    public function sumUserVacationsDaysExcept($id) {
        return Vacation
                        ::whereYear('created_at', date('Y'))
                        ->where(Vacation::USER_ID, $this->getUserId())
                        ->where(Vacation::ID, '!=', $id)
                        ->groupBy(Vacation::USER_ID)
                        ->sum(Vacation::VACATION_DAYS);
    }

    public function store($input) {
        Vacation::create($input);
    }

    public function getVacation($id) {
        return Vacation::find($id);
    }

    public function isAfterLastVacationExcept($startDate, $id) {
        if (Vacation::count() === 1) {
            return true;
        }
        $count = Vacation
                ::whereDate(Vacation::END_DATE, '<', $startDate)
                ->orWhere(Vacation::ID, $id)
                ->count();

        return $count > 1;
    }

    public function update($inputs, $id) {
        $vacation = Vacation::find($id);
        $vacation[Vacation::START_DATE] = $inputs[Vacation::START_DATE];
        $vacation[Vacation::END_DATE] = $inputs[Vacation::END_DATE];
        $vacation[Vacation::VACATION_DAYS] = $inputs[Vacation::VACATION_DAYS];
        $vacation->save();
    }

    public function delete($vacation) {
        $vacation->delete();
    }

    public function getDeletedVacation($id) {
        return Vacation::onlyTrashed()->find($id);
    }

    public function forceDelete($vacation) {
        $vacation->forceDelete();
    }

    public function restore($vacation) {
        $vacation->restore();
    }

    public function getWorkerVacations($pageSize) {
        return Vacation::withTrashed()
                        ->with(Vacation::USER)
                        ->where(Vacation::USER_ID,$this->getUserId())
                        ->simplePaginate($pageSize);
    }
    public function search($name,$pageSize) {
        return Vacation::join(
                GlobalVariables::USERS, 
                GlobalVariables::VACATIONS . '.' . Vacation::USER_ID,
                '=',
                GlobalVariables::USERS . '.' . User::ID
                )
                ->withTrashed()
                ->where(User::NAME,'like',"%$name%")
                ->simplePaginate($pageSize);
    }
    
    public function getAll($pageSize) {
        return Vacation::withTrashed()
                ->with(Vacation::USER)
                ->simplePaginate($pageSize);
    }

    //Utils
    private function getUserId() {
        return Auth::user()[User::ID];
    }

}
