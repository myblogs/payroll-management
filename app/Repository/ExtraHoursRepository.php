<?php

namespace App\Repository;

use App\Models\ExtraHours;
use App\Utils\GlobalVariables;
use App\User;

class ExtraHoursRepository {

    public function store($inputs) {
        return ExtraHours::create($inputs);
    }

    public function getExtraHours($id) {
        return ExtraHours::find($id);
    }

    public function update($inputs, $id) {
        $extraHours = ExtraHours::find($id);
        $extraHours[ExtraHours::USER_ID] = $inputs[ExtraHours::USER_ID];
        $extraHours[ExtraHours::EXTRA_HOURS] = $inputs[ExtraHours::EXTRA_HOURS];
        $extraHours->save();
    }

    public function getPagingExtraHours($pageSize) {
        return ExtraHours::with(ExtraHours::USER)
                        ->simplePaginate($pageSize);
    }

    public function delete($extraHours) {
        $extraHours->delete();
    }

    public function getDeletedExtraHours($id) {
        return ExtraHours::onlyTrashed()->find($id);
    }

    public function restore($extraHours) {
        return $extraHours->restore();
    }

    public function forceDelete($extraHours) {
        $extraHours->forceDelete();
    }

    public function getPagingDeletedExtraHours($pageSize) {
        return ExtraHours::onlyTrashed()
                        ->simplePaginate($pageSize);
    }

    public function search($pageSize, $request) {
        $needle=$request[User::NAME];
        $extraHours = ExtraHours::select(GlobalVariables::EXTRA_HOURS. ".*",User::NAME)
                ->join(
                        GlobalVariables::USERS, GlobalVariables::EXTRA_HOURS . '.' . ExtraHours::USER_ID, '=', GlobalVariables::USERS . '.' . User::ID
                )
                ->withTrashed()
                ->where(User::NAME, 'like', "%$needle%")
                ->simplePaginate($pageSize);
        $extraHours->appends($request->all(User::NAME));
        return $extraHours;
    }   
    
    public function getWorkerExtraHours($pageSize,$id){
        return ExtraHours::with(ExtraHours::USER)
                ->withTrashed()
                ->where(ExtraHours::USER_ID,$id)
                ->simplePaginate($pageSize);
    }
}
