<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Job;
use App\Policies\JobPolicy;
use App\Policies\BorrowingPolicy;
use App\Models\Borrowing;
use App\Policies\VacationPolicy;
use App\Vacation;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         Job::class => JobPolicy::class,
         Borrowing::class => BorrowingPolicy::class,
         Vacation::class=> VacationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
