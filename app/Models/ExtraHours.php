<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class ExtraHours extends Model
{
    use SoftDeletes;
    const ID="id";
    const USER_ID="user_id";
    const EXTRA_HOURS="extra_hours";
    const USER="user";
    protected $fillable=[
        self::EXTRA_HOURS,
        self::USER_ID,
    ];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
