<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Reward extends Model {
    use SoftDeletes;
    const ID = "id";
    const REWARD_VALUE = "reward_value";
    const DESCRIPTION = "description";
    const USER_ID = "user_id";
    const USER="user";
    protected $fillable = [
        self::REWARD_VALUE,
        self::DESCRIPTION,
        self::USER_ID,
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

}
