<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
class Borrowing extends Model {
    use SoftDeletes;
    const ID = "id";
    const PREMIUMS_NUMBER = "perimiums_number";
    const FISRT_DATE_PREMIUM = "first_date_premium";
    const BORRWOING_VALUE = "borrowing_value";
    const USER_ID = "user_id";
    const USER = "user";

    protected $fillable = [
        self::ID,
        self::PREMIUMS_NUMBER,
        self::FISRT_DATE_PREMIUM,
        self::BORRWOING_VALUE,
        self::USER_ID,
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }

}
