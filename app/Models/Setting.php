<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Setting extends Model {

    const ID = 'id';
    const MAXIMUM_BORROWING = 'maximum_borrowing';
    const MAXIMUM_PREMIUMS_NUMBERS = 'maximum_premiums_numbers';
    const MAXIMUM_VACATION_DAYS="maximum_vacation_days";
    const FIRST_WEEKEND_DAY="first_weekend_day";
    const SECOND_WEEKEND_DAY="second_weekend_day";
    const MAX_EXTRA_HOURS="max_extra_hours";
    const HOUR_VALUE="hour_value";
    protected $fillable = [
        self::MAXIMUM_BORROWING,
        self::MAXIMUM_PREMIUMS_NUMBERS,
        self::MAXIMUM_VACATION_DAYS,
        self::FIRST_WEEKEND_DAY,
        self::SECOND_WEEKEND_DAY,
    ];

}
