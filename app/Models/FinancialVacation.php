<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class FinancialVacation extends Model {
    use SoftDeletes;
    
    const ID = "id";
    const SETTING_ID = "setting_id";
    const DATE = "date";
    const NAME = "name";
    const DESCRIPTION = "description";

    protected $fillable = [
        self::SETTING_ID,
        self::DATE,
        self::NAME,
        self::DESCRIPTION,
    ];

}
