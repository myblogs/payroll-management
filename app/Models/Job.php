<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Job extends Model
{
    use SoftDeletes;
    const NAME='name';
    const ID='id';
    protected $fillable=[self::NAME];
    public function user(){
        return $this->hasOne(User::class);
    }
}
