function putPostVacation(e, methodType) {
    e.preventDefault();
    if (methodType === 'post') {
        var user_id = $("#user_id").attr('name');
    } else if (methodType === 'put') {
        var id = $("#id").attr('name');
    }
    var start_date = $("#start_date").attr('name');
    var end_date = $("#end_date").attr('name');
    
    inputs = {};
    inputs['_token'] = $('#form').attr('data-token');
    if (methodType === 'post') {
        inputs[user_id] = $("#user_id").val();

    } else if (methodType === 'put') {
        inputs[id] = $("#id").val();
    }
    inputs[start_date] = $("#start_date").val();
    inputs[end_date] = $("#end_date").val();
    $.ajax({
        url: $('#form').attr('data-url'),
        type: methodType,
        data: inputs,
        success: function (data) {
            alert(data);
            window.location = $('#form').attr('data-redirect');
        },
        error: function (data) {
            if (data.status === 400) {
                alert(JSON.parse(data.responseText)['message']);
            } else if (data.status === 422) {
                var errors = JSON.parse(data.responseText).errors;
                var start_date_errors = errors[start_date] == null ? '' : errors[start_date][0];
                var end_date_errors = errors[end_date] == null ? '' : errors[end_date][0];
                $("#start_date_errors").text(start_date_errors);
                $("#end_date_errors").text(end_date_errors);
            }

        }
    });

}
