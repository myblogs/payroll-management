function putPostExtraHours(e, methodType) {
    e.preventDefault();
    var user_id = $("#user_id").attr('name');
    var extra_hours = $("#extra_hours").attr('name');
    inputs = {};
    inputs['_token'] = $('#form').attr('data-token');
    inputs[user_id] = $("#user_id").val();
    inputs[extra_hours] = $("#extra_hours").val();
    $.ajax({
        url: $('#form').attr('data-url'),
        type: methodType,
        data: inputs,
        success: function (data) {
            alert(data);
            window.location=$('#form').attr('data-redirect');
        },
        error: function (data) {
                var errors = JSON.parse(data.responseText).errors;
                var extra_hours_errors = errors[extra_hours] == null ? '' : errors[extra_hours][0];
                $("#extra_hours_errors").text(extra_hours_errors);
        }
    });

}
