function postPutFinancialVacation(e, $methodType) {
    e.preventDefault();
    var date = $("#date").attr('name');
    var name = $("#name").attr('name');
    var description = $("#description").attr('name');
    var inputs = {};
    inputs['_token'] = $('#form').attr('data-token');
    inputs[name] = $("#name").val();
    inputs[date] = $("#date").val();
    inputs[description] = $("#description").val();
    if ($methodType === "post") {
        var setting_id = $("#setting_id").attr('name');
        inputs[setting_id] = $("#setting_id").val();
    }
    $.ajax({
        url: $('#form').attr('data-url'),
        type: $methodType,
        data: inputs,
        success: function (data) {
            alert(data);
            window.location = $('#form').attr('data-redirect');
        },
        error: function (data) {
            var errors = JSON.parse(data.responseText).errors;
                    var date_errors = errors[date] == null ? '' : errors[date][0];
                    var name_errors = errors[name] == null ? '' : errors[name][0];
                    var description_errors = errors[description] == null ? '' : errors[description][0];
                    $("#date_errors").text(date_errors);
                    $("#name_errors").text(name_errors);
                    $("#description_errors").text(description_errors);
                    
        }
    });
}
