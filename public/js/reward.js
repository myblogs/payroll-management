function putPostReward(e, methodType) {
    e.preventDefault();
    var user_id = $("#user_id").attr('name');
    var reward_value = $("#reward_value").attr('name');
    var description = $("#description").attr('name');
    inputs = {};
    inputs['_token'] = $('#form').attr('data-token');
    inputs[reward_value] = $('#reward_value').val();
    inputs[description] = $('#description').val();
    inputs[user_id] = $('#user_id').val();
    $.ajax({
        url: $('#form').attr('data-url'),
        type: methodType,
        data: inputs,
        success: function (data) {
            alert(data);
            window.location = $('#form').attr('data-redirect');
        },
        error: function (data) {
            var errors = JSON.parse(data.responseText).errors;
            var reward_value_errors = errors[reward_value] == null ? '' : errors[reward_value][0];
            var description_errors = errors[description] == null ? '' : errors[description][0];
            $("#reward_value_errors").text(reward_value_errors);
            $("#description_errors").text(description_errors);
        }
    });

}
