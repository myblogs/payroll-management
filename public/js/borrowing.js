function putBorrowing(e,methodType) {
    e.preventDefault();
    var user_id=$("#user_id").attr('name');
    if(methodType==='put'){
         var borrowing_id=$("#borrowing_id").attr('name');
    }
    var borrowing_value=$("#borrowing_value").attr('name');
    var premiums_number=$("#premiums_number").attr('name');
    var first_date_premium=$("#first_date_premium").attr('name');
    inputs={};
    inputs['_token']=$('#form').attr('data-token');
    inputs[user_id]=$("#user_id").val();
    if(methodType==='put'){
         inputs[borrowing_id]=$("#borrowing_id").val();
    }
    inputs[borrowing_value]=$("#borrowing_value").val();
    inputs[premiums_number]=$("#premiums_number").val();
    inputs[first_date_premium]=$("#first_date_premium").val();
    $.ajax({
            url: $('#form').attr('data-url'),
            type: methodType,
            data: inputs,
            success: function (data) {
                alert(data);
                window.location = $('#form').attr('data-redirect');
            },
            error: function (data) {
            if (data.status === 400) {
                alert(data.responseText);
            } else if (data.status === 422) {
            var errors = JSON.parse(data.responseText).errors;
                    var borrowing_value_errors = errors.borrowing_value == null ? '' : errors[borrowing_value][0];
                    var first_date_premium_errors = errors.first_date_premium == null ? '' : errors[first_date_premium][0];
                    $("#borrowing_value_errors").text(borrowing_value_errors);
                    $("#first_date_premium_errors").text(first_date_premium_errors);
            }
        }
    });
}
