function setSettings(e) {
    e.preventDefault();
    var max_borrowing_value=$("#max_borrowing_value").attr('name');
    var max_premiums_numbers=$("#max_premiums_numbers").attr('name');
    var first_weekend_day=$("#first_weekend_day").attr('name');
    var second_weekend_day=$("#second_weekend_day").attr('name');
    var max_vacation_days=$("#max_vacation_days").attr('name');
    var max_extra_hours=$("#max_extra_hours").attr('name');
    var hour_value=$("#hour_value").attr('name');
    var inputs={};
    inputs['_token']=$('#form').attr('data-token');
    inputs[max_borrowing_value]=$("#max_borrowing_value").val();
    inputs[max_premiums_numbers]=$("#max_premiums_numbers").val();
    inputs[first_weekend_day]=$("#first_weekend_day").val();
    inputs[second_weekend_day]=$("#second_weekend_day").val();
    inputs[max_vacation_days]=$("#max_vacation_days").val();
    inputs[max_extra_hours]=$("#max_extra_hours").val();
    inputs[hour_value]=$("#hour_value").val();
    $.ajax({
            url: $('#form').attr('data-url'),
            type: 'put',
            data: inputs,
            success: function (data) {
                alert(data);
                window.location = $('#form').attr('data-redirect');
            },
            error: function (data) {
            var errors = JSON.parse(data.responseText).errors;
                    var max_borrowing_value_errors = errors[max_borrowing_value] == null ? '' : errors[max_borrowing_value][0];
                    var max_premiums_numbers_errors = errors[max_premiums_numbers] == null ? '' : errors[max_premiums_numbers][0];
                    var max_vacation_days_errors = errors[max_vacation_days] == null ? '' : errors[max_vacation_days][0];
                    var max_extra_hours_errors = errors[max_extra_hours] == null ? '' : errors[max_extra_hours][0];
                    var hour_value_errors = errors[hour_value] == null ? '' : errors[hour_value][0];
                    $("#max_borrowing_value_errors").text(max_borrowing_value_errors);
                    $("#max_premiums_numbers_errors").text(max_premiums_numbers_errors);
                    $("#max_vacation_days_errors").text(max_vacation_days_errors);
                    $("#max_extra_hours_errors").text(max_extra_hours_errors);
                    $("#hour_value_errors").text(hour_value_errors);
        }
    });
}
